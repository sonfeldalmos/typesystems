\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module NatBoolNorm where

open import Lib
open import NatBool

module NormalForms {i j}(M : Model {i}{j}) where
  module M = Model M
  data Nf : Σ M.Ty M.Tm → Set where
    numNf   : (n : ℕ) → Nf (M.Nat , M.num n)
    trueNf  : Nf (M.Bool , M.true)
    falseNf : Nf (M.Bool , M.false)

  Nf~ : {w₀ w₁ : Σ M.Ty M.Tm}(w₂ : w₀ ≡ w₁) → Nf w₀ → Nf w₁ → Prop
  Nf~ w₂ (numNf m) (numNf n) = m ≡ n
  Nf~ w₂ (numNf n) trueNf    = ⊥
  Nf~ w₂ (numNf n) falseNf   = ⊥
  Nf~ w₂ trueNf    (numNf n) = ⊥
  Nf~ w₂ trueNf    trueNf    = ⊤
  Nf~ w₂ trueNf    falseNf   = ⊥
  Nf~ w₂ falseNf   (numNf n) = ⊥
  Nf~ w₂ falseNf   trueNf    = ⊥
  Nf~ w₂ falseNf   falseNf   = ⊤
  postulate
    Nf~= : (λ {w₀}{w₁} → _~ Nf {w₀}{w₁}) ≡ Nf~
    {-# REWRITE Nf~= #-}

  -- definitional injectivity:
  numNfInj' : {m n : ℕ} → Σp (M.num m ≡ M.num n) (λ e → (_~ Nf (refl {x = M.Nat} , e) (numNf m) (numNf n))) → m ≡ n
  numNfInj' = π₂

  -- NEM: minden A,a-ra, es n₀,n₁ : Nf (A,a)-ra Dec (n₀ ≡ n₁)

  dec=Nf : {w₀ w₁ : Σ M.Ty M.Tm}(n₀ : Nf w₀)(n₁ : Nf w₁) → Decp (Σp (w₀ ≡ w₁) λ e → (Nf ~) e n₀ n₁)
  dec=Nf (numNf m) (numNf n) with dec=ℕ m n
  ... | ι₁ e = ι₁ (mk ((refl {A = M.Ty} , cong M.num (un e)) , un e))
  ... | ι₂ ne = ι₂ λ e → ne (mk (π₂ (un e)))
  dec=Nf (numNf n) trueNf    = ι₂ λ w → mk (π₂ (un w))
  dec=Nf (numNf n) falseNf   = ι₂ λ w → mk (π₂ (un w))
  dec=Nf trueNf    (numNf n) = ι₂ λ w → mk (π₂ (un w))
  dec=Nf trueNf    trueNf    = ι₁ (mk (refl {A = Σ _ Nf}{x = (M.Bool , M.true) , trueNf}))
  dec=Nf trueNf    falseNf   = ι₂ λ w → mk (π₂ (un w))
  dec=Nf falseNf   (numNf n) = ι₂ λ w → mk (π₂ (un w))
  dec=Nf falseNf   trueNf    = ι₂ λ w → mk (π₂ (un w))
  dec=Nf falseNf   falseNf   = ι₁ (mk (refl {A = Σ _ Nf}{x = (M.Bool , M.false) , falseNf}))

open NormalForms I

Norm : DepModel
Norm = record
  { Ty       = λ _ → Lift ⊤
  ; Tm       = λ {Aᴵ} _ uᴵ → Nf (Aᴵ , uᴵ)
  ; Nat      = mk trivi
  ; Bool     = mk trivi
  ; true     = trueNf
  ; false    = falseNf
  ; ite      = λ {Aᴵ}{tᴵ}{uᴵ}{vᴵ}{_} → λ {
      trueNf  u v → coe (λ tᴵ → Nf (Aᴵ , tᴵ)) (sym {A = I.Tm Aᴵ} I.iteβ₁) u ;
      falseNf u v → coe (λ tᴵ → Nf (Aᴵ , tᴵ)) (sym {A = I.Tm Aᴵ} I.iteβ₂) v }
  ; num      = numNf
  ; isZero   = λ {tᴵ} → λ { (numNf zero) → coe (λ tᴵ → Nf (I.Bool , tᴵ)) (sym {A = I.Tm I.Bool} I.isZeroβ₁) trueNf ; (numNf (suc n)) → coe (λ tᴵ → Nf (I.Bool , tᴵ)) (sym {A = I.Tm I.Bool} I.isZeroβ₂) falseNf }
  ; _+o_     = λ {uᴵ}{vᴵ} → λ { (numNf m) (numNf n) → coe (λ tᴵ → Nf (I.Nat , tᴵ)) (sym {A = I.Tm I.Nat} I.+β) (numNf (m + n)) }
  ; iteβ₁    = λ {_}{_}{_}{A}{u}{v} → refl {x = u}
  ; iteβ₂    = λ {_}{_}{_}{A}{u}{v} → refl {x = v}
  ; isZeroβ₁ = refl {x = trueNf}
  ; isZeroβ₂ = refl {x = falseNf}
  ; +β       = λ {m}{n} → refl {x = numNf (m + n)}
  }
{-
eval : I.Tm Aᴵ → St.⟦ Aᴵ ⟧
eval true := tt
eval false := ff
eval (ite t u v) := ha (eval t),  akkor eval u

norm' : (aᴵ : I.Tm Aᴵ) → Nf (Aᴵ , aᴵ)
norm' true  :=  trueNf
norm' false := falseNf
norm' (ite t u v) := ha (norm' t = trueNf), akkor norm' u
-}
module Norm = DepModel Norm

-- B : (x : A) → C → Set
-- e : a₀ ≡ a₁
-- c : C
-- b₀ : B (a₀,c)
-- b₁ : B (a₁,c)
-- (B ~) (e, refl {c}) b₀ b₁ = (B(_,c) ~) e b₀ b₁
-- (B ~) (e , ap f e , e') b₀ b₁ = ((λ(x,z).B(x,f x,z)) ~) (e,e') b₀ b₁

-- norm' : (w : Σ I.Ty I.Tm) → Nf w
norm' : {Aᴵ : I.Ty}(aᴵ : I.Tm Aᴵ) → Nf (Aᴵ , aᴵ)
norm' = Norm.⟦_⟧t

stab' : {Aᴵ : I.Ty}{aᴵ : I.Tm Aᴵ}(n : Nf (Aᴵ , aᴵ)) → norm' {Aᴵ} aᴵ ≡ n
stab' (numNf n) = refl {x = numNf n}
stab' trueNf = refl {x = trueNf}
stab' falseNf = refl {x = falseNf}

propNf : {Aᴵ : I.Ty}{aᴵ : I.Tm Aᴵ}(m n : Nf (Aᴵ , aᴵ)) → m ≡ n
propNf {Aᴵ}{aᴵ} m n = trans {A = Nf (Aᴵ , aᴵ)} (sym {A = Nf (Aᴵ , aᴵ)} (stab' m)) (stab' n)

dec=Tm : {Aᴵ : I.Ty}(aᴵ₀ aᴵ₁ : I.Tm Aᴵ) → Decp (aᴵ₀ ≡ aᴵ₁)
dec=Tm {Aᴵ} aᴵ₀ aᴵ₁ with dec=Nf (norm' aᴵ₀) (norm' aᴵ₁)
... | ι₁ e = ι₁ (mk (π₂ (π₁ (un e))))
... | ι₂ ne = ι₂ λ e → ne (mk ((refl {x = Aᴵ} , un e) , cong {A = Σ I.Ty I.Tm}{B = Nf}(λ w → norm' (π₂ w)) (refl {x = Aᴵ} , un e)))
\end{code}
