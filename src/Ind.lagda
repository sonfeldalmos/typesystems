\chapter{Inductive types}
\label{ch:Ind}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Inductive types by examples: natural numbers, lists, binary trees.
  Type formers, constructors, iterator, recursor, defining functions by recursion.
\end{tcolorbox}

TODO: definablility. Equality is coarse: recNat zero (suc q) q ≠ q. We have several representations of the same function.

TODO: iterator is like a for loop where the body of the loop cannot refer to the loop variable. Recursor is like for loop. Fixpoint combinator is like while loop.

TODO: example: syntax as an inductive type.

TODO: redo nullary and binary products, sums as inductive types. See an exercise below.

TODO: add an infinitary example.

TODO: all functions defined by the iterator are terminating.

TODO: add mutual inductive types.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
open import Lib

module Ind where

module I where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_

  data Ty      : Set where
    _⇒_        : Ty → Ty → Ty
    Unit       : Ty
    Bool       : Ty
    Nat        : Ty
    List       : Ty → Ty
    Tree       : Ty → Ty

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  postulate
    Sub        : Con → Con → Set
    _⊚_        : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass        : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id         : ∀{Γ} → Sub Γ Γ
    idl        : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr        : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm         : Con → Ty → Set
    _[_]       : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]        : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]       : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_       : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂        : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η         : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]      : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                 (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                 (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    trivial    : ∀{Γ} → Tm Γ Unit
    iteUnit    : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
    Unitβ      : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
    trivial[]  : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
    iteUnit[]  : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
                 iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])

    true       : ∀{Γ} → Tm Γ Bool
    false      : ∀{Γ} → Tm Γ Bool
    iteBool    : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁     : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂     : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]  : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                 iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

    zeroo      : ∀{Γ} → Tm Γ Nat
    suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
    zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                 iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 iteList u v (cons t₁ t) ≡ (v [ id ,o t₁ ,o iteList u v t ])
    nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
    cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
    iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (t [ γ ])

    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    iteTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} → iteTree l n (leaf a) ≡ l [ id ,o a ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 iteTree l n (node ll rr) ≡ n [ id ,o iteTree l n ll ,o iteTree l n rr ]
    leaf[]     : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leaf a) [ γ ] ≡ leaf (a [ γ ])
    node[]     : ∀{Γ A}{ll rr : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 (node ll rr) [ γ ] ≡ node (ll [ γ ]) (rr [ γ ])
    iteTree[]  : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 iteTree l n t [ γ ] ≡ iteTree (l [ (γ ⊚ p) ,o q ]) (n [ (γ ⊚ p ⊚ p) ,o (q [ p ]) ,o q ]) (t [ γ ])

  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]


variable
  Aᴵ Bᴵ : I.Ty
  Γᴵ Δᴵ Θᴵ : I.Con
  γᴵ δᴵ θᴵ σᴵ γaᴵ : I.Sub Δᴵ Γᴵ
  tᴵ uᴵ vᴵ : I.Tm Γᴵ Aᴵ

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Con       : Set i
    Sub       : Con → Con → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Ty        : Set k
    Tm        : Con → Ty → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_       : Con → Ty → Con
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    _⇒_       : Ty → Ty → Ty
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    Unit      : Ty
    trivial   : ∀{Γ} → Tm Γ Unit
    iteUnit   : ∀{Γ A} → Tm Γ A → Tm Γ Unit → Tm Γ A
    Unitβ     : ∀{Γ A t} → iteUnit {Γ}{A} t trivial ≡ t
    trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
    iteUnit[] : ∀{Γ A t u Δ}{γ : Sub Δ Γ} →
                iteUnit {Γ}{A} u t [ γ ] ≡ iteUnit (u [ γ ]) (t [ γ ])

    Bool      : Ty
    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    iteBool   : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁    : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂    : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[] : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])
\end{code}

An inductive type is specified by its constructors. Its eliminator and
computation rules are determined by the constructors: the eliminator
says that for any type \verb$C$ (called motive) and elements of that
type which have the shape of the constructors (these are called
methods), there is a function (called iterator, recursor, fold,
catamorphism) from the inductive type to \verb$C$. The computation
rules say that if we apply the iterator to a constructor, we obtain
the corresponding element.

We have already seen some inductive types: \verb$Bool$ had two
constructors \verb$true$ and \verb$false$, and its iterator was called
if-then-else, it said that for any type \verb$C$ and two elements of
\verb$C$, there is a function from \verb$Bool$ to \verb$C$. For any
two types \verb$A$ and \verb$B$, \verb$A +o B$ was an inductive type
with constructors \verb$inl$ and \verb$inr$. Its iterator was called
\verb$caseo$, it said that for any type \verb$C$ and elements \verb$A → C$
and \verb$B → C$, there is a function from \verb$A +o B$ to \verb$C$.

Natural numbers are also an inductive type: the constructors are
\verb$zero$ and \verb$suc$, the latter can be written in several
different equivalent ways:
\begin{verbatim}
suc : Tm Γ Nat → Tm Γ Nat
suc : Tm Γ (Nat ⇒ Nat)
suc : Tm (Γ ▹ Nat) Nat
\end{verbatim}
When specifying the arguments of the iterator, we use the last version
which does not refer to metatheoretic or object theoretic function space.
So the iterator says that given a type \verb$C$, a term in \verb$Tm Γ C$
and a term in \verb$Tm (Γ ▹ C) C$, we get a function from \verb$Tm Γ Nat$ to
\verb$Tm Γ C$.

A language has natural numbers if it extends the substitution calculus
with the following operators and equations:
\begin{code}
    Nat        : Ty
    zeroo      : ∀{Γ} → Tm Γ Nat
    suco       : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                 iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
    zero[]     : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]      : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]   : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                 iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])
\end{code}
The computation rules \verb$Natβ₁$ and \verb$Natβ₂$ express that
\verb$iteNat u v t$ works as follows: it replaces all \verb$suco$s in
\verb$t$ by what is specified by \verb$v$, and replaces \verb$zeroo$ by
\verb$u$. For example, \verb$iteNat u v (suco (suco (suco zero)))$ is
equal to \verb$v [ id ,o v [ id ,o v [ id ,o u ] ] ]$ (which can be thought about as
\verb$v (v (v u))$ but we use substitution to express the applications).

Gödel's System T is the name of the language which has function space
\verb$⇒$ and natural numbers as above.

The iterator is sometimes called recursor or primitive recursor. We
distinguish the two. The recursor for natural numbers does not only
receive the result of the recursive call, but also the number.
\begin{verbatim}
recNat    : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ Nat ▹ A) A → Tm Γ Nat → Tm Γ A
recNatβ₁  : recNat u v zeroo ≡ u
recNatβ₂  : recNat u v (suco t) ≡ v [ id ,o t ,o recNat u v t ]
\end{verbatim}
For example, with the iterator, it is not possible to define a function \verb$pred : Tm (∙ ▹ Nat) Nat$ s.t.\ \verb$pred [ id ,o suco t ] ≡ t$ for any \verb$t$ (note that \verb$t$ might be a variable e.g.\ \verb$q$) \cite{DBLP:conf/csl/Parigot89}.

\begin{exe}[recommended]
Define the recursor for natural numbers with the help of the iterator. It does not need to (and will not) satisfy \verb$recNatβ₂$. TODO: elaborate.
\end{exe}

In the case of \verb$Bool$ (or other non-recursive inductive types)
there is no difference between the iterator and the recursor.

Our previous definition of \verb$Nat$ (in Section \ref{sec:def})
was not an inductive type because it did not have an iterator or
recursor, only two special cases of the iterator: addition and
\verb$isZero$. Both of these can be defined using the iterator, see
below.

Another inductive type is that of lists. For any type \verb$A$, we have a
type \verb$List A$. It has two constructors \verb$nil : Tm Γ (List A)$ and
\verb$cons$ which again can be expressed in three different ways. We will use
the first one when specifying the constructor and the third one when specifying
the arguments of the iterator.
\begin{verbatim}
cons : Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
cons : Tm Γ (A ⇒ List A ⇒ List A)
cons : Tm (Γ ▹ A ▹ List A) (List A)
\end{verbatim}

A language has lists if it extends the substitution calculus with the following
operators and equations:
\begin{code}
    List       : Ty → Ty
    nil        : ∀{Γ A} → Tm Γ (List A)
    cons       : ∀{Γ A} → Tm Γ A → Tm Γ (List A) → Tm Γ (List A)
    iteList    : ∀{Γ A B} → Tm Γ B → Tm (Γ ▹ A ▹ B) B → Tm Γ (List A) → Tm Γ B
    Listβ₁     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B} → iteList u v nil ≡ u
    Listβ₂     : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t₁ : Tm Γ A}{t : Tm Γ (List A)} →
                 iteList u v (cons t₁ t) ≡ (v [ id ,o t₁ ,o iteList u v t ])
    nil[]      : ∀{Γ A Δ}{γ : Sub Δ Γ} → nil {Γ}{A} [ γ ] ≡ nil {Δ}{A}
    cons[]     : ∀{Γ A}{t₁ : Tm Γ A}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 (cons t₁ t) [ γ ] ≡ cons (t₁ [ γ ]) (t [ γ ])
    iteList[]  : ∀{Γ A B}{u : Tm Γ B}{v : Tm (Γ ▹ A ▹ B) B}{t : Tm Γ (List A)}{Δ}{γ : Sub Δ Γ} →
                 iteList u v t [ γ ] ≡ iteList (u [ γ ]) (v [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (t [ γ ])
\end{code}
In \verb$iteList u v t$, \verb$u$ expresses what to do when \verb$t = nil t'$,
\verb$v$ expresses what to do when \verb$t = cons t' t''$.

Another inductive type is that of binary trees with information at the
leaves. For any type \verb$A$, \verb$Ty A$ is a type, the constructors are
\verb$leaf$ and \verb$node$. A language has these binary trees if it has the
following operators and equations:
\begin{code}
    Tree       : Ty → Ty
    leaf       : ∀{Γ A} → Tm Γ A → Tm Γ (Tree A)
    node       : ∀{Γ A} → Tm Γ (Tree A) → Tm Γ (Tree A) → Tm Γ (Tree A)
    iteTree    : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm (Γ ▹ B ▹ B) B → Tm Γ (Tree A) → Tm Γ B
    Treeβ₁     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{a : Tm Γ A} →
                 iteTree l n (leaf a) ≡ l [ id ,o a ]
    Treeβ₂     : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{ll rr : Tm Γ (Tree A)} →
                 iteTree l n (node ll rr) ≡ n [ id ,o iteTree l n ll ,o iteTree l n rr ]
    leaf[]     : ∀{Γ A}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ} → (leaf a) [ γ ] ≡ leaf (a [ γ ])
    node[]     : ∀{Γ A}{ll rr : Tm Γ (Tree A)}{Δ}{γ : Sub Δ Γ} →
                 (node ll rr) [ γ ] ≡ node (ll [ γ ]) (rr [ γ ])
    iteTree[]  : ∀{Γ A B}{l : Tm (Γ ▹ A) B}{n : Tm (Γ ▹ B ▹ B) B}{t : Tm Γ (Tree A)}
                 {Δ}{γ : Sub Δ Γ} →
                 iteTree l n t [ γ ] ≡
                 iteTree (l [ (γ ⊚ p) ,o q ]) (n [ (γ ⊚ p ⊚ p) ,o (q [ p ]) ,o q ]) (t [ γ ])
\end{code}

\begin{code}[hide]
  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]
  v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
  v0 = q
  v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]
  ▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
  ▹η' {Γ}{A} =
    p ,o q
      ≡⟨ sym {A = Sub (Γ ▹ A) (Γ ▹ A)}
           (cong {A = Sub (Γ ▹ A) Γ × Tm (Γ ▹ A) A} (λ w → π₁ w ,o π₂ w) (idr , [id])) ⟩
    p ⊚ id ,o q [ id ]
      ≡⟨ ▹η ⟩
    id
      ∎

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong (_⊚ δ) ▹β₁ , cong (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎

  ⟦_⟧T : I.Ty → Ty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
    ⟦_⟧t      : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S            ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S            ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S             ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t          ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S           ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S         ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t         ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦lam⟧     : ⟦ I.lam tᴵ ⟧t             ≡ lam ⟦ tᴵ ⟧t
    ⟦$⟧       : ⟦ tᴵ I.$ uᴵ ⟧t            ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}

    ⟦trivial⟧  : ⟦ I.trivial {Γᴵ} ⟧t      ≡ trivial
    ⟦iteUnit⟧  : ⟦ I.iteUnit uᴵ tᴵ ⟧t     ≡ iteUnit ⟦ uᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦trivial⟧ ⟦iteUnit⟧ #-}

    ⟦true⟧     : ⟦ I.true {Γᴵ} ⟧t         ≡ true
    ⟦false⟧    : ⟦ I.false {Γᴵ} ⟧t        ≡ false
    ⟦iteBool⟧  : ⟦ I.iteBool uᴵ vᴵ tᴵ ⟧t  ≡ iteBool ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

    ⟦zero⟧     : ⟦ I.zeroo {Γᴵ} ⟧t        ≡ zeroo
    ⟦suc⟧      : ⟦ I.suco tᴵ ⟧t           ≡ suco ⟦ tᴵ ⟧t
    ⟦iteNat⟧   : ⟦ I.iteNat uᴵ vᴵ tᴵ ⟧t   ≡ iteNat ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    ⟦nil⟧      : ⟦ I.nil {Γᴵ}{Aᴵ} ⟧t      ≡ nil
    ⟦cons⟧     : ⟦ I.cons uᴵ vᴵ ⟧t        ≡ cons ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦iteList⟧  : ⟦ I.iteList uᴵ vᴵ tᴵ ⟧t  ≡ iteList ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦iteList⟧ #-}

    ⟦leaf⟧     : ⟦ I.leaf tᴵ ⟧t           ≡ leaf ⟦ tᴵ ⟧t
    ⟦node⟧     : ⟦ I.node uᴵ vᴵ ⟧t        ≡ node ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦iteTree⟧  : ⟦ I.iteTree uᴵ vᴵ tᴵ ⟧t  ≡ iteTree ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦iteTree⟧ #-}
\end{code}

\begin{code}[hide]
module Examples {i}{j}{k}{l}(M : Model {i}{j}{k}{l}) where
  open Model M
\end{code}
With the iterator we can define the following functions on natural
numbers. Addition with variable names is written
\verb$plus = λ x y . iteNat y (z . suc z) x$
which means that we have a function with two inputs \verb$x$ and \verb$y$,
we do recursion on the first input (\verb$x$), and in the input we replace
\verb$zero$s by \verb$y$, and \verb$suc$s by \verb$suc$. The same information
is expressed in languages with pattern matching as follows.
\begin{verbatim}
plus zero      y = y
plus (suc x') y = suc (plus x' y)
\end{verbatim}
The first line corresponds to the first argument of \verb$iteNat$ (i.e.\ \verb$y$),
the second line corresponds to the second argument in which \verb$z$ is bound
(\verb$z . suc z$) where \verb$z$ refers to the recursive call which we
write \verb$(plus x' y)$ using pattern matching notation.

In our formal
syntax we write this as follows.
\begin{code}
  plus : Tm ∙ (Nat ⇒ Nat ⇒ Nat)
  plus = lam (lam (iteNat v0 (suco v0) v1))
\end{code}

The \verb$isZero$ operation can be defined as follows.
\begin{code}
  isZero : Tm ∙ (Nat ⇒ Bool)
  isZero = lam (iteNat true false v0)
\end{code}
The zero case is simply \verb$true$, the successor case is \verb$false$
regardless of the result of the recursive call.

Examples for lists:
\begin{code}
  isnil : {A : Ty} → Tm ∙ (List A ⇒ Bool)
  isnil = lam (iteList true false v0)

  concat : {A : Ty} → Tm ∙ (List A ⇒ List A ⇒ List A)
  concat = lam (lam (iteList v0 (cons v1 v0) v1))
\end{code}
Concatenation works as follows.
\begin{code}
  concatTest : {A : Ty}{x y z : Tm ∙ A} →
    concat $ cons x (cons y nil) $ cons z nil ≡ cons x (cons y (cons z nil))
\end{code}
\begin{code}[hide]
  concatTest = exercisep
\end{code}
\verb$isnil$ and \verb$concat$ are polymorphic functions in the sense
that they work for any type \verb$A$ but the polymorphism happens in
our metalanguage and not in our object language.


The following two trees in \verb$Tm ∙ (Tree Nat)$

\begin{tikzpicture}
  \node (x10) at (0,0) {};
  \node (x20) at (-0.5,-1) {};
  \node (x21) at (0.5,-1) {\verb$zero$};
  \node (x30) at (-1,-2) {\verb$zero$};
  \node (x31) at (0,-2) {\verb$zero$};
  \draw[-] (x10) edge node {} (x20);
  \draw[-] (x10) edge node {} (x21);
  \draw[-] (x20) edge node {} (x30);
  \draw[-] (x20) edge node {} (x31);
  \node (y10) at (3,0) {};
  \node (y20) at (2.5,-1) {\verb$zero$};
  \node (y21) at (3.5,-1) {};
  \node (y30) at (3,-2) {\verb$zero$};
  \node (y31) at (4,-2) {\verb$zero$};
  \draw[-] (y10) edge node {} (y20);
  \draw[-] (y10) edge node {} (y21);
  \draw[-] (y21) edge node {} (y30);
  \draw[-] (y21) edge node {} (y31);
\end{tikzpicture}

are defined as
\begin{code}[hide]
  treeEx1 treeEx2 : Tm ∙ (Tree Nat)
\end{code}
\begin{code}
  treeEx1 = node (node (leaf zeroo) (leaf zeroo)) (leaf zeroo)
  treeEx2 = node (leaf zeroo) (node (leaf zeroo) (leaf zeroo))
\end{code}

The following function adds all the numbers in the leaves of a tree.
\begin{code}
  sum : Tm ∙ (Tree Nat ⇒ Nat)
  sum = lam (iteTree v0 (plus [ p ⊚ p ⊚ p ] $ v1 $ v0) v0)
\end{code}
With variable names we would write this as
\verb@sum = λ x . iteTree (z . z) (z₁ z₂ . plus $ z₁ $ z₂) x@.
In the formal syntax, we had to weaken \verb$plus$ by \verb$p ∘ p ∘ p$
because it was defined in the empty context above and now we used it
in a context with three free variables (one bound by \verb$lam$, the other
two by \verb$iteTree$).

The following function turns a binary tree into a list:
\begin{code}
  flatten : ∀{A} → Tm ∙ (Tree A ⇒ List A)
  flatten = lam (iteTree (cons v0 nil) (concat [ p ⊚ p ⊚ p ] $ v1 $ v0) v0)
\end{code}

\begin{exe}[compulsory]
  List the rules for inductively defined trees with ternary branching, a boolean at each node and a natural number at each leaf.
\end{exe}
\begin{exe}[compulsory]
  List the rules for inductively defined trees with two kinds of nullary, one kind of binary and three kinds of ternary branching. There is no extra information at leaves or nodes.
\end{exe}
\begin{exe}[compulsory]
  List the rules for inductively defined trees no information at the leaves and infinity (\verb$Nat$-ary) branching.
\end{exe}
\begin{exe}[compulsory]
  For types \verb$A$, \verb$B$, list the rules for the inductive type with only leaves (and no nodes) that contain an \verb$A$ and a \verb$B$. Derive all its rules from binary products of the previous section.
\end{exe}
\begin{exe}[compulsory]
  For types \verb$A$, \verb$B$, list the rules for the inductive type with two kinds of leaves and no nodes. One kind of leaf contains an \verb$A$, the other kind of leaf contains a \verb$B$. Derive all the rules from binary sums of the previous section.
\end{exe}

\section{Standard model}

We define lists and trees using Agda's inductive types.
\begin{code}
data List (A : Set) : Set where
  []  : List A
  _∷_ : A → List A → List A
iteList : {A B : Set} → B → (A → B → B) → List A → B
iteList b f [] = b
iteList b f (a ∷ as) = f a (iteList b f as)

data Tree (A : Set) : Set where
  leaf : A → Tree A
  node : Tree A → Tree A → Tree A
iteTree : {A B : Set} → (A → B) → (B → B → B) → Tree A → B
iteTree f g (leaf a) = f a
iteTree f g (node t t') = g (iteTree f g t) (iteTree f g t')
\end{code}

\begin{code}[hide]
St : Model
St = record
  { Con       = Set
  ; Sub       = λ Δ Γ → Δ → Γ
  ; _⊚_       = λ γ δ θ* → γ (δ θ*)
  ; ass       = λ {Γ}{Δ}{Θ}{Ξ} → refl {A = Ξ → Γ}
  ; id        = λ γ* → γ*
  ; idl       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; idr       = λ {Γ}{Δ} → refl {A = Δ → Γ}

  ; ∙         = Lift ⊤
  ; ε         = _
  ; ∙η        = λ {Γ}{σ} → refl {A = Γ → Lift ⊤}

  ; Ty        = Set

  ; Tm        = λ Γ A → Γ → A
  ; _[_]      = λ a γ δ* → a (γ δ*)
  ; [∘]       = λ {Γ}{Δ}{Θ}{A} → refl {A = Θ → A}
  ; [id]      = λ {Γ}{A}{a} → refl {A = Γ → A}
  ; _▹_       = _×_
  ; _,o_      = λ γ t δ* → γ δ* , t δ*
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; ▹β₂       = λ {Γ}{Δ}{A} → refl {A = Δ → A}
  ; ▹η        = λ {Γ}{Δ}{A} → refl {A = Δ → Γ × A}

  ; _⇒_       = λ A B → A → B
  ; lam       = λ t γ* α* → t (γ* , α*)
  ; _$_       = λ t u γ* → t γ* (u γ*)
  ; ⇒β        = λ {Γ}{A}{B}{t}{u} → refl {A = Γ → B}
  ; ⇒η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A → B}
  ; lam[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A → B}
  ; $[]       = λ {Γ}{A}{B}{t}{u}{Δ}{γ} → refl {A = Δ → B}

  ; Unit      = Lift ⊤
  ; trivial   = λ _ → mk trivi
  ; iteUnit   = λ z _ → z
  ; Unitβ     = λ {Γ}{A}{t} a → refl {x = t a}
  ; trivial[] = λ a → mk trivi
  ; iteUnit[] = λ {Γ}{A}{t}{u} a → refl {A = A}

  ; Bool      = 𝟚
  ; true      = λ _ → tt
  ; false     = λ _ → ff
  ; iteBool   = λ u v t γ* → if t γ* then u γ* else v γ*
  ; Boolβ₁    = λ {Γ}{A} → refl {A = Γ → A}
  ; Boolβ₂    = λ {Γ}{A} → refl {A = Γ → A}
  ; true[]    = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → tt}
  ; false[]   = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → ff}
  ; iteBool[] = λ {Γ}{A}{t}{u}{v}{Δ}{γ} → refl {A = Δ → A}
\end{code}

The new components in the standard model:
\begin{code}
  ; Nat        = ℕ
  ; zeroo      = λ _ → zero
  ; suco       = λ t γ* → suc (t γ*)
  ; iteNat     = λ u v t γ* → rec (u γ*) (λ x → v (γ* , x)) (t γ*)
  ; Natβ₁      = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
  ; Natβ₂      = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
  ; zero[]     = λ {Γ}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ _ → zero}
  ; suc[]      = λ {Γ}{t}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ δ* → suc (t (γ δ*))}
  ; iteNat[]   = λ {Γ}{A}{u}{v}{t}{Δ}{γ} → refl {A = Δ → A}

  ; List       = List
  ; nil        = λ _ → []
  ; cons       = λ u v γ* → u γ* ∷ v γ*
  ; iteList    = λ u v t γ* → iteList (u γ*) (λ x y → v ((γ* , x) , y)) (t γ*)
  ; Listβ₁     = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → B}
  ; Listβ₂     = λ {Γ}{A}{B}{u}{v}{a}{t} → refl {A = Γ → B}
  ; nil[]      = λ {Γ}{A}{Δ}{γ} → refl {A = Δ → List A}
  ; cons[]     = λ {Γ}{A}{u}{v}{Δ}{γ} → refl {A = Δ → List A}
  ; iteList[]  = λ {Γ}{A}{B}{u}{v}{t}{Δ}{γ} → refl {A = Δ → B}

  ; Tree       = Tree
  ; leaf       = λ t γ* → leaf (t γ*)
  ; node       = λ u v γ* → node (u γ*) (v γ*)
  ; iteTree    = λ {Γ}{A}{B} u v t γ* → iteTree (λ x → u (γ* , x)) (λ x y → v ((γ* , x) , y)) (t γ*)
  ; Treeβ₁     = λ {Γ}{A}{B}{u}{v}{t} → refl {A = Γ → B}
  ; Treeβ₂     = λ {Γ}{A}{B}{u}{v}{t}{t'} → refl {A = Γ → B}
  ; leaf[]     = λ {Γ}{A}{a}{Δ}{γ} → refl {A = Δ → Tree A}
  ; node[]     = λ {Γ}{A}{u}{v}{Δ}{γ} → refl {A = Δ → Tree A}
  ; iteTree[]  = λ {Γ}{A}{B}{u}{v}{t}{Δ}{γ} → refl {A = Δ → B}
\end{code}
\begin{code}[hide]
  }
module St = Model St
\end{code}

As we explained in Subsection \ref{sec:natbool-standard}, from the
standard model we obtain that \verb$true ≠ false$ and \verb$zero ≠ suc t$ in the syntax for any
\verb$t$.
\begin{code}
true≠false : ¬ (I.true {I.∙} ≡ I.false)
true≠false = λ e → cong St.⟦_⟧t e (mk trivi) (mk trivi) (mk trivi)

zero≠suc : ∀{t} → ¬ (I.zeroo {I.∙} ≡ I.suco t)
zero≠suc = λ e → cong St.⟦_⟧t e (mk trivi) (mk trivi) (mk trivi)

postulate
  t≠suct : ∀{t} → ¬ (t ≡ I.suco {I.∙} t)
\end{code}

TODO: show that these hold in any context.


\section{Definable functions on natural numbers}

The Ackermann function is not definable using first-order primitive
recursion (that is, in Def + natural numbers as a \verb$Tm (∙ ▹ Nat) Nat$).
But it is definable in our language because we have higher
order functions \cite[Section 9.3]{harper}. Here is its definition in
Agda:
\begin{code}
ack : (x y : ℕ) -> ℕ
ack zero n = 1 + n
ack (suc m) zero = ack m 1
ack (suc m) (suc n) = ack m (ack (suc m) n)
\end{code}

\begin{code}
module Definability where
  open I

  ⌜_⌝ : ℕ → Tm ∙ Nat
  ⌜ zero ⌝ = zeroo
  ⌜ suc n ⌝ = suco ⌜ n ⌝

  Definable : (ℕ → ℕ) → Set
  Definable f = Σ (Tm ∙ (Nat ⇒ Nat)) λ t → (n : ℕ) → Lift (t $ ⌜ n ⌝ ≡ ⌜ f n ⌝)

  definable2* : Definable (rec zero (λ x → suc (suc x)))
  definable2* = (lam (iteNat zeroo (suco (suco q)) q)) , λ n → mk (
    lam (iteNat zeroo (suco (suco q)) q) $ ⌜ n ⌝
        ≡⟨ ⇒β ⟩
    iteNat zeroo (suco (suco q)) q [ id ,o ⌜ n ⌝ ]
        ≡⟨ iteNat[] ⟩
    iteNat (zeroo [ id ,o ⌜ n ⌝ ]) (suco (suco q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat x (suco (suco q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])) zero[] ⟩
    iteNat zeroo (suco (suco q) [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroo x (q [ id ,o ⌜ n ⌝ ])) suc[] ⟩
    iteNat zeroo (suco (suco q [ (id ,o ⌜ n ⌝) ⊚ p ,o q ])) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroo (suco x) (q [ id ,o ⌜ n ⌝ ])) suc[] ⟩
    iteNat zeroo (suco (suco (q [ (id ,o ⌜ n ⌝) ⊚ p ,o q ]))) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (λ x → iteNat zeroo (suco (suco x)) (q [ id ,o ⌜ n ⌝ ])) ▹β₂ ⟩
    iteNat zeroo (suco (suco q)) (q [ id ,o ⌜ n ⌝ ])
        ≡⟨ cong (iteNat zeroo (suco (suco q))) ▹β₂ ⟩
    iteNat zeroo (suco (suco q)) ⌜ n ⌝
        ≡⟨ sym {A = Tm _ _} (⌜rec⌝ n) ⟩
      ⌜ rec zero (λ x → suc (suc x)) n ⌝
        ∎)
    where
      -- TODO: obtain this from normalisation:
      ⌜rec⌝ : (n : ℕ) → ⌜ rec zero (λ x → suc (suc x)) n ⌝ ≡ iteNat zeroo (suco (suco q)) ⌜ n ⌝
      ⌜rec⌝ zero = sym {A = Tm _ _} Natβ₁
      ⌜rec⌝ (suc n) =
        suco (suco ⌜ rec zero (λ x → suc (suc x)) n ⌝)
          ≡⟨ cong {A = Tm _ _}(λ x → suco (suco x)) (⌜rec⌝ n) ⟩
        suco (suco (iteNat zeroo (suco (suco q)) ⌜ n ⌝))
          ≡⟨ cong {A = Tm _ _}(λ x → suco (suco x)) (sym {A = Tm _ _} ▹β₂) ⟩
        suco (suco (q [ id ,o iteNat zeroo (suco (suco q)) ⌜ n ⌝ ]))
          ≡⟨ cong suco (sym {A = Tm _ _} suc[]) ⟩
        suco (suco q [ id ,o iteNat zeroo (suco (suco q)) ⌜ n ⌝ ])
          ≡⟨ sym {A = Tm _ _} suc[] ⟩
        suco (suco q) [ id ,o iteNat zeroo (suco (suco q)) ⌜ n ⌝ ]
          ≡⟨ sym {A = Tm _ _} Natβ₂ ⟩
        iteNat zeroo (suco (suco q)) (suco ⌜ n ⌝)
          ∎

  Definable' : (ℕ → ℕ → ℕ) → Set
  Definable' f = Σ (Tm ∙ (Nat ⇒ Nat ⇒ Nat)) λ t → (m n : ℕ) → Lift (t $ ⌜ m ⌝ $ ⌜ n ⌝ ≡ ⌜ f m n ⌝)

  postulate
    code : Tm ∙ (Nat ⇒ Nat) → ℕ
    eval : ℕ → ℕ → ℕ
    evalProp : (t : Tm ∙ (Nat ⇒ Nat))(n : ℕ) → t $ ⌜ n ⌝ ≡ ⌜ eval (code t) n ⌝

  evalNotDefinable : Definable' eval → Lift ⊥
  evalNotDefinable (t , f) = mk (t≠suct (
     ⌜ eval (code u) (code u) ⌝
       ≡⟨ sym {A = Tm _ _} (evalProp u (code u)) ⟩
     u $ ⌜ code u ⌝
       ≡⟨ ⇒β ⟩
     suco (t [ p ] $ q $ q) [ id ,o ⌜ code u ⌝ ]
       ≡⟨ suc[] ⟩
     suco ((t [ p ] $ q $ q) [ id ,o ⌜ code u ⌝ ])
       ≡⟨ cong suco $[] ⟩
     suco (((t [ p ] $ q) [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ]))
       ≡⟨ cong (λ x → suco (x $ q [ id ,o ⌜ code u ⌝ ])) $[] ⟩
     suco ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ] $ q [ id ,o ⌜ code u ⌝ ]))
       ≡⟨ cong (λ x → suco ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ x $ x))) ▹β₂ ⟩
     suco ((t [ p ] [ id ,o ⌜ code u ⌝ ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → suco ((x $ ⌜ code u ⌝ $ ⌜ code u ⌝))) (sym {A = Tm _ _} [∘]) ⟩
     suco ((t [ p ⊚ (id ,o ⌜ code u ⌝) ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → suco ((t [ x ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))) ▹β₁ ⟩
     suco ((t [ id ] $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong (λ x → suco ((x $ ⌜ code u ⌝ $ ⌜ code u ⌝))) [id] ⟩
     suco ((t $ ⌜ code u ⌝ $ ⌜ code u ⌝))
       ≡⟨ cong suco (un (f (code u) (code u))) ⟩
     suco ⌜ eval (code u) (code u) ⌝
       ∎))
    where
      u : Tm ∙ (Nat ⇒ Nat)
      u = lam (suco (t [ p ] $ q $ q))
\end{code}
