{-# OPTIONS --prop --rewriting #-}
module SKDec where

open import Lib
open import Agda.Primitive using (_⊔_)

record Model {i j} : Set (lsuc (i ⊔ j)) where
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Ty  : Set i
    ι   : Ty
    _⇒_ : Ty → Ty → Ty
    Tm  : Ty → Set j
    _$_ : ∀{A B}    → Tm (A ⇒ B) → Tm A → Tm B
    K   : ∀{A B}    → Tm (A ⇒ B ⇒ A)
    S   : ∀{A B C}  → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
    Kβ  : ∀{A B}{t : Tm A}{u : Tm B} → K $ t $ u ≡ t
    Sβ  : ∀{A B C}{t : Tm (A ⇒ B ⇒ C)}{u : Tm (A ⇒ B)}{v : Tm A} →
          S $ t $ u $ v ≡ t $ v $ (u $ v)

postulate
  M : Model {lzero}{lzero}
open Model M
variable
  A B C D A' B' C' D' : Ty
  t u v d : Tm A

data _≣_ {i}{A : Set i}(a : A) : A → Set i where
  refl' : a ≣ a
infix  4 _≣_
tr : ∀{i j}{A : Set i}(P : A → Set j){a a' : A}(a= : a ≣ a') → P a → P a'
tr P refl' u = u

data Nf (D : Ty)(d : Tm D) : Set where
  K₀ : (e : A ⇒ B ⇒ A                       ≣ D)(e' : tr Tm e K       ≣ d) →          Nf D d
  K₁ : (e : B ⇒ A                           ≣ D)(e' : tr Tm e (K $ t) ≣ d) → Nf A t → Nf D d
  S₀ : (e : ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) ≣ D)(e' : tr Tm e S       ≣ d)          → Nf D d

disjK₀K₁ :
  (e₀ : A ⇒ B ⇒ A                       ≣ D)(e'₀ : tr Tm e₀ K       ≣ d)
  (e₁ : B ⇒ A                           ≣ D)(e'₁ : tr Tm e₁ (K $ t) ≣ d)(n : Nf A t) →
  K₀ e₀ e'₀ ≣ K₁ e₁ e'₁ n → ⊥
disjK₀K₁ _ _ _ _ _ ()

dec : (u v : Nf D d) → Dec (u ≣ v)
dec (K₀ {A₀}{B₀} e₀ e'₀) (K₀ {A₁}{B₁} e₁ e'₁) = ι₁ {!!}
dec (K₀ e e') (K₁ e₁ e'' v) = ι₂ λ ()
dec (K₀ e e') (S₀ e₁ e'') = {!!}
dec (K₁ e e' u) (K₀ e₁ e'') = {!!}
dec (K₁ e e' u) (K₁ e₁ e'' v) = {!!}
dec (K₁ e e' u) (S₀ e₁ e'') = {!!}
dec (S₀ e e') (K₀ e₁ e'') = {!!}
dec (S₀ e e') (K₁ e₁ e'' v) = {!!}
dec (S₀ e e') (S₀ e₁ e'') = {!!}


{-
data Nf : (A : Ty) → Tm A → Set where
  K₀ : Nf (A ⇒ B ⇒ A) K
  K₁ : Nf A t → Nf (B ⇒ A) (K $ t)
  S₀ : Nf ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C) S
  S₁ : Nf (A ⇒ B ⇒ C) t → Nf ((A ⇒ B) ⇒ A ⇒ C) (S $ t)
  S₂ : Nf (A ⇒ B ⇒ C) t → Nf (A ⇒ B) u → Nf (A ⇒ C) (S $ t $ u)
-}

{-
-- dec : {uᴵ vᴵ : I.Tm I.A}(nu : Nf I.A uᴵ)(nv : Nf I.A vᴵ) → Dec (Σ (uᴵ ≣ vᴵ) λ e → tr (Nf I.A) e nu ≣ nv)

,= : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}(a= : a ≣ a'){b : B a}{b' : B a'}(b= : tr B a= b ≣ b') → _≣_ {A = Σ A B} (a , b) (a' , b')
,= refl' refl' = refl'

disj : {nv : Nf B v}
  (e : (A I.⇒ A' I.⇒ A) ≣ (B' I.⇒ B))
  (e' : (tr I.Tm e (I.K {A}{A'})) ≣ (I.K {B}{B'} I.$ v)) →
  (e'' : tr (λ w → Nf (π₁ w) (π₂ w)) {(A I.⇒ A' I.⇒ A) , I.K}{(B' I.⇒ B) , (I.K {B}{B'} I.$ v)} (,= e e') (K₀ {A}{A'}) ≣ K₁ {B} nv) →
  ⊥
disj refl' e' e'' = {!!}

\begin{code}[hide]
-- Decidability of equality:
{-
dec : {uᴵ vᴵ : I.Tm I.A}(nu : Nf I.A uᴵ)(nv : Nf I.A vᴵ) → Lift (Σp (uᴵ ≡ vᴵ) λ e → (Nf I.A ~) e nu nv) ⊎ Lift (¬ (Σp (uᴵ ≡ vᴵ) λ e → (Nf I.A ~) e nu nv))
dec K₀ K₀ = ι₁ (mk (refl {x = I.K} , refl {x = K₀}))
dec (K₀ {A}{B}) (K₁ {t = v} nv) = ι₂ {!!} -- (mk λ { (e , e') → Jp {A = I.Tm (A I.⇒ B I.⇒ A)}(λ {v'} e' → (nv' : Nf _ _) → (Nf (A I.⇒ B I.⇒ A) ~) e' K₀ (K₁ nv') → ⊥) {!!} e (K₁ nv) e' })
dec K₀ (S₁ nv) = {!!}
dec K₀ (S₂ nv nv₁) = {!!}
dec (K₁ nu) K₀ = {!!}
dec (K₁ nu) (K₁ nv) = {!!}
dec (K₁ nu) S₀ = {!!}
dec (K₁ nu) (S₁ nv) = {!!}
dec (K₁ nu) (S₂ nv nv₁) = {!!}
dec S₀ (K₁ nv) = {!!}
dec S₀ S₀ = {!!}
dec S₀ (S₂ nv nv₁) = {!!}
dec (S₁ nu) K₀ = {!!}
dec (S₁ nu) (K₁ nv) = {!!}
dec (S₁ nu) (S₁ nv) = {!!}
dec (S₁ nu) (S₂ nv nv₁) = {!!}
dec (S₂ nu nu₁) K₀ = {!!}
dec (S₂ nu nu₁) (K₁ nv) = {!!}
dec (S₂ nu nu₁) S₀ = {!!}
dec (S₂ nu nu₁) (S₁ nv) = {!!}
dec (S₂ nu nu₁) (S₂ nv nv₁) = {!!}
-}
-- dec : {uᴵ : I.Tm I.A}{vᴵ : I.Tm I.B}(nu : Nf I.A uᴵ)(nv : Nf I.B vᴵ) → Dec (Lift (Σp (I.A ≡ I.B) λ e → Σp ((I.Tm ~) e uᴵ vᴵ) λ e' → _~ {A = Σ I.Ty I.Tm}(λ w → Nf (π₁ w) (π₂ w))(e , e') nu nv))
-- dec : {uᴵ : I.Tm I.A}(n n' : Nf I.A uᴵ) → Lift (n ≡ n') ⊎ Lift (¬ (n ≡ n'))
-- dec n n' = {!n n'!}
-- dec : (a b : Σ I.Ty λ Aᴵ → Σ (I.Tm Aᴵ) (Nf Aᴵ)) → (Lift (a ≡ b)) ⊎ Lift (¬ (a ≡ b))
{-
-- we first have to prove disjointness of Nf-constructors, this should be almost automatic
contra : {u : I.Tm I.D}(n : Nf I.D u) →
  (eTy : (I.A I.⇒ I.B I.⇒ I.A) ≡ (I.C I.⇒ I.D))
  (eTm : (I.Tm ~) eTy I.K (I.K I.$ u))
  (eNf : _~ {A = Σ I.Ty I.Tm} (λ w → Nf (π₁ w) (π₂ w)) (eTy , eTm) K₀ (K₁ n)) →
  ⊥
contra = {!!}
-- we should be able to define a function which maps K₀ to ⊤ and K₁ n to ⊥ and use that to derive this contradiction without relying on I being initial
-}
\end{code}

-}
