\chapter{Function space}
\label{ch:STT}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Rules for function space. Constructor and destructor operators, computation and uniqueness rules. Currying. Relationship of metatheoretic and object theoretic function space.
\end{tcolorbox}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module STT where

open import Lib

module I where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _+o_

  data Ty   : Set where
    Nat     : Ty
    Bool    : Ty
    _⇒_     : Ty → Ty → Ty

  data Con  : Set where
    ∙       : Con
    _▹_     : Con → Ty → Con

  postulate
    Sub       : Con → Con → Set
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm        : Con → Ty → Set
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
    iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    ite[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

    num       : ∀{Γ} → ℕ → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
    isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
    +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
    num[]     : ∀{Γ n Δ}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
    isZero[]  : ∀{Γ t Δ}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
    +[]       : ∀{Γ u v Δ}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])

    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong {A = Sub _ _} (_⊚ δ) ▹β₁ , cong {A = Tm _ _} (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎

variable
  Aᴵ Bᴵ : I.Ty
  Γᴵ Δᴵ Θᴵ : I.Con
  γᴵ δᴵ θᴵ σᴵ γaᴵ : I.Sub Δᴵ Γᴵ
  tᴵ uᴵ vᴵ : I.Tm Γᴵ Aᴵ

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Con       : Set i
    Sub       : Con → Con → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} →
                (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Ty        : Set k

    Tm        : Con → Ty → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →
                t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_       : Con → Ty → Con
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    Bool      : Ty
    true      : ∀{Γ} → Tm Γ Bool
    false     : ∀{Γ} → Tm Γ Bool
    ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
    iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
    iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    ite[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

    Nat       : Ty
    num       : ∀{Γ} → ℕ → Tm Γ Nat
    isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
    _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
    isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
    isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
    +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
    num[]     : ∀{Γ n Δ}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
    isZero[]  : ∀{Γ t Δ}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
    +[]       : ∀{Γ u v Δ}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])
\end{code}
An STT model is a Def model which has the following additional operators and equations.
\begin{code}
    _⇒_       : Ty → Ty → Ty
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]
\end{code}
The operator \verb$_⇒_$ is right associative, e.g.\ \verb$A ⇒ B ⇒ C = A ⇒ (B ⇒ C)$.
In an STT model, types are binary trees with \verb$Nat$ or \verb$Bool$ at the leaves.

In STT, there are three ways to form types: \verb$Bool$, \verb$Nat$, and for any two types \verb$A$ and \verb$B$, we also have a type \verb$A ⇒ B$.
The operators and equations for each such way to form types can be grouped as follows.
\begin{itemize}
\item type introduction: (type formation, type former) the operator which constructs the given type (\verb$_⇒_$ for function space),
\item constructors (introduction principle): operators which introduce elements of the type (\verb$lam$),
\item destructors (elimination principle, eliminator): operators that can be used to eliminate an element of the type (\verb._$_.),
\item computation (\verb$β$) rules: equations that explain what happens if a destructor is applied to a constructor (\verb$⇒β$),
\item uniqueness (\verb$η$) rules: equations that explain what happens if a constructor is applied to a destructor (\verb$⇒η$),
\item substitution rules: explain how instantiation of substitution \verb$_[_]$ interacts with the operators (we only need the equation \verb$lam[]$ as the one for \verb._$_. can be proven, see below).
\end{itemize}

\begin{exe}[compulsory]
Group the operators and equations for \verb$Bool$ and \verb$Nat$ as above (hints: there are no uniqueness rules; to decide whether \verb$isZero$ is a constructor for \verb$Bool$ or a destructor for \verb$Nat$, see the associated computation rule).
\end{exe}

The \emph{main arguments} of a destructor are those which have to be
in constructor form so that the computation rule applies. For example,
the only argument of \verb$isZero$ is a main argument, both arguments
of \verb$+o$ are main arguments, and only the first argument of
\verb$ite$ is a main argument.

\begin{code}[hide]
  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]
  v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
  v0 = q
  v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]
  ▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
  ▹η' {Γ}{A} =
    p ,o q
      ≡⟨ sym {A = Sub (Γ ▹ A) (Γ ▹ A)}
           (cong {A = Sub (Γ ▹ A) Γ × Tm (Γ ▹ A) A} (λ w → π₁ w ,o π₂ w) (idr , [id])) ⟩
    p ⊚ id ,o q [ id ]
      ≡⟨ ▹η ⟩
    id
      ∎

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong (_⊚ δ) ▹β₁ , cong (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎

  ^∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{Θ}{δ : Sub Θ Δ}{t : Tm Θ A} →
    (γ ⊚ p ,o q) ⊚ (δ ,o t) ≡ (γ ⊚ δ ,o t)
  ^∘ {Γ}{Δ}{γ}{A}{Θ}{δ}{t} =
    (γ ⊚ p ,o q) ⊚ (δ ,o t)
      ≡⟨ ,∘ ⟩
    (γ ⊚ p ⊚ (δ ,o t) ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → (x ,o q [ δ ,o t ])) ass ⟩
    (γ ⊚ (p ⊚ (δ ,o t)) ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → (γ ⊚ x ,o q [ δ ,o t ])) ▹β₁ ⟩
    (γ ⊚ δ ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → γ ⊚ δ ,o x) ▹β₂ ⟩
    (γ ⊚ δ ,o t)
      ∎

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
    ⟦_⟧t      : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S       ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S       ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S        ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t     ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S      ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S    ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t    ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦true⟧    : ⟦ I.true {Γᴵ} ⟧t     ≡ true
    ⟦false⟧   : ⟦ I.false {Γᴵ} ⟧t    ≡ false
    ⟦ite⟧     : ⟦ I.ite tᴵ uᴵ vᴵ ⟧t  ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦num⟧     : ⟦ I.num {Γᴵ} n ⟧t    ≡ num n
    ⟦isZero⟧  : ⟦ I.isZero tᴵ ⟧t     ≡ isZero ⟦ tᴵ ⟧t
    ⟦+⟧       : ⟦ uᴵ I.+o vᴵ ⟧t      ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
    {-# REWRITE ⟦num⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦lam⟧     : ⟦ I.lam tᴵ ⟧t        ≡ lam ⟦ tᴵ ⟧t
    ⟦$⟧       : ⟦ tᴵ I.$ uᴵ ⟧t       ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}

record DepModel {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Con       : I.Con → Set i
    Sub       : Con Δᴵ → Con Γᴵ → I.Sub Δᴵ Γᴵ → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ γᴵ → Sub Θ Δ δᴵ → Sub Θ Γ (γᴵ I.⊚ δᴵ)
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ γᴵ}{δ : Sub Θ Δ δᴵ}{θ : Sub Ξ Θ θᴵ} →
                (Sub Ξ Γ ~) I.ass ((γ ⊚ δ) ⊚ θ) (γ ⊚ (δ ⊚ θ))
    id        : ∀{Γ} → Sub Γ Γ (I.id {Γᴵ})
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Sub Δ Γ ~) I.idl (id ⊚ γ) γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Sub Δ Γ ~) I.idr (γ ⊚ id) γ

    ∙         : Con I.∙
    ε         : ∀{Γ} → Sub Γ ∙ (I.ε {Γᴵ})
    ∙η        : ∀{Γ}{σ : Sub Γ ∙ σᴵ} → (Sub Γ ∙ ~) I.∙η σ ε

    Ty        : I.Ty → Set k

    Tm        : Con Γᴵ → Ty Aᴵ → I.Tm Γᴵ Aᴵ → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A tᴵ → Sub Δ Γ γᴵ → Tm Δ A (tᴵ I.[ γᴵ ])
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A tᴵ}{γ : Sub Δ Γ γᴵ}{δ : Sub Θ Δ δᴵ} →
                (Tm Θ A ~) I.[∘] (t [ γ ⊚ δ ]) (t [ γ ] [ δ ])
    [id]      : ∀{Γ A}{t : Tm Γ A tᴵ} → (Tm Γ A ~) I.[id] (t [ id ]) t
    _▹_       : Con Γᴵ → Ty Aᴵ → Con (Γᴵ I.▹ Aᴵ)
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ γᴵ → Tm Δ A tᴵ → Sub Δ (Γ ▹ A) (γᴵ I.,o tᴵ)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ (I.p {Γᴵ}{Aᴵ})
    q         : ∀{Γ A} → Tm (Γ ▹ A) A (I.q {Γᴵ}{Aᴵ})
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ γᴵ}{t : Tm Δ A tᴵ} → (Sub Δ Γ ~) I.▹β₁ (p ⊚ (γ ,o t)) γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ γᴵ}{t : Tm Δ A tᴵ} → (Tm Δ A ~) I.▹β₂ (q [ γ ,o t ]) t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A) γaᴵ} → (Sub Δ (Γ ▹ A) ~) I.▹η (p ⊚ γa ,o q [ γa ]) γa

    Bool      : Ty I.Bool
    true      : ∀{Γ} → Tm Γ Bool (I.true {Γᴵ})
    false     : ∀{Γ} → Tm Γ Bool (I.false {Γᴵ})
    ite       : ∀{Γ A} → Tm Γ Bool tᴵ → Tm Γ A uᴵ → Tm Γ A vᴵ → Tm Γ A (I.ite tᴵ uᴵ vᴵ)
    iteβ₁     : ∀{Γ}{A : Ty Aᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ} → (Tm Γ A ~) I.iteβ₁ (ite true u v) u
    iteβ₂     : ∀{Γ}{A : Ty Aᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ} → (Tm Γ A ~) I.iteβ₂ (ite false u v) v
    true[]    : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Tm Δ Bool ~) I.true[] (true [ γ ]) true
    false[]   : ∀{Γ Δ}{γ : Sub Δ Γ γᴵ} → (Tm Δ Bool ~) I.false[] (false [ γ ]) false
    ite[]     : ∀{Γ A}{t : Tm Γ Bool tᴵ}{u : Tm Γ A uᴵ}{v : Tm Γ A vᴵ}{Δ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ A ~) I.ite[] ((ite t u v) [ γ ]) (ite (t [ γ ]) (u [ γ ]) (v [ γ ]))

    Nat       : Ty I.Nat
    num       : ∀{Γ}(n : ℕ) → Tm Γ Nat (I.num {Γᴵ} n)
    isZero    : ∀{Γ} → Tm Γ Nat tᴵ → Tm Γ Bool (I.isZero tᴵ)
    _+o_      : ∀{Γ} → Tm Γ Nat uᴵ → Tm Γ Nat vᴵ → Tm Γ Nat (uᴵ I.+o vᴵ)
    isZeroβ₁  : ∀{Γ} → (Tm Γ Bool ~) I.isZeroβ₁ (isZero (num {Γᴵ}{Γ} 0)) true
    isZeroβ₂  : ∀{Γ n} → (Tm Γ Bool ~) I.isZeroβ₂ (isZero (num {Γᴵ}{Γ} (1 + n))) false
    +β        : ∀{Γ m n} → (Tm Γ Nat ~) I.+β (num {Γᴵ}{Γ} m +o num n) (num (m + n))
    num[]     : ∀{Γ n Δ}{γ : Sub Δ Γ γᴵ} → (Tm Δ Nat ~) I.num[] (num n [ γ ]) (num n)
    isZero[]  : ∀{Γ}{t : Tm Γ Nat tᴵ}{Δ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ Bool ~) I.isZero[] (isZero t [ γ ]) (isZero (t [ γ ]))
    +[]       : ∀{Γ}{u : Tm Γ Nat uᴵ}{v : Tm Γ Nat vᴵ}{Δ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ Nat ~) I.+[] ((u +o v) [ γ ]) ((u [ γ ]) +o (v [ γ ]))

    _⇒_       : Ty Aᴵ → Ty Bᴵ → Ty (Aᴵ I.⇒ Bᴵ)
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B tᴵ → Tm Γ (A ⇒ B) (I.lam tᴵ)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) tᴵ → Tm Γ A uᴵ → Tm Γ B (tᴵ I.$ uᴵ)
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B tᴵ}{u : Tm Γ A uᴵ} → (Tm Γ B ~) I.⇒β (lam t $ u) (t [ id ,o u ])
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B) tᴵ} → (Tm Γ (A ⇒ B) ~) I.⇒η (lam (t [ p ] $ q)) t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B tᴵ}{Δ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ (A ⇒ B) ~) I.lam[] ((lam t) [ γ ]) (lam (t [ γ ⊚ p ,o q ]))
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B) tᴵ}{u : Tm Γ A uᴵ}{Δ}{γ : Sub Δ Γ γᴵ} →
                (Tm Δ B ~) I.$[] ((t $ u) [ γ ]) (t [ γ ] $ u [ γ ])

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : (γᴵ : I.Sub  Δᴵ  Γᴵ)  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C  γᴵ
    ⟦_⟧t      : (tᴵ : I.Tm   Γᴵ  Aᴵ)  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T  tᴵ
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S       ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S       ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S        ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t     ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S      ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S    ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t    ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦true⟧    : ⟦ I.true {Γᴵ} ⟧t     ≡ true
    ⟦false⟧   : ⟦ I.false {Γᴵ} ⟧t    ≡ false
    ⟦ite⟧     : ⟦ I.ite tᴵ uᴵ vᴵ ⟧t  ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦num⟧     : ⟦ I.num {Γᴵ} n ⟧t    ≡ num n
    ⟦isZero⟧  : ⟦ I.isZero tᴵ ⟧t     ≡ isZero ⟦ tᴵ ⟧t
    ⟦+⟧       : ⟦ uᴵ I.+o vᴵ ⟧t      ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
    {-# REWRITE ⟦num⟧ ⟦isZero⟧ ⟦+⟧ #-}

    ⟦lam⟧     : ⟦ I.lam tᴵ ⟧t        ≡ lam ⟦ tᴵ ⟧t
    ⟦$⟧       : ⟦ tᴵ I.$ uᴵ ⟧t       ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}
\end{code}

\begin{code}[hide]
module ChurchEncodings where
  open I
  BoolCh : Ty → Ty
  BoolCh A = A ⇒ A ⇒ A

  NatCh : Ty → Ty
  NatCh A = (A ⇒ A) ⇒ A ⇒ A

  trueCh : ∀ {Γ A} → Tm Γ (BoolCh A)
  trueCh = lam (lam v1)

  falseCh : ∀ {Γ A} → Tm Γ (BoolCh A)
  falseCh = lam (lam q)

  iteCh : ∀ {Γ A} → Tm Γ (BoolCh A ⇒ A ⇒ A ⇒ A)
  iteCh = lam q

  zeroCh : ∀ {Γ A} → Tm Γ (NatCh A)
  zeroCh = lam (lam q)

  sucCh : ∀ {Γ A} → Tm Γ (NatCh A ⇒ NatCh A)
  sucCh = lam (lam (lam (v1 $ (v2 $ v1 $ q))))

  plusCh : ∀ {Γ A} → Tm Γ (NatCh A ⇒ NatCh A ⇒ NatCh A)
  plusCh = lam (lam (lam (lam (v3 $ v1 $ (v2 $ v1 $ q)))))

module Examples where
  open I
\end{code}
The function that adds 2 to its input is given as follows.
\begin{code}
  add2 : Tm ∙ (Nat ⇒ Nat)
  add2 = lam (v0 +o num 2) -- λx.x+2
\end{code}
Note that \verb$isZero$ does not have a function type (in fact, we introduced \verb$isZero$ before
\verb$_⇒_$). But we can define a function which acts like \verb$isZero$ as follows.
\begin{code}
  isZero' : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
  isZero' = lam (isZero v0) -- λx.isZero x
\end{code}
It obeys a computation rule similar to \verb$isZeroβ₁$ which is admissible:
\begin{code}
  isZero'β₁ : ∀{Γ} → isZero' {Γ} $ num 0 ≡ true
  isZero'β₁ =
    isZero' $ num 0
      ≡⟨ refl {x = isZero' $ num 0} ⟩
    lam (isZero v0) $ num 0
      ≡⟨ ⇒β ⟩
    isZero v0 [ id ,o num 0 ]
      ≡⟨ isZero[] ⟩
    isZero (v0 [ id ,o num 0 ])
      ≡⟨ cong isZero ▹β₂ ⟩
    isZero (num 0)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎
\end{code}
\begin{exe}[compulsory]
Prove the other computation rule:
{\normalfont
\begin{code}
  isZero'β₂ : ∀{Γ} → isZero' {Γ} $ num 1 ≡ false
\end{code}
\begin{code}[hide]
  isZero'β₂ = exercisep
\end{code}
}
\end{exe}
The function that constantly returns true is the following. Its domain type can be anything.
\begin{code}
  constTrue : Tm ∙ (Aᴵ ⇒ Bool)
  constTrue = lam true -- λx.true
\end{code}
A constant function is really constant:
\begin{code}
  constIsConst : ∀{Γ A B}{t : Tm Γ B}{u : Tm Γ A} → lam (t [ p ]) $ u ≡ t
  constIsConst {t = t}{u = u} =
    lam (t [ p ]) $ u
      ≡⟨ ⇒β ⟩
    t [ p ] [ id ,o u ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    t [ p ⊚ (id ,o u) ]
      ≡⟨ cong {A = Sub _ _} (t [_]) ▹β₁  ⟩
    t [ id ]
      ≡⟨ [id] ⟩
    t
      ∎
\end{code}
Functions with multiple parameters can be defined using the so-called
\emph{currying}. A function whose input is a \verb$Nat$ and a \verb$Bool$ and whose output is a \verb$Nat$ is the same
as a function whose domain is \verb$Nat$ and whose codomain is \verb$Bool ⇒ Nat$:
\begin{code}
  curried : Tm ∙ (Nat ⇒ (Bool ⇒ Nat))
  curried = lam (lam (v1 +o ite v0 (num 1) (num 2))) -- λ x y . x + if y then 1 else 2
\end{code}
The type of the following term also contains two arrows but it is parenthesised differently. It is a higher order function, where the domain type is a function itself.
\begin{code}
  higher : Tm ∙ ((Bool ⇒ Nat) ⇒ Nat)
  higher = lam (v0 $ isZero (v0 $ true)) -- λ f . f $ (isZero (f $ true))
\end{code}
\begin{exe}[compulsory]
Define the function \verb$apply3 : Tm ∙ ((Bool ⇒ Bool) ⇒ Bool ⇒ Bool)$ which applies the function given as the first parameter three times to the boolean given as the second parameter.
\end{exe}

Note the difference between the types of \verb$isZero$ and \verb$isZero'$:
\begin{verbatim}
isZero   : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
isZero'  : ∀{Γ} → Tm Γ (Nat ⇒ Bool)
\end{verbatim}
One is a metatheoretic function relating terms, the other is a function in our object theory. We show that the latter is stronger than the former: the types
\begin{code}[hide]
module ⇒vs→ {i j k l}(M : Model {i}{j}{k}{l})(A B : Model.Ty M) where
  open Model M
\end{code}
\begin{code}
  EXT = Σ (∀ Γ → Tm Γ A → Tm Γ B) λ f → ∀{Γ Δ γ a} → Lift (f Γ a [ γ ] ≡ f Δ (a [ γ ]))
  INT = Tm ∙ (A ⇒ B)
\end{code}
are isomorphic (for any \verb$A$, \verb$B$ in any model).
\begin{code}
  toINT : EXT → INT
  toINT f = lam (π₁ f (∙ ▹ A) q)
  toEXT : INT → EXT
  toEXT t = (λ Γ a → t [ ε ] $ a) , λ {Γ}{Δ}{γ}{a} → mk (
    (t [ ε ] $ a) [ γ ]
                             ≡⟨ $[] ⟩
    t [ ε ] [ γ ] $ a [ γ ]
                             ≡⟨ cong (_$ a [ γ ]) (sym {A = Tm _ _} [∘]) ⟩
    t [ ε ⊚ γ ] $ a [ γ ]
                             ≡⟨ cong (λ x → t [ x ] $ a [ γ ]) ∙η ⟩
    t [ ε ] $ a [ γ ]
                             ∎)
  extRoundtrip : (f : EXT) → toEXT (toINT f) ≡ f
  extRoundtrip f = (λ Γ a →
      lam (π₁ f (∙ ▹ A) q) [ ε ] $ a
                                                    ≡⟨ cong (_$ a) lam[] ⟩
      lam (π₁ f (∙ ▹ A) q [ ε ⊚ p ,o q ]) $ a
                                                    ≡⟨ cong (λ x → lam x $ a) (un (π₂ f)) ⟩
      lam (π₁ f (Γ ▹ A) (q [ ε ⊚ p ,o q ])) $ a
                                                    ≡⟨ cong (λ x → lam (π₁ f (Γ ▹ A) x) $ a) ▹β₂ ⟩
      lam (π₁ f (Γ ▹ A) q) $ a
                                                    ≡⟨ ⇒β ⟩
      π₁ f (Γ ▹ A) q [ id ,o a ]
                                                    ≡⟨ un (π₂ f) ⟩
      π₁ f Γ (q [ id ,o a ])
                                                    ≡⟨ cong (π₁ f Γ) ▹β₂ ⟩
      π₁ f Γ a
                                                    ∎)
    , λ _ _ _ _ → mk trivi
  intRoundtrip : (t : INT) → toINT (toEXT t) ≡ t
  intRoundtrip t = trans {A = Tm _ _}
    (cong (λ γ → lam (t [ γ ] $ q)) (sym {A = Sub _ ∙} ∙η))
    ⇒η
\end{code}

\begin{exe}[recommended]
Show that for any model, \verb$Tm Γ (A ⇒ B)$ is isomorphic to \\
\verb$Σ (∀{Δ}(γ : Sub Δ Γ) → Tm Δ A → Tm Δ B) λ f → ∀{γ a δ} → f γ a [ δ ] ≡ f (γ ∘ δ) (a [ δ ])$
\end{exe}

\begin{code}[hide]
St : Model
St = record
  { Con       = Set
  ; Sub       = λ Δ Γ → Δ → Γ
  ; _⊚_       = λ γ δ θ* → γ (δ θ*)
  ; ass       = λ {Γ}{Δ}{Θ}{Ξ} → refl {A = Ξ → Γ}
  ; id        = λ γ* → γ*
  ; idl       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; idr       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  
  ; ∙         = Lift ⊤
  ; ε         = _
  ; ∙η        = λ {Γ}{σ} → refl {A = Γ → Lift ⊤}
  
  ; Ty        = Set
  
  ; Tm        = λ Γ A → Γ → A
  ; _[_]      = λ a γ δ* → a (γ δ*) 
  ; [∘]       = λ {Γ}{Δ}{Θ}{A} → refl {A = Θ → A}
  ; [id]      = λ {Γ}{A}{a} → refl {A = Γ → A}
  ; _▹_       = _×_
  ; _,o_      = λ γ t δ* → γ δ* , t δ*
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; ▹β₂       = λ {Γ}{Δ}{A} → refl {A = Δ → A}
  ; ▹η        = λ {Γ}{Δ}{A} → refl {A = Δ → Γ × A}

  ; Bool      = 𝟚
  ; true      = λ _ → tt
  ; false     = λ _ → ff
  ; ite       = λ t u v γ* → if t γ* then u γ* else v γ*
  ; iteβ₁     = λ {Γ}{A} → refl {A = Γ → A}
  ; iteβ₂     = λ {Γ}{A} → refl {A = Γ → A}
  ; true[]    = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → tt}
  ; false[]   = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → ff}
  ; ite[]     = λ {Γ}{A}{t}{u}{v}{Δ}{γ} → refl {A = Δ → A}

  ; Nat       = ℕ
  ; num       = λ n γ* → n
  ; isZero    = λ t γ* → rec tt (λ _ → ff) (t γ*)
  ; _+o_      = λ m n γ* → m γ* + n γ*
  ; isZeroβ₁  = λ {Γ} → refl {A = Γ → 𝟚}{x = λ _ → tt}
  ; isZeroβ₂  = λ {Γ} → refl {A = Γ → 𝟚}{x = λ _ → ff}
  ; +β        = λ {Γ}{m}{n} → refl {A = Γ → ℕ}{x = λ _ → m + n}
  ; num[]     = λ {_}{n}{Δ}{_} → refl {A = Δ → ℕ}{x = λ _ → n}
  ; isZero[]  = λ {_}{t}{Δ}{γ} → refl {A = Δ → 𝟚}
  ; +[]       = λ {_}{u}{v}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ δ* → u (γ δ*) + v (γ δ*)}
\end{code}
The new components in the standard model:
\begin{code}
  ; _⇒_       = λ A B → A → B
  ; lam       = λ t γ* α* → t (γ* , α*)
  ; _$_       = λ t u γ* → t γ* (u γ*)
  ; ⇒β        = λ {Γ}{A}{B}{t}{u} → refl {A = Γ → B}
  ; ⇒η        = λ {Γ}{A}{B}{t} → refl {x = t}
  ; lam[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A → B}
  ; $[]       = λ {Γ}{A}{B}{t}{u}{Δ}{γ} → refl {A = Δ → B}
\end{code}
\begin{code}[hide]
  }
module St = Model St
\end{code}

TODO: bidirectional type checking (for this, we need an ABT version of STT).

TODO: normalisation.
