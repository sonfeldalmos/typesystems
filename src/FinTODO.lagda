\section{Canonicity}

We prove canonicity using the logical predicate method as for STT.

The predicate that we will use for sums and case:
\begin{code}
data _P+_ {A' B'}(A : Tm ∙ A' → Set)(B : Tm ∙ B' → Set) : Tm ∙ (A' + B') → Set where
  Pinl : ∀ {a'} → A a' → (A P+ B) (inl a')
  Pinr : ∀ {b'} → B b' → (A P+ B) (inr b')

Pcase : ∀{A' B' C'}{t' : Tm ∙ (A' + B')}{u' : Tm (∙ ▹ A') C'}{v' : Tm (∙ ▹ B') C'}
  {A : Tm ∙ A' → Set}{B : Tm ∙ B' → Set}(C : Tm ∙ C' → Set)
  (t : (A P+ B) t')(u : ∀ {a'} → A a' → C (u' [ id , a' ]))
  (v : ∀ {b'} → B b' → C (v' [ id , b' ])) →
  C (case t' u' v')
Pcase {u' = u'} {v'} C (Pinl t) u v  = u t
Pcase {u' = u'} {v'} C (Pinr t) u v = v t
\end{code}
\begin{code}[hide]
Can : DepAlgebra
Can = record
         { Con = λ Γ' → Sub ∙ Γ' → Set
         ; Ty = λ A' → Tm ∙ A' → Set
         ; Sub = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
         ; Tm = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
         ; ∙ = λ _ → ↑p 𝟙
         ; _▹_ = λ Γ A σ' → Γ (p ∘ σ') ×m A (q [ σ' ])
         ; _∘_ = λ σ δ x → σ (δ x)
         ; id = idf
         ; ε = λ _ → *↑
         ; _,_ = λ σ t x → σ x ,Σ t x
         ; p = π₁
         ; q = π₂
         ; _[_] = λ t σ x → t (σ x)
         ; ass = refl
         ; idl = refl
         ; idr = refl
         ; ∙η = refl
         ; ▹β₁ = refl
         ; ▹β₂ = refl
         ; ▹η = refl
         ; [id] = refl
         ; [∘] = refl
         
         ; _⇒_ = λ A B t' → ∀ {u'} → A u' → B (t' $ u')
         ; lam = λ t x y → t (x ,Σ y)
         ; app = λ t xy → t (π₁ xy) (π₂ xy)
         ; ⇒β = refl
         ; ⇒η = refl
         ; lam[] = refl
\end{code}
Finite product and sum types in the logical predicate dependent algebra:
\begin{code}
         ; Unit      = λ _ → ↑p 𝟙
         ; tt        = λ _ → *↑
         ; Unitη     = refl
         
         ; _×_       = λ A B t' → A (proj₁ t') ×m B (proj₂ t')
         ; ⟨_,_⟩     = λ u v x → u x ,Σ v x
         ; proj₁     = λ t x → π₁ (t x)
         ; proj₂     = λ t x → π₂ (t x)
         ; ×β₁       = refl
         ; ×β₂       = refl
         ; ×η        = refl
         ; ⟨,⟩[]     = refl
         
         ; Empty     = λ _ → ↑p 𝟘
         ; absurd    = λ t x → ⟦ ↓[ t x ]↓ ⟧𝟘
         ; absurd[]  = refl
         
         ; _+_       = _P+_
         ; inl       = λ t x → Pinl (t x)
         ; inr       = λ t x → Pinr (t x)
         ; case      = λ { {C = C} t u v {ν'} x → Pcase C (t x) (λ a → u (x ,Σ a)) (λ b → v (x ,Σ b)) }
         ; +β₁       = refl
         ; +β₂       = refl
         ; inl[]     = refl
         ; inr[]     = refl
         ; case[]    = refl
\end{code}
\begin{code}[hide]
         }
module Can = DepAlgebra Can
\end{code}
The canonicity statements for unit and product follow from their \verb$η$ rules:
\begin{code}
canUnit : (t : Tm ∙ Unit) → ↑p (t ≡ tt)
canUnit t = ↑[ Unitη ]↑

can× : ∀{A B}(t : Tm ∙ (A × B)) → Σ (Tm ∙ A) λ u → Σ (Tm ∙ B) λ v → ↑p (t ≡ ⟨ u , v ⟩)
can× t = proj₁ t ,Σ (proj₂ t ,Σ ↑[ refl ]↑)
\end{code}
For the empty type and sums we use interpretation into the canonicity dependent algebra:
\begin{code}
canEmpty : (t : Tm ∙ Empty) → 𝟘
canEmpty t = ↓[ Can.⟦ t ⟧t {id} ↑[ * ]↑ ]↓

can+ : ∀{A B}(t : Tm ∙ (A + B)) →
  Σ (Tm ∙ A) (λ a → ↑p (t ≡ inl a)) ⊎ Σ (Tm ∙ B) (λ b → ↑p (t ≡ inr b))
can+ t with Can.⟦ t ⟧t {id} ↑[ * ]↑
... | Pinl {u'} u = ι₁ (u' ,Σ ↑[ refl ]↑)
... | Pinr {v'} v = ι₂ (v' ,Σ ↑[ refl ]↑)
-}
\end{code}
