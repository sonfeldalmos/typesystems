{-# OPTIONS --type-in-type --rewriting --prop #-}

open import Agda.Primitive public

postulate
  _~>_ : {A : Set} → A → A → Set
{-# BUILTIN REWRITE _~>_ #-}

record Σ {i}{j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σ public

data Σp {i}{j}(A : Prop i)(B : A → Prop j) : Prop (i ⊔ j) where
  _,_ : (a : A) → B a → Σp A B
π₁p : ∀{A B} → Σp A B → A
π₁p (a , b) = a
π₂p : ∀{A B} → (w : Σp A B) → B (π₁p w)
π₂p (a , b) = b
{-
record Σp {i}{j}(A : Prop i)(B : A → Prop j) : Prop (i ⊔ j) where
  constructor _,_
  field
    π₁ : A
    π₂ : B π₁
open Σp public
-}

record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift public

module A where
  postulate
    _⁼  : (A : Set) → A → Prop
    _~  : {A : Set}(B : A → Set){a : A} → (A ⁼) a → B a → Prop
    _~t : {A : Set}{B : A → Set}(b : (a : A) → B a){a : A}(a* : (A ⁼) a) → (B ~) a* (b a)
    
    Σ=  : (A : Set)(B : A → Set)(w : Σ A B) → (Σ A B ⁼) w ~> Σp ((A ⁼) (π₁ w)) λ a* → (B ~) a* (π₂ w)
    {-# REWRITE Σ= #-}
    ~~  : {A : Set}{B : A → Set}{C : Σ A B → Set}{a : A}{a* : (A ⁼) a}{b : (a : A) → B a} → _~ {Σ A B} C {a , b a} (a* , (b ~t) a*) ~> ((λ a → C (a , b a)) ~) a*
    -- {-# REWRITE ~~ #-}

module B where

  {-
  data SET : Set
  EL : SET → Set
  data SET where
    Σ' : (A : SET) → (EL A → SET) → SET
    Π' : (A : SET) → (EL A → SET) → SET
  EL (Σ' A B) = Σ (EL A) λ a → EL (B a)
  EL (Π' A B) = (a : EL A) → EL (B a)
  -}
  SET : Set
  SET = Set
  EL : SET → Set
  EL A = A
  Σ' : (A : SET) → (EL A → SET) → SET
  Σ' A B = Σ A B
  Π' : (A : SET) → (EL A → SET) → SET
  Π' A B = (x : A) → B x

  postulate
    --_≡_  : {Ω : Set} → Ω → Ω → Set
    _≡_  : {Ω : Set} → Ω → Ω → Prop
    --_~ : {Ω : Set}(A : Ω → SET){ω₀ ω₁ : Ω} → ω₀ ≡ ω₁ → EL (A ω₀) → EL (A ω₁) → Set
    _~ : {Ω : Set}(A : Ω → SET){ω₀ ω₁ : Ω} → ω₀ ≡ ω₁ → EL (A ω₀) → EL (A ω₁) → Prop
    --Σ≡ : ∀{Ω A}{ω₀ ω₁ : Ω}{a₀ : EL (A ω₀)}{a₁ : EL (A ω₁)} → (_≡_ {Σ Ω (λ ω → EL (A ω))} (ω₀ , a₀)(ω₁ , a₁)) ~> Σ (ω₀ ≡ ω₁) λ ω₂ → (A ~) ω₂ a₀ a₁
    Σ≡ : ∀{Ω A}{ω₀ ω₁ : Ω}{a₀ : EL (A ω₀)}{a₁ : EL (A ω₁)} → (_≡_ {Σ Ω (λ ω → EL (A ω))} (ω₀ , a₀)(ω₁ , a₁)) ~> Σp (ω₀ ≡ ω₁) λ ω₂ → (A ~) ω₂ a₀ a₁
    {-# REWRITE Σ≡ #-}

  postulate
    cong : {Ω : Set}{A : Ω → SET}(a : (ω : Ω) → EL (A ω)){ω₀ ω₁ : Ω}(ω₂ : ω₀ ≡ ω₁) → (A ~) ω₂ (a ω₀) (a ω₁)

    ~cong : {Ω : Set}{A : Ω → SET}{B : Σ Ω (λ ω → EL (A ω)) → SET}{ω₀ ω₁ : Ω}{ω₂ : ω₀ ≡ ω₁}{a : (ω : Ω) → EL (A ω)} →
      _~ {Σ Ω (λ ω → EL (A ω))} B (ω₂ , cong a ω₂) ~> _~ {Ω} (λ ω → B (ω , a ω)) ω₂
    -- {-# REWRITE ~cong #-}

  postulate
    Σ~ : {Ω : Set}{A : Ω → SET}{B : (ω : Ω) → EL (A ω) → SET}{ω₀ ω₁ : Ω}{ω₂ : ω₀ ≡ ω₁}{a₀ : EL (A ω₀)}{a₁ : EL (A ω₁)}{b₀ : EL (B ω₀ a₀)}{b₁ : EL (B ω₁ a₁)} →
      ((λ ω → Σ' (A ω) (B ω)) ~) ω₂ (a₀ , b₀) (a₁ , b₁) ~> Σp ((A ~) ω₂ a₀ a₁) λ a₂ → _~ {Σ Ω (λ ω → EL (A ω))} (λ ωa → B (π₁ ωa) (π₂ ωa)) (ω₂ , a₂) b₀ b₁
    {-# REWRITE Σ~ #-}
    Π~ : {Ω : Set}{A : Ω → SET}{B : (ω : Ω) → EL (A ω) → SET}{ω₀ ω₁ : Ω}{ω₂ : ω₀ ≡ ω₁}{f₀ : EL (Π' (A ω₀) (B ω₀))}{f₁ : EL (Π' (A ω₁) (B ω₁))} →
      ((λ ω → Π' (A ω) (B ω)) ~) ω₂ f₀ f₁ ~> ((a₀ : EL (A ω₀))(a₁ : EL (A ω₁))(a₂ : (A ~) ω₂ a₀ a₁) → _~ {Σ Ω (λ ω → EL (A ω))} (λ ωa → B (π₁ ωa) (π₂ ωa)) (ω₂ , a₂) (f₀ a₀) (f₁ a₁))
    {-# REWRITE Π~ #-}

