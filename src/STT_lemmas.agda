{-# OPTIONS --prop --rewriting #-}
module STT_lemmas where

open import Lib
open import STT
open I

cong-ite₁ : {Γ : Con} → {A : Ty} → {co co' : Tm Γ Bool} → {tr fa : Tm Γ A} →
            (eq : co ≡ co') → ite co tr fa ≡ ite co' tr fa
cong-ite₁ {tr = tr} {fa = fa} eq = cong (λ a → ite a tr fa) eq

cong-ite₂ : {Γ : Con} → {A : Ty} → {co : Tm Γ Bool} → {tr tr' fa : Tm Γ A} →
            (eq : tr ≡ tr') → ite co tr fa ≡ ite co tr' fa
cong-ite₂ {co = co} {fa = fa} eq = cong (λ a → ite co a fa) eq

cong-ite₃ : {Γ : Con} → {A : Ty} → {co : Tm Γ Bool} → {tr fa fa' : Tm Γ A} →
            (eq : fa ≡ fa') → ite co tr fa ≡ ite co tr fa'
cong-ite₃ {co = co} {tr = tr} eq = cong (λ a → ite co tr a) eq

cong-+₁ : {Γ : Con} → {l l' r : Tm Γ Nat} →
          (eq : l ≡ l') → l +o r ≡ l' +o r
cong-+₁ {r = r} eq = cong (_+o r) eq

cong-+₂ : {Γ : Con} → {l r r' : Tm Γ Nat} →
          (eq : r ≡ r') → l +o r ≡ l +o r'
cong-+₂ {l = l} eq = cong (l +o_) eq

cong-+ : {Γ : Con} → {l l' r r' : Tm Γ Nat} →
          (eql : l ≡ l') → (eqr : r ≡ r') → l +o r ≡ l' +o r'
cong-+ eql eqr = trans {A = Tm _ _} (cong-+₁ eql) (cong-+₂ eqr)

cong-isZero : {Γ : Con} → {t t' : Tm Γ Nat} →
              (eq : t ≡ t') → isZero t ≡ isZero t'
cong-isZero eq = cong isZero eq

cong-[]₁ : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A : Ty} → {t t' : Tm Δ A} →
           (eq : t ≡ t') → t [ σ ] ≡ t' [ σ ]
cong-[]₁ {σ = σ} eq = cong {A = Tm _ _} (_[ σ ]) eq

cong-[]₂ : {Γ Δ : Con} → {σ σ' : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
           (eq : σ ≡ σ') → t [ σ ] ≡ t [ σ' ]
cong-[]₂ {t = t} eq = cong {A = Sub _ _} (t [_]) eq

▹v1 : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {a : Tm Γ A} → {b : Tm Γ B} →
      v1 [ σ ,o a ,o b ] ≡ a
▹v1 {σ = σ} {a = a} {b = b} =
  ((q [ p ]) [ (σ ,o a) ,o b ])
    ≡⟨ sym {A = Tm _ _} [∘] ⟩
  (q [ p ⊚ ((σ ,o a) ,o b) ])
    ≡⟨ cong {A = Sub _ _} (q [_]) ▹β₁ ⟩
  (q [ σ ,o a ])
    ≡⟨ ▹β₂ ⟩
  a
    ∎

▹v2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B C : Ty} →
      {a : Tm Γ A} → {b : Tm Γ B} → {c : Tm Γ C} →
      v2 [ σ ,o a ,o b ,o c ] ≡ a
▹v2 {σ = σ} {a = a} {b = b} {c = c} =
  q [ p ⊚ p ] [ σ ,o a ,o b ,o c ]
    ≡⟨ sym {A = Tm _ _} [∘] ⟩
  q [ p ⊚ p ⊚ (σ ,o a ,o b ,o c) ]
    ≡⟨ cong {A = Sub _ _} (q [_]) ass ⟩
  q [ p ⊚ (p ⊚ (σ ,o a ,o b ,o c)) ]
    ≡⟨ cong {A = Sub _ _} (λ a → q [ p ⊚ a ]) ▹β₁ ⟩
  q [ p ⊚ (σ ,o a ,o b) ]
    ≡⟨ cong {A = Sub _ _} (q [_]) ▹β₁ ⟩
  q [ σ ,o a ]
    ≡⟨ ▹β₂ ⟩
  a
    ∎

sym-[∘] : {Γ Δ Θ : Con}{A : Ty}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →
         t [ γ ] [ δ ] ≡ t [ γ ⊚ δ ]
sym-[∘] = sym {A = Tm _ _} [∘]

sym-[id] : {Γ : Con}{A : Ty}{t : Tm Γ A} → t ≡ t [ id ]
sym-[id] = sym {A = Tm _ _} [id]

sym-ass : {Γ Δ Θ Ξ : Con}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} →
          γ ⊚ (δ ⊚ θ) ≡ (γ ⊚ δ) ⊚ θ
sym-ass = sym {A = Sub _ _} ass

sym-idl : {Γ Δ : Con}{σ : Sub Γ Δ} → σ ≡ id ⊚ σ
sym-idl = sym {A = Sub _ _} idl

sym-idr : {Γ Δ : Con}{σ : Sub Γ Δ} → σ ≡ σ ⊚ id
sym-idr = sym {A = Sub _ _} idr

cong-⊚₁ : {Γ Δ Θ : Con}{σ σ' : Sub Δ Θ}{δ : Sub Γ Δ} →
          σ ≡ σ' → σ ⊚ δ ≡ σ' ⊚ δ
cong-⊚₁ {δ = δ} eq = cong {A = Sub _ _} (_⊚ δ) eq

cong-⊚₂ : {Γ Δ Θ : Con}{σ : Sub Δ Θ}{δ δ' : Sub Γ Δ} →
          δ ≡ δ' → σ ⊚ δ ≡ σ ⊚ δ'
cong-⊚₂ {σ = σ} eq = cong {A = Sub _ _} (σ ⊚_) eq

cong-,₁ : {Γ Δ : Con}{A : Ty}{σ σ' : Sub Γ Δ}{t : Tm Γ A} →
          σ ≡ σ' → σ ,o t ≡ σ' ,o t
cong-,₁ {t = t} eq = cong {A = Sub _ _} (_,o t) eq

cong-,₂ : {Γ Δ : Con}{A : Ty}{σ : Sub Γ Δ}{t t' : Tm Γ A} →
          t ≡ t' → σ ,o t ≡ σ ,o t'
cong-,₂ {σ = σ} eq = cong {A = Tm _ _} (σ ,o_) eq

cong-lam : {Γ : Con}{A B : Ty}{t t' : Tm (Γ ▹ A) B} →
           t ≡ t' → lam t ≡ lam t'
cong-lam eq = cong {A = Tm _ _} lam eq

cong-$₁ : {Γ : Con}{A B : Ty}{f f' : Tm Γ (A ⇒ B)}{t : Tm Γ A} →
          f ≡ f' → f $ t ≡ f' $ t
cong-$₁ {t = t} eq = cong {A = Tm _ _} (_$ t) eq

cong-$₂ : {Γ : Con}{A B : Ty}{f : Tm Γ (A ⇒ B)}{t t' : Tm Γ A} →
          t ≡ t' → f $ t ≡ f $ t'
cong-$₂ {f = f} eq = cong {A = Tm _ _} (f $_) eq