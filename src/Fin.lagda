\chapter{Product and sum types}
\label{ch:Fin}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Nullary and binary products. Nullary and binary sums, enumerations, booleans encoded as sums, option type. More equalities in the standard model than in the syntax.
\end{tcolorbox}

TODO: named records, curry, uncurry, isomorphisms of finite types, normalisation (hard for sums).

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Fin where

open import Lib

module I where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 ⟨_,_⟩
  infixl 6 _+o_

  data Ty     : Set where
    _⇒_       : Ty → Ty → Ty
    _×o_      : Ty → Ty → Ty
    Unit      : Ty
    _+o_      : Ty → Ty → Ty
    Empty     : Ty

  data Con    : Set where
    ∙         : Con
    _▹_       : Con → Ty → Con

  postulate
    Sub       : Con → Con → Set
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm        : Con → Ty → Set
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    ⟨_,_⟩     : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
    ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
    ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
    ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩

    trivial   : ∀{Γ} → Tm Γ Unit
    Unitη     : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial

    inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (A +o B)
    inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (A +o B)
    caseo     : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm (Γ ▹ A +o B) C
    +β₁       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ A} →
                caseo u v [ id ,o inl t ] ≡ u [ id ,o t ]
    +β₂       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ B} →
                caseo u v [ id ,o inr t ] ≡ v [ id ,o t ]
    +η        : ∀{Γ A B C}{t : Tm (Γ ▹ A +o B) C} →
                caseo (t [ p ,o inl q ]) (t [ p ,o inr q ]) ≡ t
    inl[]     : ∀{Γ A B}{t : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (inl {B = B} t) [ γ ] ≡ inl (t [ γ ])
    inr[]     : ∀{Γ A B}{t : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                (inr {A = A} t) [ γ ] ≡ inr (t [ γ ])
    case[]    : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ} →
                (caseo u v) [ γ ⊚ p ,o q ] ≡
                caseo (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ])

    absurd    : ∀{Γ A} → Tm (Γ ▹ Empty) A
    Emptyη    : ∀{Γ A}{t : Tm (Γ ▹ Empty) A} → t ≡ absurd

  caseo' : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ (A +o B) → Tm Γ C
  caseo' l r s = caseo l r [ id ,o s ]

  absurd' : ∀{Γ A} → Tm Γ Empty → Tm Γ A
  absurd' e = absurd [ id ,o e ]

  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]

variable
  Aᴵ Bᴵ : I.Ty
  Γᴵ Δᴵ Θᴵ : I.Con
  γᴵ δᴵ θᴵ σᴵ γaᴵ : I.Sub Δᴵ Γᴵ
  tᴵ uᴵ vᴵ : I.Tm Γᴵ Aᴵ

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 ⟨_,_⟩
  infixl 6 _+o_

  field
    Con       : Set i
    Sub       : Con → Con → Set j
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ∙         : Con
    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Ty        : Set k

    Tm        : Con → Ty → Set l
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_       : Con → Ty → Con
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    _⇒_       : Ty → Ty → Ty
    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]
\end{code}
A Fin model consists of a substitution calculus (sCwF, see Section \ref{sec:def}), components
for function space and components for the following four new type formers.
\begin{itemize}
\item Binary products:
\begin{code}
    _×o_      : Ty → Ty → Ty
    ⟨_,_⟩     : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd       : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
    ×β₂       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
    ×η        : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
    ,[]       : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩
\end{code}
A term of the binary product type \verb$A ×o B$ (other notations: \verb$A × B$, \verb$record { a : A , b : B}$) is an ordered pair of a
term of type \verb$A$ and another of type \verb$B$. The constructor is
pairing, and there are two destructors: first and second
projections. The computation rules say what happens if we project out
the first or second element of a pair, the uniqueness rule says that
any pair is composed from its two projections. We only need a
substitution rule for the constructor, the substitution rules for the
destructors can be proven, see below.
\item Nullary products:
\begin{code}
    Unit      : Ty
    trivial   : ∀{Γ} → Tm Γ Unit
    Unitη     : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial
\end{code}
The nullary product is the unit (top, \verb$()$) type with only one
constructor and no destructor: this shows that there is no information
in a term of type \verb$Unit$, there is no way to use such a
term. This is why in some programing languages this type is called
\verb$void$ -- when a function does not return a value, its return type
is unit.
\item Binary sums:
\begin{code}
    _+o_      : Ty → Ty → Ty
    inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (A +o B)
    inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (A +o B)
    caseo     : ∀{Γ A B C} → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm (Γ ▹ A +o B) C
    +β₁       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ A} →
                caseo u v [ id ,o inl t ] ≡ u [ id ,o t ]
    +β₂       : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{t : Tm Γ B} →
                caseo u v [ id ,o inr t ] ≡ v [ id ,o t ]
    +η        : ∀{Γ A B C}{t : Tm (Γ ▹ A +o B) C} →
                caseo (t [ p ,o inl q ]) (t [ p ,o inr q ]) ≡ t
    inl[]     : ∀{Γ A B}{t : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                (inl {B = B} t) [ γ ] ≡ inl (t [ γ ])
    inr[]     : ∀{Γ A B}{t : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                (inr {A = A} t) [ γ ] ≡ inr (t [ γ ])
    case[]    : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ} →
                (caseo u v) [ γ ⊚ p ,o q ] ≡
                caseo (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ])
\end{code}
They are specified by the following isomorphism.
\begin{verbatim}
  (inl q, inr q) : Tm (Γ ▹ A +o B) C ≅ Tm (Γ ▹ A) C × Tm (Γ ▹ B) C : case
  TODO: prove this: f ∘ case u v = case (f∘u) (f∘v)
\end{verbatim}
\item Nullary sums:
\begin{code}
    Empty     : Ty
    absurd    : ∀{Γ A} → Tm (Γ ▹ Empty) A
    Emptyη    : ∀{Γ A}{t : Tm (Γ ▹ Empty) A} → t ≡ absurd
\end{code}
\end{itemize}

\begin{code}[hide]
  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]
  v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
  v0 = q
  v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]
  ▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
  ▹η' {Γ}{A} =
    p ,o q
      ≡⟨ sym {A = Sub (Γ ▹ A) (Γ ▹ A)}
           (cong {A = Sub (Γ ▹ A) Γ × Tm (Γ ▹ A) A} (λ w → π₁ w ,o π₂ w) (idr , [id])) ⟩
    p ⊚ id ,o q [ id ]
      ≡⟨ ▹η ⟩
    id
      ∎

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong (_⊚ δ) ▹β₁ , cong (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎

  ^∘ : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{Θ}{δ : Sub Θ Δ}{t : Tm Θ A} →
    (γ ⊚ p ,o q) ⊚ (δ ,o t) ≡ (γ ⊚ δ ,o t)
  ^∘ {Γ}{Δ}{γ}{A}{Θ}{δ}{t} =
    (γ ⊚ p ,o q) ⊚ (δ ,o t)
      ≡⟨ ,∘ ⟩
    (γ ⊚ p ⊚ (δ ,o t) ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → (x ,o q [ δ ,o t ])) ass ⟩
    (γ ⊚ (p ⊚ (δ ,o t)) ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → (γ ⊚ x ,o q [ δ ,o t ])) ▹β₁ ⟩
    (γ ⊚ δ ,o q [ δ ,o t ])
      ≡⟨ cong (λ x → γ ⊚ δ ,o x) ▹β₂ ⟩
    (γ ⊚ δ ,o t)
      ∎
\end{code}
The following are provable in any algebra:
\begin{code}
  fst[] : ∀{Γ Δ A B}{t : Tm Γ (A ×o B)}{γ : Sub Δ Γ} →
    (fst t) [ γ ] ≡ fst (t [ γ ])
  fst[] {Γ}{Δ}{A}{B}{t}{γ} =
    fst t [ γ ]
                          ≡⟨ sym {A = Tm _ _} ×β₁ ⟩
    fst ⟨ fst t [ γ ] , snd t [ γ ] ⟩
                          ≡⟨ cong fst (sym {A = Tm _ _} ,[]) ⟩
    fst (⟨ fst t , snd t ⟩ [ γ ])
                          ≡⟨ cong (λ x → fst (x [ γ ])) ×η ⟩
    fst (t [ γ ])
                          ∎

  snd[] : ∀{Γ Δ A B}{t : Tm Γ (A ×o B)}{γ : Sub Δ Γ} →
    (snd t) [ γ ] ≡ snd (t [ γ ])
  snd[] {Γ}{Δ}{A}{B}{t}{γ} =
    snd t [ γ ]
                          ≡⟨ sym {A = Tm _ _} ×β₂ ⟩
    snd ⟨ fst t [ γ ] , snd t [ γ ] ⟩
                          ≡⟨ cong snd (sym {A = Tm _ _} ,[]) ⟩
    snd (⟨ fst t , snd t ⟩ [ γ ])
                          ≡⟨ cong (λ x → snd (x [ γ ])) ×η ⟩
    snd (t [ γ ])
                          ∎

  trivial[] : ∀{Γ Δ}{γ : Sub Δ Γ} → trivial [ γ ] ≡ trivial
  trivial[] = Unitη

  +β₁' : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ}{t : Tm Δ A} →
    caseo u v [ γ ,o inl t ] ≡ u [ γ ,o t ]
  +β₁' {Γ}{A}{B}{C}{u}{v}{Δ}{γ}{t} =
    caseo u v [ γ ,o inl t ]
      ≡⟨ cong (λ x → caseo u v [ x ,o inl t ]) (sym {A = Sub _ _} idr) ⟩
    caseo u v [ (γ ⊚ id ,o inl t) ]
      ≡⟨ cong (caseo u v [_]) (sym {A = Sub _ _} ^∘)  ⟩
    caseo u v [ (γ ⊚ p ,o q) ⊚ (id ,o inl t) ]
      ≡⟨ [∘] ⟩
    caseo u v [ γ ⊚ p ,o q ] [ id ,o inl t ]
      ≡⟨ cong (_[ id ,o inl t ]) case[] ⟩
    caseo (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) [ id ,o inl t ]
      ≡⟨ +β₁ ⟩
    u [ γ ⊚ p ,o q ] [ id ,o t ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    u [ (γ ⊚ p ,o q) ⊚ (id ,o t) ]
      ≡⟨ cong (u [_]) ^∘ ⟩
    u [ γ ⊚ id ,o t ]
      ≡⟨ cong (λ x → u [ x ,o t ]) idr ⟩
    u [ γ ,o t ]
      ∎

  +β₂' : ∀{Γ A B C}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{Δ}{γ : Sub Δ Γ}{t : Tm Δ B} →
    caseo u v [ γ ,o inr t ] ≡ v [ γ ,o t ]
  +β₂' {Γ}{A}{B}{C}{u}{v}{Δ}{γ}{t} =
    caseo u v [ γ ,o inr t ]
      ≡⟨ cong (λ x → caseo u v [ x ,o inr t ]) (sym {A = Sub _ _} idr) ⟩
    caseo u v [ (γ ⊚ id ,o inr t) ]
      ≡⟨ cong (caseo u v [_]) (sym {A = Sub _ _} ^∘)  ⟩
    caseo u v [ (γ ⊚ p ,o q) ⊚ (id ,o inr t) ]
      ≡⟨ [∘] ⟩
    caseo u v [ γ ⊚ p ,o q ] [ id ,o inr t ]
      ≡⟨ cong (_[ id ,o inr t ]) case[] ⟩
    caseo (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) [ id ,o inr t ]
      ≡⟨ +β₂ ⟩
    v [ γ ⊚ p ,o q ] [ id ,o t ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    v [ (γ ⊚ p ,o q) ⊚ (id ,o t) ]
      ≡⟨ cong (v [_]) ^∘ ⟩
    v [ γ ⊚ id ,o t ]
      ≡⟨ cong (λ x → v [ x ,o t ]) idr ⟩
    v [ γ ,o t ]
      ∎

  absurd[] : ∀{Γ A}{Δ}{γ : Sub Δ Γ} →
    absurd {Γ}{A} [ γ ⊚ p ,o q ] ≡ absurd
  absurd[] = Emptyη
\end{code}
\begin{code}[hide]
  ⟦_⟧T : I.Ty → Ty
  ⟦ A I.⇒ B     ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.×o B    ⟧T = ⟦ A ⟧T ×o ⟦ B ⟧T
  ⟦ I.Unit      ⟧T = Unit
  ⟦ A I.+o B    ⟧T = ⟦ A ⟧T +o ⟦ B ⟧T
  ⟦ I.Empty     ⟧T = Empty

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙         ⟧C = ∙
  ⟦ Γ I.▹ A     ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S      : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
    ⟦_⟧t      : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
    ⟦∘⟧       : ⟦ γᴵ I.⊚ δᴵ ⟧S       ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧      : ⟦ I.id {Γᴵ} ⟧S       ≡ id
    ⟦ε⟧       : ⟦ I.ε {Γᴵ} ⟧S        ≡ ε
    ⟦[]⟧      : ⟦ tᴵ I.[ γᴵ ] ⟧t     ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧       : ⟦ γᴵ I.,o tᴵ ⟧S      ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧       : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S    ≡ p
    ⟦q⟧       : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t    ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦lam⟧     : ⟦ I.lam tᴵ ⟧t        ≡ lam ⟦ tᴵ ⟧t
    ⟦$⟧       : ⟦ tᴵ I.$ uᴵ ⟧t       ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}

    ⟦⟨,⟩⟧       : ⟦ I.⟨ uᴵ , vᴵ ⟩ ⟧t ≡ ⟨ ⟦ uᴵ ⟧t , ⟦ vᴵ ⟧t ⟩
    ⟦fst⟧     : ⟦ I.fst tᴵ ⟧t        ≡ fst  ⟦ tᴵ ⟧t
    ⟦snd⟧     : ⟦ I.snd tᴵ ⟧t        ≡ snd  ⟦ tᴵ ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦fst⟧ ⟦snd⟧ #-}

    ⟦trivial⟧ : ⟦ I.trivial {Γᴵ} ⟧t  ≡ trivial
    {-# REWRITE ⟦trivial⟧ #-}

    ⟦inl⟧     : ⟦ I.inl {Γᴵ}{Aᴵ}{Bᴵ} tᴵ  ⟧t ≡ inl ⟦ tᴵ ⟧t
    ⟦inr⟧     : ⟦ I.inr {Γᴵ}{Aᴵ}{Bᴵ} tᴵ  ⟧t ≡ inr ⟦ tᴵ ⟧t
    ⟦case⟧    : ⟦ I.caseo uᴵ vᴵ          ⟧t ≡ caseo ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    {-# REWRITE ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦absurd⟧  : ⟦ I.absurd {Γᴵ}{Aᴵ} ⟧t ≡ absurd
    {-# REWRITE ⟦absurd⟧ #-}
\end{code}

\section{Applications}

\begin{code}[hide]
module Applications {i j k l}(M : Model {i}{j}{k}{l}) where
  open Model M
\end{code}
Enumerations can be defined using the unit and sum types. For example, the type with three elements:
\begin{code}
  Three : Ty
  Three = Unit +o (Unit +o Unit)
\end{code}
Its three elements:
\begin{code}
  three0 three1 three2 : ∀{Γ} → Tm Γ Three
  three0 = inl trivial
  three1 = inr (inl trivial)
  three2 = inr (inr trivial)
\end{code}
Elimination principle of \verb$Three$.
\begin{exe}[recommended]
The second and third rules are exercises.
\end{exe}
\begin{code}
  caseThree : ∀{Γ A} → Tm Γ Three → Tm Γ A → Tm Γ A → Tm Γ A → Tm Γ A
  caseThree t u v w = caseo (u [ p ]) (caseo (v [ p ]) (w [ p ])) [ id ,o t ]
  Threeβ0 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three0 u v w ≡ u
  Threeβ1 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three1 u v w ≡ v
  Threeβ2 : ∀{Γ A}{u v w : Tm Γ A} → caseThree three2 u v w ≡ w
  Threeβ0 {Γ}{a}{u}{v}{w} =
    caseThree three0 u v w
                           ≡⟨ refl {x = caseThree three0 u v w} ⟩
    caseo (u [ p ]) (caseo (v [ p ]) (w [ p ])) [ id ,o inl trivial ]
                           ≡⟨ +β₁ ⟩
    u [ p ] [ id ,o trivial ]
                           ≡⟨ sym {A = Tm _ _} [∘] ⟩
    u [ p ⊚ (id ,o trivial) ]
                           ≡⟨ cong {A = Sub _ _} (u [_]) ▹β₁ ⟩
    u [ id ]
                           ≡⟨ [id] ⟩
    u
                           ∎
\end{code}
\begin{code}[hide]
  Threeβ1 = exercisep
  Threeβ2 = exercisep
\end{code}
\begin{exe}[compulsory]
Define booleans using \verb$Unit$ and binary sums.
\end{exe}
Addition modulo 3:
\begin{code}
  plus3 : Tm ∙ (Three ⇒ Three ⇒ Three)
  plus3 = lam (lam (caseThree v1
    {- v1=0 -} v0
    {- v1=1 -} (caseThree v0 three1 three2 three0)
    {- v1=2 -} (caseThree v0 three2 three0 three1)))

  plus3test00 : plus3 $ three0 $ three0 ≡ three0
  plus3test11 : plus3 $ three1 $ three1 ≡ three2
  plus3test12 : plus3 $ three1 $ three2 ≡ three0
  plus3test00 = exercisep
  plus3test11 = exercisep
  plus3test12 = exercisep
\end{code}
We define booleans. The usual eliminator (if-then-else) is defined using case.
\begin{code}
  Bool : Ty
  Bool = Unit +o Unit

  true : ∀{Γ} → Tm Γ Bool
  true = inl trivial

  false : ∀{Γ} → Tm Γ Bool
  false = inr trivial

  ite : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
  ite b u v = caseo (u [ p ]) (v [ p ]) [ id ,o b ]

  Boolβ₁ : ∀{Γ A}{u v : Tm Γ A} → ite true u v ≡ u
  Boolβ₁ {Γ}{A}{u}{v} =
    ite true u v
      ≡⟨ refl {x = ite true u v} ⟩
    caseo (u [ p ]) (v [ p ]) [ id ,o inl trivial ]
      ≡⟨ +β₁ ⟩
    u [ p ] [ id ,o trivial ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    u [ p ⊚ (id ,o trivial) ]
      ≡⟨ cong {A = Sub _ _} (u [_]) ▹β₁ ⟩
    u [ id ]
      ≡⟨ [id] ⟩
    u
      ∎
  Boolβ₂ : ∀{Γ A}{u v : Tm Γ A} → ite false u v ≡ v
  Boolβ₂ {Γ}{A}{u}{v} =
    ite false u v
      ≡⟨ refl {x = ite false u v} ⟩
    caseo (u [ p ]) (v [ p ]) [ id ,o inr trivial ]
      ≡⟨ +β₂ ⟩
    v [ p ] [ id ,o trivial ]
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    v [ p ⊚ (id ,o trivial) ]
      ≡⟨ cong {A = Sub _ _} (v [_]) ▹β₁ ⟩
    v [ id ]
      ≡⟨ [id] ⟩
    v
      ∎
\end{code}
With \verb$Bool$ and binary products we can simulate homogeneous binary sums:
\begin{code}
  2*_ : Ty → Ty
  2* A = Bool ×o A
  inl' inr' : ∀{Γ A} → Tm Γ A → Tm Γ (2* A)
  inl' t = ⟨ true , t ⟩
  inr' t = ⟨ false , t ⟩
  case' : ∀{Γ A C} → Tm Γ (2* A) → Tm (Γ ▹ A) C → Tm (Γ ▹ A) C → Tm Γ C
  case' t u v = ite (fst t) (u [ id ,o snd t ]) (v [ id ,o snd t ])
  2*β₁ : ∀{Γ A C}{t : Tm Γ A}{u v : Tm (Γ ▹ A) C} → case' (inl' t) u v ≡ u [ id ,o t ]
  2*β₁ {Γ}{A}{C}{t}{u}{v} =
    case' (inl' t) u v
      ≡⟨ refl {x = case' (inl' t) u v} ⟩
    ite (fst ⟨ true , t ⟩) (u [ id ,o snd (inl' t) ]) (v [ id ,o snd (inl' t) ])
      ≡⟨ cong (λ x → ite x (u [ id ,o snd (inl' t) ]) (v [ id ,o snd (inl' t) ])) ×β₁ ⟩
    ite true (u [ id ,o snd (inl' t) ]) (v [ id ,o snd (inl' t) ])
      ≡⟨ Boolβ₁ ⟩
    u [ id ,o snd (inl' t) ]
      ≡⟨ refl {x = u [ id ,o snd (inl' t) ]} ⟩
    u [ id ,o snd ⟨ true , t ⟩ ]
      ≡⟨ cong (λ x → u [ id ,o x ]) ×β₂ ⟩
    u [ id ,o t ]
      ∎
  2*β₂ : ∀{Γ A C}{t : Tm Γ A}{u v : Tm (Γ ▹ A) C} → case' (inr' t) u v ≡ v [ id ,o t ]
  2*β₂ = exercisep
\end{code}
The option type former (sometimes called maybe) adds an extra element to a type:
\begin{code}
  Opt : Ty → Ty
  Opt A = A +o Unit
  none : ∀{Γ A} → Tm Γ (Opt A)
  none = inr trivial
  some : ∀{Γ A} → Tm Γ A → Tm Γ (Opt A)
  some t = inl t
\end{code}

A type with \verb$n$ elements:
\begin{code}
  Fino : ℕ → Ty
  Fino zero    = Unit
  Fino (suc n) = Unit +o Fino n
\end{code}
\begin{exe}
  For each \verb$n$, define the elimination principle of \verb$Fino n$.
\end{exe}

For a natural number \verb$n$, we define \verb$n$-ary products which are parameterised by \verb$n$ types.
\begin{code}
  Tys : ℕ → Set k
  Tys zero     = Raise (Lift ⊤)
  Tys (suc n)  = Ty × Tys n

  Prod : {n : ℕ} → Tys n → Ty
  Prod {zero}  _         = Unit
  Prod {suc n} (A , As)  = A ×o Prod As
\end{code}
The constructor takes \verb$n$ terms and turns it into a term.
\begin{code}
  Tms : Con → {n : ℕ} → Tys n → Set l
  Tms Γ {zero}  _         = Raise (Lift ⊤)
  Tms Γ {suc n} (A , As)  = Tm Γ A × Tms Γ As

  tpl : ∀{Γ n}{As : Tys n} → Tms Γ As → Tm Γ (Prod As)
  tpl {Γ} {zero}   _         = trivial
  tpl {Γ} {suc n}  (t , ts)  = ⟨ t , tpl ts ⟩
\end{code}
We have \verb$n$ different projections.
\begin{code}
  _!_ : ∀{n} → Tys n → Fin n → Ty
  (A , As) ! zero   = A
  (A , As) ! suc i  = As ! i

  prj : ∀{Γ n}{As : Tys n}(i : Fin n) → Tm Γ (Prod As) → Tm Γ (As ! i)
  prj zero     t = fst t
  prj (suc i)  t = prj i (snd t)
\end{code}
\begin{exe}
  State and prove the computation rules and the substitution laws.
\end{exe}
\begin{exe}
  Define \verb$n$-ary coproducts.
\end{exe}

\section{Algebraic properties of types}

Types form a commutative semiring with exponentials, up to isomorphism.
\begin{code}[hide]
module Algebraic {i j k l}(M : Model {i}{j}{k}{l}) where
  open Model M
\end{code}
\begin{code}
  record _≅_ (A B : Ty) : Set l where
    field
      f  : Tm (∙ ▹ A) B
      g  : Tm (∙ ▹ B) A
      fg : f [ p ,o g ] ≡ q
      gf : g [ p ,o f ] ≡ q
  open _≅_
  infix 4 _≅_

  +idr : ∀{A} → A +o Empty ≅ A
  f  (+idr {A}) = caseo q absurd
  g  (+idr {A}) = inl q
  fg (+idr {A}) =
    caseo q absurd [ p ,o inl q ]
      ≡⟨ +β₁' ⟩
    q [ p ,o q ]
      ≡⟨ ▹β₂ ⟩
    q
      ∎
  gf (+idr {A}) =
    inl q [ p ,o caseo q absurd ]
      ≡⟨ inl[] ⟩
    inl (q [ p ,o caseo q absurd ])
      ≡⟨ cong inl ▹β₂ ⟩
    inl (caseo q absurd)
      ≡⟨ sym {A = Tm _ _} +η ⟩
    caseo (inl (caseo q absurd) [ p ,o inl q ]) (inl (caseo q absurd) [ p ,o inr q ])
      ≡⟨ cong (λ x → caseo x (inl (caseo q absurd) [ p ,o inr q ])) inl[] ⟩
    caseo (inl (caseo q absurd [ p ,o inl q ])) (inl (caseo q absurd) [ p ,o inr q ])
      ≡⟨ cong (λ x → caseo (inl (caseo q absurd [ p ,o inl q ])) x) inl[] ⟩
    caseo (inl (caseo q absurd [ p ,o inl q ])) (inl (caseo q absurd [ p ,o inr q ]))
      ≡⟨ cong (λ x → caseo (inl {A = A}{Empty} x) (inl (caseo q absurd [ p ,o inr q ]))) +β₁' ⟩
    caseo (inl (q [ p ,o q ]))                  (inl (caseo q absurd [ p ,o inr q ]))
      ≡⟨ cong (λ x → caseo (inl  {A = A}{Empty} x) (inl (caseo q absurd [ p ,o inr q ]))) ▹β₂ ⟩
    caseo (inl q)                               (inl (caseo q absurd [ p ,o inr q ]))
      ≡⟨ cong (λ x → caseo (inl q) x) Emptyη ⟩
    caseo (inl q) absurd
      ≡⟨ cong (λ x → caseo (inl q) x) (sym {A = Tm _ _} Emptyη) ⟩
    caseo (inl q) (inr q)
      ≡⟨ cong (λ x → caseo (inl q) x) (sym {A = Tm _ _} ▹β₂) ⟩
    caseo (inl q) (q [ p ,o inr q ])
      ≡⟨ cong (λ x → caseo x (q [ p ,o inr q ])) (sym {A = Tm _ _} ▹β₂) ⟩
    caseo (q [ p ,o inl q ]) (q [ p ,o inr q ])
      ≡⟨ +η ⟩
    q
      ∎
{-
  +comm : ∀{A B} → A +o B ≅ B +o A
  f  (+comm {A} {B}) = caseo (inr q) (inl q)
  g  (+comm {A} {B}) = caseo (inr q) (inl q)
  fg (+comm {A} {B}) = {!!}
  gf (+comm {A} {B}) = {!!}
-}
\end{code}

\begin{exe}[compulsory]
  Show that for any two types \verb$A$, \verb$B$, there is an isomorphism between \verb$A +o B$ and \verb$B +o A$.
\end{exe}
The following exercise is a type theoretic version of the algebraic equation \verb$(a+b)² = a² + 2*a*b + b²$.
\begin{exe}[compulsory]
  Show that for any two types \verb$A$, \verb$B$, there is an isomorphism between \verb$(Unit +o Unit) ⇒ (A +o B)$ and \verb$((Unit +o Unit) ⇒ A +o (Unit +o Unit) ×o A ×o B) +o (Unit +o Unit) ⇒ B$. At least define the first two components (\verb$f$ and \verb$g$).
\end{exe}

\section{Standard model}

\begin{code}[hide]
St : Model
St = record
  { Con       = Set
  ; Sub       = λ Δ Γ → Δ → Γ
  ; _⊚_       = λ γ δ θ* → γ (δ θ*)
  ; ass       = λ {Γ}{Δ}{Θ}{Ξ} → refl {A = Ξ → Γ}
  ; id        = λ γ* → γ*
  ; idl       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; idr       = λ {Γ}{Δ} → refl {A = Δ → Γ}

  ; ∙         = Lift ⊤
  ; ε         = _
  ; ∙η        = λ {Γ}{σ} → refl {A = Γ → Lift ⊤}

  ; Ty        = Set

  ; Tm        = λ Γ A → Γ → A
  ; _[_]      = λ a γ δ* → a (γ δ*)
  ; [∘]       = λ {Γ}{Δ}{Θ}{A} → refl {A = Θ → A}
  ; [id]      = λ {Γ}{A}{a} → refl {A = Γ → A}
  ; _▹_       = _×_
  ; _,o_      = λ γ t δ* → γ δ* , t δ*
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; ▹β₂       = λ {Γ}{Δ}{A} → refl {A = Δ → A}
  ; ▹η        = λ {Γ}{Δ}{A} → refl {A = Δ → Γ × A}

  ; _⇒_       = λ A B → A → B
  ; lam       = λ t γ* α* → t (γ* , α*)
  ; _$_       = λ t u γ* → t γ* (u γ*)
  ; ⇒β        = λ {Γ}{A}{B}{t}{u} → refl {A = Γ → B}
  ; ⇒η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A → B}
  ; lam[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A → B}
  ; $[]       = λ {Γ}{A}{B}{t}{u}{Δ}{γ} → refl {A = Δ → B}
\end{code}

The new components:
\begin{code}
  ; _×o_      = _×_
  ; ⟨_,_⟩     = λ a b γ* → (a γ* , b γ*)
  ; fst       = λ t γ* → π₁ (t γ*)
  ; snd       = λ t γ* → π₂ (t γ*)
  ; ×β₁       = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → A}
  ; ×β₂       = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → B}
  ; ×η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A × B}
  ; ,[]       = λ {Γ}{A}{B}{u}{v}{Δ}{γ} → refl {A = Δ → A × B}

  ; Unit      = Lift ⊤
  ; trivial   = λ γ* → mk trivi
  ; Unitη     = λ {Γ}{u} → refl {A = Γ → Lift ⊤}

  ; _+o_      = _⊎_
  ; inl       = λ a γ* → ι₁ (a γ*)
  ; inr       = λ b γ* → ι₂ (b γ*)
  ; caseo     = λ u v γw* → case (π₂ γw*) (λ a* → u (π₁ γw* , a*)) λ b* → v (π₁ γw* , b*)
  ; +β₁       = λ {Γ}{A}{B}{C}{u}{v}{t} → refl {A = Γ → C}
  ; +β₂       = λ {Γ}{A}{B}{C}{u}{v}{t} → refl {A = Γ → C}
  ; +η        = λ {Γ}{A}{B}{C}{t} γw → un (ind⊎
                (λ x → Lift (case x (λ a* → t (π₁ γw , ι₁ a*)) (λ b* → t (π₁ γw , ι₂ b*)) ≡ t (π₁ γw , x)))
                (λ a → mk (refl {A = C}))
                (λ b → mk (refl {A = C}))
                (π₂ γw))
  ; inl[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A ⊎ B}
  ; inr[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A ⊎ B}
  ; case[]    = λ {Γ}{A}{B}{C}{u}{v}{Δ}{γ} → refl {A = Δ × (A ⊎ B) → C}

  ; Empty     = Lift ⊥
  ; absurd    = λ γb → exfalso (un (π₂ γb))
  ; Emptyη    = λ γa → exfalsop (un (π₂ γa))
\end{code}
\begin{code}[hide]
  }
\end{code}

Some equalities which hold in the standard model are not present in the syntax, for example:
\begin{code}[hide]
module EqualitiesInSt where
  module St = Model St
  open St
\end{code}
\begin{code}
  e1 : Con ≡ Ty
  e2 : ∀{Γ Δ} → Sub Δ Γ ≡ Tm Δ Γ
  e3 : ∀{Γ A Δ} → _≡_ {A = Sub Δ (Γ ▹ A) → Sub Δ Γ} (p ∘_)  fst
  e4 : ∀{Γ A Δ} → _≡_ {A = Sub Δ (Γ ▹ A) → Tm  Δ A} (q [_]) snd
  e5 : ∀{A} → Tm ∙ (Unit ×o A) ≡ Sub ∙ (∙ ▹ A)
\end{code}
\begin{code}[hide]
  e1 = refl {A = Set₁}
  e2 = refl {A = Set}
  e3 {Γ}{A}{Δ} = refl {A = (Δ → Γ × A) → Δ → Γ}
  e4 {Γ}{A}{Δ} = refl {A = (Δ → Γ × A) → Δ → A}
  e5 {A} = refl {A = Set}
\end{code}
