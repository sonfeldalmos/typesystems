\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Fix.OCPO where

open import Lib

module I {i} (A : Set i) where
  infix 5 _⊑_

  postulate
    D : Set
    _⊑_ : D → D → Prop

    η : A → D
    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

_,⊥ : ∀ {i} → Set i → Set
A ,⊥ = I.D A
\end{code}
\begin{code}
record ωCPO {i j k} (A : Set k) : Set (lsuc (i ⊔ j ⊔ k)) where
  infix 5 _⊑_
  
  field
    D : Set i
    _⊑_ : D → D → Prop j

    η : A → D
    ⊥ : D
    ∐ : (f : ℕ → D) → ((n : ℕ) → f n ⊑ f (S n)) → D

    refl⊑ : ∀ {d} → d ⊑ d
    trans⊑ : ∀ {a b c} → a ⊑ b → b ⊑ c → a ⊑ c
    antisym⊑ : ∀ {a b} → a ⊑ b → b ⊑ a → a ≡ b
    inf⊑ : ∀ {d} → ⊥ ⊑ d

    ιn : ∀ {f p d} → ((n : ℕ) → f n ⊑ d) → ∐ f p ⊑ d
    out : ∀ {f p d} → ∐ f p ⊑ d → (n : ℕ) → f n ⊑ d

  -------------------------------------------
  -- recursor
  -------------------------------------------
  
  private
    module J = I A
    
  postulate
    ⟦_⟧ : J.D → D
    ⟦_⟧⊑ : ∀ {a b} → a J.⊑ b → ⟦ a ⟧ ⊑ ⟦ b ⟧

    ⟦η⟧ : ∀ {a} → ⟦ J.η a ⟧ ≡ η a
    ⟦⊥⟧ : ⟦ J.⊥ ⟧ ≡ ⊥
    ⟦∐⟧ : ∀ {f p} → ⟦ J.∐ f p ⟧ ≡ ∐ (⟦_⟧ ∘ f) (λ n → ⟦ p n ⟧⊑)

-------------------------------------------
-- product ωCPO algebra
-------------------------------------------

_×ω_ : ωCPO {lzero}{lzero} (↑p 𝟘) → ωCPO {lzero}{lzero} (↑p 𝟘) → ωCPO {lzero}{lzero} (↑p 𝟘) 
Γ ×ω A = let module Γ = ωCPO Γ  
             module A = ωCPO A
         in record
             { D = Γ.D × A.D
             ; _⊑_ = λ u v → (π₁ u Γ.⊑ π₁ v) ×p (π₂ u A.⊑ π₂ v)
             ; η = λ b → ⟦ ↓[ b ]↓ ⟧𝟘
             ; ⊥ = Γ.⊥ , A.⊥
             ; ∐ = λ f p → (Γ.∐ (π₁ ∘ f) λ n → π₁ (p n)) , (A.∐ (π₂ ∘ f) λ n → π₂ (p n))
             ; refl⊑ = Γ.refl⊑ ,p A.refl⊑
             ; trans⊑ = λ u v → (Γ.trans⊑ (π₁ u) (π₁ v)) ,p ((A.trans⊑ (π₂ u) (π₂ v)))
             ; antisym⊑ = λ u v → ap2 _,_ (Γ.antisym⊑ (π₁ u) (π₁ v)) ((A.antisym⊑ (π₂ u) (π₂ v)))
             ; inf⊑ = Γ.inf⊑ ,p A.inf⊑
             ; ιn = λ u → (Γ.ιn (λ n → π₁ (u n))) ,p ((A.ιn (λ n → π₂ (u n))))
             ; out = λ u n → (Γ.out (π₁ u) n) ,p (A.out (π₂ u) n)
             }


-------------------------------------------
-- dependent ωCPO algebra
-------------------------------------------

record ωCPODep {i j k} (A : Set k) : Set (lsuc (i ⊔ j ⊔ k)) where
  module I' = I A 
  field
   D : I'.D → Set i
   _⊑_ : ∀ {a' b'} → D a' → D b' → a' I'.⊑ b' → Prop j

   η : (a' : A) → D (I'.η a')
   ⊥ : D (I'.⊥)
   ∐ : ∀ {f' p'} (f : (n : ℕ) → D (f' n)) → ((n : ℕ) → (f n ⊑ f (S n)) (p' n)) → D (I'.∐ f' p')

   refl⊑ : ∀ {d'}{d : D d'} → (d ⊑ d) I'.refl⊑
   trans⊑ : ∀ {a' b' c' u' v'} {a : D a'} {b : D b'} {c : D c'} →
     (a ⊑ b) u' → (b ⊑ c) v' → (a ⊑ c) (I'.trans⊑ u' v')
   antisym⊑ : ∀ {a' b' u' v'} {a : D a'} {b : D b'} →
     (a ⊑ b) u' → (b ⊑ a) v' → a =[ ap D (I'.antisym⊑ u' v') ]= b

   ιn : ∀ {f' : ℕ → I'.D} {p' : (n : ℕ) → f' n I'.⊑ f' (S n)} { d' u'}
     {d : D d'} {f : (n : ℕ) → D (f' n)}{p : (n : ℕ) → (f n ⊑ f (S n)) (p' n)} →
     ((n : ℕ) → (f n ⊑ d) (u' n)) → (∐ f p ⊑ d) (I'.ιn u')
   out : ∀ {f' : ℕ → I'.D} {p' : (n : ℕ) → f' n I'.⊑ f' (S n)} {d' u'} →
     {d : D d'} {f : (n : ℕ) → D (f' n)}{p : (n : ℕ) → (f n ⊑ f (S n)) (p' n)} →
     (∐ f p ⊑ d) u' → (n : ℕ) → (f n ⊑ d) (I'.out u' n) 

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  private
    module J = I A
    
  postulate
    ⟦_⟧ : (a : J.D) → D a
    ⟦_⟧⊑ : ∀ {a b} → (u : a J.⊑ b) → (⟦ a ⟧ ⊑ ⟦ b ⟧) u

    ⟦η⟧ : ∀ {a} → ⟦ J.η a ⟧ ≡ η a
    ⟦⊥⟧ : ⟦ J.⊥ ⟧ ≡ ⊥
    ⟦∐⟧ : ∀ {f p} → ⟦ J.∐ f p ⟧ ≡ ∐ (λ n → ⟦ f n ⟧) (λ n → ⟦ p n ⟧⊑)
    
I𝟘 : ∀ {i} (A : Set i) → ωCPO {lzero}{lzero} (↑p 𝟘)
I𝟘 A = let module I' = I A in record
         { D = I'.D
         ; _⊑_ = I'._⊑_
         ; η = λ b → ⟦ ↓[ b ]↓ ⟧𝟘
         ; ⊥ = I'.⊥
         ; ∐ = I'.∐
         ; refl⊑ = I'.refl⊑
         ; trans⊑ = I'.trans⊑
         ; antisym⊑ = I'.antisym⊑
         ; inf⊑ = I'.inf⊑
         ; ιn = I'.ιn
         ; out = I'.out
         }


-------------------------------------------
-- non-strict morphism
-------------------------------------------

record ωCPOMor (Γ Δ : ωCPO {lzero}{lzero} (↑p 𝟘)) : Set where
  private
    module Γ = ωCPO Γ
    module Δ = ωCPO Δ
  field
    D : Γ.D → Δ.D
    _⊑_ : ∀{γ γ'} → γ Γ.⊑ γ' → D γ Δ.⊑ D γ'
    ∐ : ∀ {f p} → D (Γ.∐ f p) ≡ Δ.∐ (D ∘ f) λ n →  _⊑_ (p n) 

-- identity morphism
idm : ∀{Γ} → ωCPOMor Γ Γ
idm = record { D = idf ; _⊑_ = λ u → u ; ∐ = refl }

-- composition of morphisms
_∘ₘ_ : ∀{Γ Δ Θ} → ωCPOMor Δ Θ → ωCPOMor Γ Δ → ωCPOMor Γ Θ
G ∘ₘ F = let module F = ωCPOMor F
             module G = ωCPOMor G
         in record { D = G.D ∘ F.D ; _⊑_ = λ u → G._⊑_ (F._⊑_ u) ; ∐ = ap G.D F.∐ ◾ G.∐ }

≡mor : ∀{Γ Δ} {F G : ωCPOMor Γ Δ} → let open ωCPOMor in
     D F ≡ D G → (∀{u} → D F u ≡ D G u) → F ≡ G
≡mor refl v = refl

module PartialityMonad where
  return : ∀ {i} {A : Set i} → A → A ,⊥
  return {A = A} = I.η A

  private
    P : ∀ {i} (A : Set i) → ωCPO (A ,⊥)
    P A = let open I A
                in  record
                      { D = D
                      ; _⊑_ = _⊑_
                      ; η = idf
                      ; ⊥ = ⊥
                      ; ∐ = ∐
                      ; refl⊑ = refl⊑
                      ; trans⊑ = trans⊑
                      ; antisym⊑ = antisym⊑
                      ; inf⊑ = inf⊑
                      ; ιn = ιn
                      ; out = out
                      }
    module P {i} {A : Set i} = ωCPO (P A)

  join : ∀ {i} {A : Set i} → A ,⊥ ,⊥ → A ,⊥
  join = P.⟦_⟧

  private
    Q : ∀ {i j} {A : Set i}{B : Set j} → (A → B) → ωCPO A
    Q {A = A}{B} f = let open I B
                     in  record
                           { D = D
                           ; _⊑_ = _⊑_
                           ; η = η ∘ f
                           ; ⊥ = ⊥
                           ; ∐ = ∐
                           ; refl⊑ = refl⊑
                           ; trans⊑ = trans⊑
                           ; antisym⊑ = antisym⊑
                           ; inf⊑ = inf⊑
                           ; ιn = ιn
                           ; out = out
                           }
    module Q {i}{j} {A : Set i}{B : Set j} (f : A → B) = ωCPO (Q f)

  fmap : ∀ {i j} {A : Set i}{B : Set j} → (A → B) → A ,⊥ → B ,⊥
  fmap = Q.⟦_⟧

  open I

  -- proof of functor laws

  fmapid : ∀ {i} {A : Set i} {a : A ,⊥} → fmap idf a ≡ a
  fmapid {A = A} = {!!} 

  -- lemmas for the Fix model

  fmap⊑ : ∀ {i j} {A : Set i}{B : Set j}{a a' : A ,⊥}{f} →
    _⊑_ A a a' → _⊑_ B (fmap f a) (fmap f a')
  fmap⊑ {f = f} = Q.⟦_⟧⊑ f

  fmap∐ : ∀ {i j} {A : Set i}{B : Set j}{a a' : A ,⊥}{f g p} →
    fmap g (∐ A f p) ≡ ∐ B (fmap g ∘ f) λ n → fmap⊑ (p n)
  fmap∐ {g = g} = Q.⟦∐⟧ g

  fmapη : ∀{i j} {A : Set i} {B : Set j}{a : A}{f} →
    fmap f (η A a) ≡ η B (f a)
  fmapη {f = f} = Q.⟦η⟧ f

  infixl 7 _<$>_
  _<$>_ = fmap

  infixl 6 _>>=_
  _>>=_ : ∀ {i j} {A : Set i}{B : Set j} → A ,⊥ → (A → B ,⊥) → B ,⊥
  ma >>= f = join (fmap f ma)

  infixl 7 _<*>_
  _<*>_ : ∀{i j} {A : Set i} {B : Set j} → (A → B) ,⊥ → A ,⊥ → B ,⊥
  f <*> x  = f >>= _<$> x
  
\end{code}
