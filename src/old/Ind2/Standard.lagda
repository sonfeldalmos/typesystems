\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module Ind2.Standard where

open import Lib renaming (_∘_ to _∘f_ ; _×_ to _×m_ ; _,_ to _⸴_)
open import Ind2.Algebra
\end{code}
\begin{code}
module M where
  data Ar (C : Set) : Set where
    X   : Ar C
    X⇒_ : Ar C → Ar C
    C⇒_ : Ar C → Ar C

  data Sign (C : Set) : Set where
    ◆   : Sign C
    _▷_ : Sign C → Ar C → Sign C

  infixr 6 X⇒_
  infixr 6 C⇒_
  infixl 5 _▷_

  data VarI (C : Set) : Sign C → Ar C → Set where
    vvz : ∀ {Γ A} → VarI C (Γ ▷ A) A
    vvs : ∀ {Γ A B} → VarI C Γ A → VarI C (Γ ▷ B) A

  data TmI (C : Set) : Sign C → Ar C → Set where
    var : ∀ {Γ A} → VarI C Γ A → TmI C Γ A
    _$_ : ∀ {Γ A} → TmI C Γ (X⇒ A) → TmI C Γ X → TmI C Γ A
    _$̂_ : ∀ {Γ A} → TmI C Γ (C⇒ A) → C → TmI C Γ A

  data SubI (C : Set) : Sign C → Sign C → Set where
    ε   : ∀ {Γ} → SubI C Γ ◆
    _,_ : ∀ {Γ Δ A} → SubI C Γ Δ → TmI C Γ A → SubI C Γ (Δ ▷ A)

  infixl 6 _$_
  infixl 6 _$̂_
  infixl 5 _,_

  vz : ∀ {C Γ A} → TmI C (Γ ▷ A) A
  vz = var vvz

  vs : ∀ {C Γ A B} → TmI C Γ A → TmI C (Γ ▷ B) A
  vs (var x) = var (vvs x)
  vs (t $ u) = vs t $ vs u
  vs (t $̂ c) = vs t $̂ c

  wk : ∀ {C Γ Δ A} → SubI C Γ Δ → SubI C (Γ ▷ A) Δ
  wk ε       = ε
  wk (σ , t) = wk σ , vs t

  id : ∀ {C Γ} → SubI C Γ Γ
  id {Γ = ◆}     = ε
  id {Γ = Γ ▷ A} = wk id , vz

  _ᴬA : ∀ {C} → Ar C → Set → Set
  (X      ᴬA) T = T
  ((X⇒ A) ᴬA) T = T → (A ᴬA) T
  _ᴬA {C} (C⇒ A) T = C → (A ᴬA) T

  _ᴬ : ∀ {C} → Sign C → Set → Set
  (◆       ᴬ) T = ↑p 𝟙
  ((Γ ▷ A) ᴬ) T = (Γ ᴬ) T ×m (A ᴬA) T

  _ᴬv : ∀ {C Γ A} → VarI C Γ A → ∀ {T} → (Γ ᴬ) T → (A ᴬA) T
  (vvz   ᴬv) (γ ⸴ α) = α
  (vvs x ᴬv) (γ ⸴ β) = (x ᴬv) γ
  
  _ᴬt : ∀ {C Γ A} → TmI C Γ A → ∀ {T} → (Γ ᴬ) T → (A ᴬA) T
  (var x   ᴬt) γ = (x ᴬv) γ
  ((t $ u) ᴬt) γ = (t ᴬt) γ ((u ᴬt) γ)
  ((t $̂ c) ᴬt) γ = (t ᴬt) γ c

  _ᴬs : ∀ {C Γ Δ} → SubI C Γ Δ → ∀ {T} → (Γ ᴬ) T → (Δ ᴬ) T
  (ε       ᴬs) γ = *↑
  ((σ , t) ᴬs) γ = (σ ᴬs) γ ⸴ (t ᴬt) γ

  vsᴬ : ∀ {C Γ A B T} {t : TmI C Γ A} → (vs {B = B} t ᴬt){T} ≡ (t ᴬt) ∘f π₁
  vsᴬ {t = var x}  = refl
  vsᴬ {t = t $ u}  = ap2 _⊗_ (vsᴬ {t = t}) (vsᴬ {t = u})
  vsᴬ {t = t $̂ c}  = ap (λ f x → f x c) (vsᴬ {t = t})

  wkᴬ : ∀ {C Γ Δ A T} {σ : SubI C Γ Δ} → (wk {A = A} σ ᴬs){T} ≡ (σ ᴬs) ∘f π₁
  wkᴬ {σ = ε}     = refl
  wkᴬ {σ = σ , t} = ap2 (λ f g x → f x ⸴ g x) (wkᴬ {σ = σ}) (vsᴬ {t = t})

  idᴬ : ∀ {C T} {Γ : Sign C} → (id {Γ = Γ} ᴬs){T} ≡ idf
  idᴬ {Γ = ◆}     = refl
  idᴬ {Γ = Γ ▷ A} = ap (λ f γ → f γ ⸴ π₂ γ)
                       (wkᴬ {σ = id} ◾ ap (_∘f π₁) (idᴬ {Γ = Γ}))

  _ᴹA : ∀ {A B C} (R : Ar C) → (A → B) → (R ᴬA) A → (R ᴬA) B → Prop
  (X      ᴹA) F a b = F a ≡ b
  ((X⇒ R) ᴹA) F f g = ∀ a → (R ᴹA) F (f a) (g (F a))
  ((C⇒ R) ᴹA) F f g = ∀ c → (R ᴹA) F (f c) (g c)

  _ᴹ : ∀ {A B C} (Ω : Sign C) → (A → B) → (Ω ᴬ) A → (Ω ᴬ) B → Prop
  (◆       ᴹ) F .*↑     .*↑       = 𝟙'
  ((Ω ▷ R) ᴹ) F (γ ⸴ α) (γ' ⸴ α') = (Ω ᴹ) F γ γ' ×p (R ᴹA) F α α'

  ind : ∀ {C} → Sign C → Set
  ind {C} Ω = TmI C Ω X

  conᵗ : ∀ {C Ω A} → TmI C Ω A → (A ᴬA) (ind Ω)
  conᵗ {A = X}    t = t
  conᵗ {A = X⇒ A} f = λ u → conᵗ (f $ u)
  conᵗ {A = C⇒ A} f = λ c → conᵗ (f $̂ c)

  conˢ : ∀ {C Ω Γ} → SubI C Ω Γ → (Γ ᴬ) (ind Ω)
  conˢ ε       = *↑
  conˢ (σ , t) = conˢ σ ⸴ conᵗ t

  con : ∀ {C} (Ω : Sign C) → (Ω ᴬ) (ind Ω)
  con Ω = conˢ id

  rec : ∀ {C A} (Ω : Sign C) → (Ω ᴬ) A → ind Ω → A
  rec Ω ω t = (t ᴬt) ω
{-
  _[_]v : ∀ {C Γ Δ A} → VarI C Δ A → SubI C Γ Δ → TmI C Γ A
  vvz   [ σ , t ]v = t
  vvs x [ σ , t ]v = x [ σ ]v

  _[_]t : ∀ {C Γ Δ A} → TmI C Δ A → SubI C Γ Δ → TmI C Γ A
  (var x) [ σ ]t = x [ σ ]v
  (t $ u) [ σ ]t = t [ σ ]t $ u [ σ ]t
  (t $̂ c) [ σ ]t = t [ σ ]t $̂ c

  [wk]v : ∀ {C Γ Δ A B} {x : VarI C Δ A}{σ : SubI C Γ Δ} →
    x [ wk {A = B} σ ]v ≡ vs (x [ σ ]v)
  [wk]v {x = vvz}  {σ , t} = refl
  [wk]v {x = vvs x}{σ , t} = [wk]v {x = x}

  [id]v : ∀ {C Γ A} {x : VarI C Γ A} → x [ id ]v ≡ var x
  [id]v {x = vvz}   = refl
  [id]v {x = vvs x} = [wk]v {x = x}{id} ◾ ap vs ([id]v {x = x})

  [id]t : ∀ {C Γ A} {t : TmI C Γ A} → t [ id ]t ≡ t
  [id]t {t = var x} = [id]v {x = x}
  [id]t {t = t $ u} = ap2 _$_ ([id]t {t = t}) ([id]t {t = u})
  [id]t {t = t $̂ c} = ap (_$̂ c) ([id]t {t = t})

  coh-conⱽ : ∀ {C Ω Γ A} (x : VarI C Γ A)(σ : SubI C Ω Γ) →
    (x ᴬv) (conˢ σ) ≡ conᵗ (x [ σ ]v)
  coh-conⱽ vvz     (σ , t) = refl
  coh-conⱽ (vvs x) (σ , t) = coh-conⱽ x σ

  coh-con : ∀ {C Ω Γ A} (t : TmI C Γ A)(σ : SubI C Ω Γ) →
    (t ᴬt) (conˢ σ) ≡ conᵗ (t [ σ ]t)
  coh-con (var x) σ = coh-conⱽ x σ
  coh-con (t $ u) σ = ap2 (λ f x → f x) (coh-con t σ) (coh-con u σ)
  coh-con (t $̂ c) σ = happly (coh-con t σ) c

  con-lem : ∀ {C Ω} (t : TmI C Ω X) → (t ᴬt) (con Ω) ≡ t
  con-lem t = coh-con t id ◾ [id]t {t = t}

  indβᵗ : ∀ {A C Ω R} {ω : (Ω ᴬ) A}(t : TmI C Ω R) →
    (R ᴹA) (rec Ω ω) ((t ᴬt) (con Ω)) ((t ᴬt) ω)
  indβᵗ {R = X}    t = ap (rec _ _) (con-lem t)
  indβᵗ {R = X⇒ R} t = λ u →
    transportp (λ y → (R ᴹA) (rec _ _) ((t ᴬt) _ y) ((t ᴬt) _ (rec _ _ u)))
               (con-lem u) (indβᵗ (t $ u))
  indβᵗ {R = C⇒ R} t = λ c → indβᵗ (t $̂ c)

  indβˢ : ∀ {A C Ω Γ} {ω : (Ω ᴬ) A}(σ : SubI C Ω Γ) →
    (Γ ᴹ) (rec Ω ω) ((σ ᴬs) (con Ω)) ((σ ᴬs) ω)
  indβˢ ε       = *
  indβˢ (σ , t) = indβˢ σ ,p indβᵗ t

  indβ : ∀ {A C} {Ω : Sign C}{ω : (Ω ᴬ) A} →
    (Ω ᴹ) (rec Ω ω) (con Ω) ω
  indβ {Ω = Ω}{ω} =
    transportp2 ((Ω ᴹ) (rec Ω ω))
                (happly idᴬ (con Ω)) (happly idᴬ ω) (indβˢ id)
-}
  indβᵗ : ∀ {A C Ω R} {ω : (Ω ᴬ) A}(t : TmI C Ω R) →
    (R ᴹA) (rec Ω ω) (conᵗ t) ((t ᴬt) ω)
  indβᵗ {R = X} t    = refl
  indβᵗ {R = X⇒ R} t = λ u → indβᵗ (t $ u)
  indβᵗ {R = C⇒ R} t = λ c → indβᵗ (t $̂ c)

  indβˢ : ∀ {A C Ω Γ} {ω : (Ω ᴬ) A}(σ : SubI C Ω Γ) →
    (Γ ᴹ) (rec Ω ω) (conˢ σ) ((σ ᴬs) ω)
  indβˢ ε       = *
  indβˢ (σ , t) = indβˢ σ ,p indβᵗ t
  
  indβ : ∀ {A C} {Ω : Sign C}{ω : (Ω ᴬ) A} →
    (Ω ᴹ) (rec Ω ω) (con Ω) ω
  indβ {Ω = Ω}{ω} =
    transportp ((Ω ᴹ) (rec Ω ω) (con Ω)) (happly idᴬ ω) (indβˢ id)

  _ᴵᴹA : ∀ {Γ : Set}{A B C} (R : Ar C) →
    (Γ → A → B) → (Γ → (R ᴬA) A) → (Γ → (R ᴬA) B) → Prop
  (X      ᴵᴹA) F t u = F ⊗ t ≡ u
  ((X⇒ R) ᴵᴹA) F f g = ∀ t → (R ᴵᴹA) F (f ⊗ t) (g ⊗ (F ⊗ t))
  ((C⇒ R) ᴵᴹA) F f g = ∀ t → (R ᴵᴹA) F (f ⊗ t) (g ⊗ t)

  _ᴵᴹ : ∀ {Γ : Set}{A B C} (Ω : Sign C) →
    (Γ → A → B) → (Γ → (Ω ᴬ) A) → (Γ → (Ω ᴬ) B) → Prop
  (◆       ᴵᴹ) F t u = 𝟙'
  ((Ω ▷ R) ᴵᴹ) F t u =
    (Ω ᴵᴹ) F (π₁ ∘f t) (π₁ ∘f u) ×p (R ᴵᴹA) F (π₂ ∘f t) (π₂ ∘f u)

  gen-indβᵗ : ∀ {Γ : Set}{A B C}
    (R : Ar C)(F : Γ → A → B)(t : Γ → (R ᴬA) A)(u : Γ → (R ᴬA) B) →
    (∀ γ → (R ᴹA) (F γ) (t γ) (u γ)) → (R ᴵᴹA) F t u
  gen-indβᵗ X      F t u p = funext p
  gen-indβᵗ (X⇒ R) F f g p = λ t → gen-indβᵗ R F (f ⊗ t) (g ⊗ (F ⊗ t))
                                             (λ γ → p γ (t γ))
  gen-indβᵗ (C⇒ R) F f g p = λ t → gen-indβᵗ R F (f ⊗ t) (g ⊗ t)
                                             (λ γ → p γ (t γ))

  gen-indβˢ : ∀ {Γ : Set}{A B C}
    (Ω : Sign C)(F : Γ → A → B)(t : Γ → (Ω ᴬ) A)(u : Γ → (Ω ᴬ) B) →
    (∀ γ → (Ω ᴹ) (F γ) (t γ) (u γ)) → (Ω ᴵᴹ) F t u
  gen-indβˢ ◆       F t u p = *
  gen-indβˢ (Ω ▷ R) F t u p =
       gen-indβˢ Ω F (π₁ ∘f t) (π₁ ∘f u) (λ γ → π₁ (p γ))
    ,p gen-indβᵗ R F (π₂ ∘f t) (π₂ ∘f u) (λ γ → π₂ (p γ))

  indβᴵ : ∀ {Γ : Set}{A C} {Ω : Sign C}{ω : Γ → (Ω ᴬ) A} →
    (Ω ᴵᴹ) (λ γ → rec Ω (ω γ)) (λ _ → con Ω) ω
  indβᴵ {Ω = Ω}{ω} = gen-indβˢ Ω (λ γ → rec Ω (ω γ)) (λ _ → con Ω) ω
                                 (λ γ → indβ)

{-
  _ⱽ : ∀ {C} → Sign C → Sign C → Set
  (◆ ⱽ) Ω' = ↑p 𝟙
  _ⱽ {C} (Ω ▷ R) Ω' = (Ω ⱽ) Ω' ×m VarI C Ω' R

  shift : ∀ {C} {Ω Ω' : Sign C}{Q : Ar C} → (Ω ⱽ) Ω' → (Ω ⱽ) (Ω' ▷ Q)
  shift {Ω = ◆} .*↑ = *↑
  shift {Ω = Ω ▷ R} (α ,Σ v) = shift α ,Σ vs v

  conVar : ∀ {C} (Ω : Sign C) → (Ω ⱽ) Ω
  conVar ◆ = *↑
  conVar (Ω ▷ R) = shift (conVar Ω) ,Σ vz

  conA : ∀ {C} {R : Ar C}{Ω' : Sign C} → TmI C Ω' R → (R ᴬA) (ind Ω')
  conA {R = X} t = t
  conA {R = X⇒ R} f = λ u → conA (f $ u)
  conA {R = C⇒ R} f = λ a → conA (f $̂ a)

  con' : ∀ {C} {Ω Ω' : Sign C}→ (Ω ⱽ) Ω' → (Ω ᴬ) (ind Ω')
  con' {Ω = ◆} .*↑ = *↑
  con' {Ω = Ω ▷ R} (α ,Σ v) = con' α ,Σ conA (var v)

  con : ∀ {C} (Ω : Sign C) → (Ω ᴬ) (ind Ω)
  con Ω = con' (conVar Ω)

  recVar : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → VarI C Ω R → (R ᴬA) A
  recVar (ω ,Σ c) vz = c
  recVar (ω ,Σ c) (vs v) = recVar ω v

  rec' : ∀ {A C} {R : Ar C}{Ω : Sign C} → (Ω ᴬ) A → TmI C Ω R → (R ᴬA) A
  rec' ω (var v) = recVar ω v
  rec' ω (f $ u) = rec' ω f (rec' ω u)
  rec' ω (f $̂ a) = rec' ω f a

  rec : ∀ {A C} (Ω : Sign C) → (Ω ᴬ) A → ind Ω → A
    rec Ω ω t = rec' ω t

  _ᴹA : ∀ {Γ : Set} {A B C} (R : Ar C) →
    ((Γ → A) → (Γ → B)) → (Γ → (R ᴬA) A) → (Γ → (R ᴬA) B) → Prop
  (X      ᴹA) F t u = F t ≡ u
  ((X⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f ⊗ t) (g ⊗ F t)
  ((C⇒ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f ⊗ t) (g ⊗ t)

  _ᴹ : ∀ {Γ : Set} {A B C} (Ω : Sign C) →
    ((Γ → A) → (Γ → B)) → (Γ → (Ω ᴬ) A) → (Γ → (Ω ᴬ) B) → Prop
  (◆       ᴹ) F t u = 𝟙'
  ((Ω ▷ R) ᴹ) F t u =
    (Ω ᴹ) F (π₁ ∘f t) (π₁ ∘f u) ×p (R ᴹA) F (π₂ ∘f t) (π₂ ∘f u)

  module _ {Γ : Set} {A C} {Ω : Sign C} {ω : Γ → (Ω ᴬ) A} where
    F : (Γ → ind Ω) → (Γ → A)
    F u γ = rec Ω (ω γ) (u γ)

    Ι : Γ → (Ω ᴬ) (ind Ω)
    Ι _ = con Ω

    indβᵗ : ∀ {R} (t : TmI C Ω R) → (R ᴹA) F ((t ᴬt) ∘f Ι) ((t ᴬt) ∘f ω)
    indβᵗ {X}    t = {!!}
    indβᵗ {X⇒ R} t = {!!}
    indβᵗ {C⇒ R} t = λ c → indβᵗ (t $̂ c {!!})

    indβˢ : ∀ {Ψ} (σ : SubI C Ω Ψ) → (Ψ ᴹ) F ((σ ᴬs) ∘f Ι) ((σ ᴬs) ∘f ω)
    indβˢ ε       = *'
    indβˢ (σ , t) = indβˢ σ ,p indβᵗ t
    
    indβ : (Ω ᴹ) F Ι ω
    indβ = transportp2 ((Ω ᴹ) F)
                       (ap (_∘f Ι) idᴬ)
                       (ap (_∘f ω) idᴬ)
                       (indβˢ id)
-}

St : Algebra
St = record
       { Ty = Set
       ; Con = Set
       ; Ar = M.Ar
       ; Sign = M.Sign
       ; Sub = λ Γ Δ → Γ → Δ
       ; Tm = λ Γ A → Γ → A
       ; Unit = ↑p 𝟙
       ; Empty = ↑p 𝟘
       ; _⇒_ = λ A B → A → B
       ; _×_ = _×m_
       ; _+_ = _⊎_
       ; ind = M.ind
       ; ∙ = ↑p 𝟙
       ; _▹_ = _×m_
       ; X = M.X
       ; X⇒_ = M.X⇒_
       ; C⇒_ = M.C⇒_
       ; ◆ = M.◆
       ; _▷_ = M._▷_
       ; _ᴬA = M._ᴬA
       ; _ᴬ = M._ᴬ
       ; _ᴹA = M._ᴵᴹA
       ; _ᴹ = M._ᴵᴹ
       ; _∘_ = _∘f_ 
       ; id = idf
       ; ε = const *↑
       ; _,_ = λ σ t γ → σ γ ⸴ t γ
       ; p = π₁
       ; q = π₂
       ; _[_] = _∘f_
       ; lam = λ t γ a → t (γ ⸴ a)
       ; app = λ t γa → t (π₁ γa) (π₂ γa)
       ; tt = const *↑
       ; ⟨_,_⟩ = λ u v γ → u γ ⸴ v γ
       ; proj₁ = λ t γ → π₁ (t γ)
       ; proj₂ = λ t γ → π₂ (t γ)
       ; absurd = λ t γ → ⟦ ↓[ t γ ]↓ ⟧𝟘
       ; inl = λ t γ → ι₁ (t γ)
       ; inr = λ t γ → ι₂ (t γ)
       ; case = λ t u v γ →
                  case⊎ (t γ) (λ a → u (γ ⸴ a)) (λ b → v (γ ⸴ b))
       ; con = λ Ω γ → M.con Ω
       ; rec = λ Ω ω γ u → M.rec Ω (ω γ) u
       ; ᴬAX = refl
       ; ᴬAX⇒ = refl
       ; ᴬAC⇒ = refl
       ; ᴬ◆ = refl
       ; ᴬ▷ = refl
       ; ᴹAX = refl
       ; ᴹAX⇒ = refl
       ; ᴹAC⇒ = refl
       ; ᴹ◆ = refl
       ; ᴹ▷ = refl
       ; ass = refl
       ; idl = refl
       ; idr = refl
       ; ∙η = refl
       ; ▹β₁ = refl
       ; ▹β₂ = refl
       ; ▹η = refl
       ; [id] = refl
       ; [∘] = refl
       ; ⇒β = refl
       ; ⇒η = refl
       ; uη = refl
       ; ×β₁ = refl
       ; ×β₂ = refl
       ; ×η = refl
       ; +β₁ = refl
       ; +β₂ = refl
       ; indβ = M.indβᴵ
       ; lam[] = refl
       ; tt[] = refl
       ; ⟨,⟩[] = refl
       ; absurd[] = refl
       ; inl[] = refl
       ; inr[] = refl
       ; case[] = refl
       ; con[] = refl
       ; rec[] = refl
       }
module St = Algebra St
\end{code}
