\chapter{Inductive types}

\begin{tcolorbox}[title=Learning goals of this chapter]
  
\end{tcolorbox}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}
module Ind3.Algebra where

open import Lib hiding (_∘_ ; _×_ ; _,_)
\end{code}
\begin{code}
module I where
  infixr 6 X⇒_
  infixr 6 _⇛_
  infixl 5 _▷_
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  data Ty  : Set
  data Con : Set
  data Ar   (n : ℕ) : Set
  data Sign (n : ℕ) : Set

  data Ty where
    Unit  : Ty
    Empty : Ty
    _⇒_   : Ty → Ty → Ty
    _×_   : Ty → Ty → Ty
    _+_   : Ty → Ty → Ty
    ind   : ∀ {n} → Sign n → 𝕍 Ty n → Ty

  data Con where
    ∙   : Con
    _▹_ : Con → Ty → Con

  data Ar n where
    X   : Ar n
    X⇒_ : Ar n → Ar n
    _⇛_ : Fin n → Ar n → Ar n

  data Sign n where
    ◆   : Sign n
    _▷_ : Sign n → Ar n → Sign n
  
  postulate
    Sub : Con → Con → Set
    Tm : Con → Ty → Set

  _ᴬA : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
  (X       ᴬA) P A = A
  ((X⇒ R)  ᴬA) P A = A ⇒ (R ᴬA) P A
  ((k ⇛ R) ᴬA) P A = (P ‼ k) ⇒ (R ᴬA) P A

  _ᴬ : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty
  (◆       ᴬ) P A = Unit
  ((Ω ▷ R) ᴬ) P A = (Ω ᴬ) P A × (R ᴬA) P A

  postulate
    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ

    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A

    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C

    con : ∀ {Γ n} (Ω : Sign n) P → Tm Γ ((Ω ᴬ) P (ind Ω P))
    rec : ∀ {Γ A n P} (Ω : Sign n) → Tm Γ ((Ω ᴬ) P A) → Tm Γ (ind Ω P ⇒ A)

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  _ᴹA : ∀ {Γ A B n} (R : Ar n) {P} →
    Tm Γ (A ⇒ B) → Tm Γ ((R ᴬA) P A) → Tm Γ ((R ᴬA) P B) → Prop
  (X       ᴹA) F t u = F $ t ≡ u
  ((X⇒ R)  ᴹA) F f g = ∀ t → (R ᴹA) F (f $ t) (g $ (F $ t))
  ((k ⇛ R) ᴹA) F f g = ∀ t → (R ᴹA) F (f $ t) (g $ t)

  _ᴹ : ∀ {Γ A B n} (Ω : Sign n) {P} →
    Tm Γ (A ⇒ B) → Tm Γ ((Ω ᴬ) P A) → Tm Γ ((Ω ᴬ) P B) → Prop
  (◆       ᴹ) F γ δ = 𝟙
  ((Ω ▷ R) ᴹ) F γ δ =
    (Ω ᴹ) F (proj₁ γ) (proj₁ δ) ×p (R ᴹA) F (proj₂ γ) (proj₂ δ)

  postulate
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε
    
    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    uη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]

    indβ : ∀ {Γ A n P} (Ω : Sign n)(ω : Tm Γ ((Ω ᴬ) P A)) →
      (Ω ᴹ) (rec Ω ω) (con Ω P) ω

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
        case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    con[] : ∀ {Γ Δ n P} {Ω : Sign n}{σ : Sub Γ Δ} →
      (con Ω P) [ σ ] ≡ con Ω P
    rec[] : ∀ {Γ Δ A n P} {Ω : Sign n}{u : Tm Δ ((Ω ᴬ) P A)}{σ : Sub Γ Δ} →
      (rec Ω u) [ σ ] ≡ rec Ω (u [ σ ])

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE ⇒β ⇒η #-}
  {-# REWRITE ×β₁ ×β₂ ×η #-}
  {-# REWRITE +β₁ +β₂ #-}
  {-# REWRITE lam[] app[] #-}
  {-# REWRITE tt[] ⟨,⟩[] proj₁[] proj₂[] #-}
  {-# REWRITE absurd[] inl[] inr[] case[] #-}
  {-# REWRITE con[] rec[] #-}

record Algebra {a b c d e f} : Set (lsuc (a ⊔ b ⊔ c ⊔ d ⊔ e ⊔ f)) where
  infixr 6 X⇒_
  infixr 6 _⇛_
  infixl 5 _▷_
  infixl 5 _▹_
  infixr 5 _⇒_
  infixl 7 _×_
  infixl 6 _+_
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]
  infixl 5 _$_
  
  field
    Ty : Set a
    Con : Set b
    Ar : ℕ → Set c
    Sign : ℕ → Set d
    Sub : Con → Con → Set e
    Tm : Con → Ty → Set f

    Unit : Ty
    Empty : Ty
    _⇒_ : Ty → Ty → Ty
    _×_ : Ty → Ty → Ty
    _+_ : Ty → Ty → Ty
    ind : ∀ {n} → Sign n → 𝕍 Ty n → Ty

    ∙ : Con
    _▹_ : Con → Ty → Con
    
    X : ∀ {n} → Ar n
    X⇒_ : ∀ {n} → Ar n → Ar n
    _⇛_ : ∀ {n} → Fin n → Ar n → Ar n
    
    ◆ : ∀ {n} → Sign n
    _▷_ : ∀ {n} → Sign n → Ar n → Sign n

  private
    M : Con → Ty → Ty → Ty → Ty → Set (lsuc f)
    M Γ A B Aᴬ Bᴬ = Tm Γ (A ⇒ B) → Tm Γ Aᴬ → Tm Γ Bᴬ → Prop f

  field
    _ᴬA : ∀ {n} → Ar n → 𝕍 Ty n → Ty → Ty
    _ᴬ : ∀ {n} → Sign n → 𝕍 Ty n → Ty → Ty

    _ᴹA : ∀ {Γ A B n} (R : Ar n) {P} → M Γ A B ((R ᴬA) P A) ((R ᴬA) P B)
    _ᴹ : ∀ {Γ A B n} (Ω : Sign n) {P} → M Γ A B ((Ω ᴬ) P A) ((Ω ᴬ) P B)

    _∘_ : ∀ {Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id : ∀ {Γ} → Sub Γ Γ
    ε : ∀ {Γ} → Sub Γ ∙
    _,_ : ∀ {Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p : ∀ {Γ A} → Sub (Γ ▹ A) Γ
    
    q : ∀ {Γ A} → Tm (Γ ▹ A) A
    _[_] : ∀ {Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    
    lam : ∀ {Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    
    tt : ∀ {Γ} → Tm Γ Unit
    ⟨_,_⟩ : ∀ {Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A × B)
    proj₁ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ A
    proj₂ : ∀ {Γ A B} → Tm Γ (A × B) → Tm Γ B

    absurd : ∀ {Γ A} → Tm Γ Empty → Tm Γ A
    inl : ∀ {Γ A B} → Tm Γ A → Tm Γ (A + B)
    inr : ∀ {Γ A B} → Tm Γ B → Tm Γ (A + B)
    case : ∀ {Γ A B C} →
      Tm Γ (A + B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C 

    con : ∀ {Γ n} (Ω : Sign n) P → Tm Γ ((Ω ᴬ) P (ind Ω P))
    rec : ∀ {Γ A n P} (Ω : Sign n) → Tm Γ ((Ω ᴬ) P A) → Tm Γ (ind Ω P ⇒ A)

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = def u (app t)

  field
    ᴬAX : ∀ {A n P} → (X {n} ᴬA) P A ≡ A
    ᴬAX⇒ : ∀ {A n P} {R : Ar n} → ((X⇒ R) ᴬA) P A ≡ A ⇒ (R ᴬA) P A
    ᴬA⇛ : ∀ {A n P k} {R : Ar n} → ((k ⇛ R) ᴬA) P A ≡ (P ‼ k) ⇒ (R ᴬA) P A

    ᴬ◆ : ∀ {A n P} → (◆ {n} ᴬ) P A ≡ Unit
    ᴬ▷ : ∀ {A n P} {Ω : Sign n}{R : Ar n} →
      ((Ω ▷ R) ᴬ) P A ≡ (Ω ᴬ) P A × (R ᴬA) P A

    ᴹAX : ∀ {Γ A B n P} {F : Tm Γ (A ⇒ B)}{t : Tm Γ A}{u : Tm Γ B} →
      coe (ap2 (M Γ A B) ᴬAX ᴬAX) ((X {n} ᴹA){P}) F t u ≡ (F $ t ≡ u)
    ᴹAX⇒ : ∀ {Γ A B n P} {R : Ar n}{F : Tm Γ (A ⇒ B)}
      {f : Tm Γ (A ⇒ (R ᴬA) P A)}{g : Tm Γ (B ⇒ (R ᴬA) P B)} →
      coe (ap2 (M Γ A B) ᴬAX⇒ ᴬAX⇒) ((X⇒ R) ᴹA) F f g ≡
        (∀ t → (R ᴹA) F (f $ t) (g $ (F $ t)))
    ᴹA⇛ : ∀ {Γ A B n P k} {R : Ar n}{F : Tm Γ (A ⇒ B)}
      {f : Tm Γ ((P ‼ k) ⇒ (R ᴬA) P A)}{g : Tm Γ ((P ‼ k) ⇒ (R ᴬA) P B)} →
      coe (ap2 (M Γ A B) ᴬA⇛ ᴬA⇛) ((k ⇛ R) ᴹA) F f g ≡
        (∀ t → (R ᴹA) F (f $ t) (g $ t))

    ᴹ◆ : ∀ {Γ A B n P} {F : Tm Γ (A ⇒ B)}{t u : Tm Γ Unit} →
      coe (ap2 (M Γ A B) ᴬ◆ ᴬ◆) ((◆ {n} ᴹ){P}) F t u ≡ 𝟙'
    ᴹ▷ : ∀ {Γ A B n P} {Ω : Sign n}{R : Ar n}{F : Tm Γ (A ⇒ B)}
      {t : Tm Γ ((Ω ᴬ) P A × (R ᴬA) P A)}
      {u : Tm Γ ((Ω ᴬ) P B × (R ᴬA) P B)} →
      coe (ap2 (M Γ A B) ᴬ▷ ᴬ▷) ((Ω ▷ R) ᴹ) F t u ≡
        ((Ω ᴹ) F (proj₁ t) (proj₁ u) ×p (R ᴹA) F (proj₂ t) (proj₂ u))
    
    ass : ∀ {Γ Δ Θ Λ} {σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr : ∀ {Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ
    ∙η : ∀ {Γ} {σ : Sub Γ ∙} → σ ≡ ε

    ▹β₁ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ
    [id] : ∀ {Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘] : ∀ {Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]
    
    ⇒β : ∀ {Γ A B} {t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t

    uη : ∀ {Γ} {t : Tm Γ Unit} → t ≡ tt
    ×β₁ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ A B} {u : Tm Γ A}{v : Tm Γ B} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ A B} {t : Tm Γ (A × B)} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t

    +β₁ : ∀ {Γ A B C} {t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inl t) u v ≡ u [ id , t ]
    +β₂ : ∀ {Γ A B C} {t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
      case (inr t) u v ≡ v [ id , t ]

    indβ : ∀ {Γ A n P} (Ω : Sign n)(ω : Tm Γ ((Ω ᴬ) P A)) →
      (Ω ᴹ) (rec Ω ω) (con Ω P) ω

    lam[] : ∀ {Γ Δ A B} {t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

    tt[] : ∀ {Γ Δ} {σ : Sub Γ Δ} → tt [ σ ] ≡ tt
    ⟨,⟩[] : ∀ {Γ Δ A B} {u : Tm Δ A}{v : Tm Δ B}{σ : Sub Γ Δ} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩

    absurd[] : ∀ {Γ Δ A} {t : Tm Δ Empty}{σ : Sub Γ Δ} →
      (absurd {A = A} t) [ σ ] ≡ absurd (t [ σ ])
    inl[] : ∀ {Γ Δ A B} {t : Tm Δ A}{σ : Sub Γ Δ} →
      (inl {B = B} t) [ σ ] ≡ inl (t [ σ ])
    inr[] : ∀ {Γ Δ A B} {t : Tm Δ B}{σ : Sub Γ Δ} →
      (inr {A = A} t) [ σ ] ≡ inr (t [ σ ])
    case[] : ∀ {Γ Δ A B C} {t : Tm Δ (A + B)}
      {u : Tm (Δ ▹ A) C}{v : Tm (Δ ▹ B) C}{σ : Sub Γ Δ} →
      (case t u v) [ σ ] ≡
            case (t [ σ ]) (u [ σ ∘ p , q ]) (v [ σ ∘ p , q ])

    con[] : ∀ {Γ Δ n P} {Ω : Sign n}{σ : Sub Γ Δ} →
      (con Ω P) [ σ ] ≡ con Ω P
    rec[] : ∀ {Γ Δ A n P} {Ω : Sign n}{u : Tm Δ ((Ω ᴬ) P A)}{σ : Sub Γ Δ} →
      (rec Ω u) [ σ ] ≡ rec Ω (u [ σ ])

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  proj₁[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₁ t) [ σ ] ≡ proj₁ (t [ σ ])
  proj₁[] {σ = σ} = ×β₁ ⁻¹
                  ◾ ap proj₁ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₁ (u [ σ ])) ×η

  proj₂[] : ∀ {Γ Δ A B} {t : Tm Δ (A × B)}{σ : Sub Γ Δ} →
    (proj₂ t) [ σ ] ≡ proj₂ (t [ σ ])
  proj₂[] {σ = σ} = ×β₂ ⁻¹
                  ◾ ap proj₂ ⟨,⟩[] ⁻¹
                  ◾ ap (λ u → proj₂ (u [ σ ])) ×η

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η
  
  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦_⟧C : I.Con → Con
  ⟦_⟧A : ∀ {n} → I.Ar n → Ar n
  ⟦_⟧S : ∀ {n} → I.Sign n → Sign n
  ⟦_⟧P : ∀ {n} → 𝕍 I.Ty n → 𝕍 Ty n

  ⟦ I.Unit ⟧T = Unit
  ⟦ I.Empty ⟧T = Empty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T
  ⟦ A I.+ B ⟧T = ⟦ A ⟧T + ⟦ B ⟧T
  ⟦ I.ind Ω P ⟧T = ind ⟦ Ω ⟧S ⟦ P ⟧P

  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  ⟦ □ ⟧P = □
  ⟦ C ◁ P ⟧P = ⟦ C ⟧T ◁ ⟦ P ⟧P
  
  ⟦ I.X ⟧A = X
  ⟦ I.X⇒ R ⟧A = X⇒ ⟦ R ⟧A
  ⟦ k I.⇛ R ⟧A = k ⇛ ⟦ R ⟧A
  
  ⟦ I.◆ ⟧S = ◆
  ⟦ Ω I.▷ R ⟧S = ⟦ Ω ⟧S ▷ ⟦ R ⟧A

  postulate
    ⟦_⟧σ : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦ᴬA⟧ : ∀ {n P A} {R : I.Ar n} →
      ⟦ (R I.ᴬA) P A ⟧T ≡ (⟦ R ⟧A ᴬA) ⟦ P ⟧P ⟦ A ⟧T
    ⟦ᴬ⟧ : ∀ {n P A} {Ω : I.Sign n} →
      ⟦ (Ω I.ᴬ) P A ⟧T ≡ (⟦ Ω ⟧S ᴬ) ⟦ P ⟧P ⟦ A ⟧T
    {-# REWRITE ⟦ᴬA⟧ ⟦ᴬ⟧ #-}

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧σ ≡ ⟦ σ ⟧σ ∘ ⟦ δ ⟧σ
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧σ ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧σ ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧σ ≡ ⟦ σ ⟧σ , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧σ ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}
    
    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧σ ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}
    
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦tt⟧ ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}

    ⟦absurd⟧ : ∀ {Γ A} {t : I.Tm Γ I.Empty} →
      ⟦ I.absurd {A = A} t ⟧t ≡ absurd ⟦ t ⟧t
    ⟦inl⟧ : ∀ {Γ A B} {t : I.Tm Γ A} →
      ⟦ I.inl {B = B} t ⟧t ≡ inl ⟦ t ⟧t
    ⟦inr⟧ : ∀ {Γ A B} {t : I.Tm Γ B} →
      ⟦ I.inr {A = A} t ⟧t ≡ inr ⟦ t ⟧t
    ⟦case⟧ : ∀ {Γ A B C} {t : I.Tm Γ (A I.+ B)}
      {u : I.Tm (Γ I.▹ A) C}{v : I.Tm (Γ I.▹ B) C} →
      ⟦ I.case t u v ⟧t ≡ case ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦absurd⟧ ⟦inl⟧ ⟦inr⟧ ⟦case⟧ #-}

    ⟦con⟧ : ∀ {Γ n P} {Ω : I.Sign n} →
      ⟦ I.con {Γ} Ω P ⟧t ≡ con ⟦ Ω ⟧S ⟦ P ⟧P
    ⟦rec⟧ : ∀ {Γ n P A} {Ω : I.Sign n}
      {u : I.Tm Γ ((Ω I.ᴬ) P A)} →
      ⟦ I.rec Ω u ⟧t ≡ rec ⟦ Ω ⟧S ⟦ u ⟧t
    {-# REWRITE ⟦con⟧ ⟦rec⟧ #-}
  
  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}
