\section{Well-typed syntax with equations}

In the well-typed syntax, we still had too many terms in a sense: for
example, the terms \verb$I.true$ and
\verb$I.isZero (I.num 0)$ were different, even if they
should be equal in a sensible semantics. The information which terms
should be the same (usually this is called operational semantics) was
missing. Now we add this information in the form of equalities.

\begin{code}[hide]
{-# OPTIONS --prop --rewriting #-}

module NatBool where

open import Lib
module I where
  data Ty     : Set where
    Nat       : Ty
    Bool      : Ty
  
  postulate
    Tm        : Ty → Set
    true      : Tm Bool
    false     : Tm Bool
    ite       : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
    num       : ℕ → Tm Nat
    isZero    : Tm Nat → Tm Bool
    _+o_      : Tm Nat → Tm Nat → Tm Nat
    iteβ₁     : ∀{A}{u v : Tm A} → ite true u v ≡ u
    iteβ₂     : ∀{A}{u v : Tm A} → ite false u v ≡ v
    isZeroβ₁  : isZero (num 0) ≡ true
    isZeroβ₂  : isZero (num (1 + n)) ≡ false
    +β        : num m +o num n ≡ num (m + n)

variable
  Aᴵ : I.Ty
  tᴵ uᴵ vᴵ : I.Tm Aᴵ
\end{code}

A NatBool model (without any qualifiers) consists of the following
components. The only difference from NatBoolWT is the addition of the
five equations.
\begin{code}
record Model {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty        : Set i
    Tm        : Ty → Set j
    Nat       : Ty
    Bool      : Ty
    true      : Tm Bool
    false     : Tm Bool
    ite       : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
    num       : ℕ → Tm Nat
    isZero    : Tm Nat → Tm Bool
    _+o_      : Tm Nat → Tm Nat → Tm Nat
    iteβ₁     : ∀{A}{u v : Tm A} → ite true u v ≡ u
    iteβ₂     : ∀{A}{u v : Tm A} → ite false u v ≡ v
    isZeroβ₁  : isZero (num 0) ≡ true
    isZeroβ₂  : isZero (num (1 + n)) ≡ false
    +β        : num m +o num n ≡ num (m + n)
\end{code}
The recursor is given as before.
\begin{code}
  ⟦_⟧T         : I.Ty → Ty
  ⟦ I.Nat ⟧T   = Nat
  ⟦ I.Bool ⟧T  = Bool

  postulate
    ⟦_⟧t       : I.Tm Aᴵ → Tm ⟦ Aᴵ ⟧T
    ⟦true⟧     : ⟦ I.true          ⟧t ≡ true
    ⟦false⟧    : ⟦ I.false         ⟧t ≡ false
    ⟦ite⟧      : ⟦ I.ite tᴵ uᴵ vᴵ  ⟧t ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦num⟧      : ⟦ I.num n         ⟧t ≡ num n
    ⟦isZero⟧   : ⟦ I.isZero tᴵ     ⟧t ≡ isZero ⟦ tᴵ ⟧t
    ⟦+o⟧       : ⟦ uᴵ I.+o vᴵ      ⟧t ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
\end{code}
\begin{code}[hide]
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ ⟦num⟧ ⟦isZero⟧ ⟦+o⟧ #-}

I : Model
I = record { Ty = I.Ty ; Tm = I.Tm ; Nat = I.Nat ; Bool = I.Bool ; true = I.true ; false = I.false; ite = I.ite ; num = I.num ; isZero = I.isZero ; _+o_ = I._+o_ ; iteβ₁ = I.iteβ₁ ; iteβ₂ = I.iteβ₂ ; isZeroβ₁ = I.isZeroβ₁ ; isZeroβ₂ = I.isZeroβ₂ ; +β = I.+β }
\end{code}
We also have dependent models and an eliminator.
\begin{code}
record DepModel {i j} : Set (lsuc (i ⊔ j)) where
  field
    Ty        : I.Ty → Set i
    Tm        : Ty Aᴵ → I.Tm Aᴵ → Set j
    Nat       : Ty I.Nat
    Bool      : Ty I.Bool
    true      : Tm Bool I.true
    false     : Tm Bool I.false
    ite       : {A : Ty Aᴵ} →
                Tm Bool tᴵ → Tm A uᴵ → Tm A vᴵ → Tm A (I.ite tᴵ uᴵ vᴵ)
    num       : (n : ℕ) → Tm Nat (I.num n)
    isZero    : Tm Nat tᴵ → Tm Bool (I.isZero tᴵ)
    _+o_      : Tm Nat uᴵ → Tm Nat vᴵ → Tm Nat (uᴵ I.+o vᴵ)
    iteβ₁     : {A : Ty Aᴵ}{u : Tm A uᴵ}{v : Tm A vᴵ} →
                ((Tm A) ~) I.iteβ₁ (ite true u v) u
    iteβ₂     : {A : Ty Aᴵ}{u : Tm A uᴵ}{v : Tm A vᴵ} →
                ((Tm A) ~) I.iteβ₂ (ite false u v) v
    isZeroβ₁  : ((Tm Bool) ~) I.isZeroβ₁ (isZero (num 0)) true
    isZeroβ₂  : ((Tm Bool) ~) I.isZeroβ₂ (isZero (num (1 + n))) false
    +β        : ((Tm Nat) ~) I.+β (num m +o num n) (num (m + n))
                
  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  postulate
    ⟦_⟧t       : (tᴵ : I.Tm Aᴵ) → Tm ⟦ Aᴵ ⟧T tᴵ
    ⟦true⟧     : ⟦ I.true          ⟧t ≡ true
    ⟦false⟧    : ⟦ I.false         ⟧t ≡ false
    ⟦ite⟧      : ⟦ I.ite tᴵ uᴵ vᴵ  ⟧t ≡ ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦num⟧      : ⟦ I.num n         ⟧t ≡ num n
    ⟦isZero⟧   : ⟦ I.isZero tᴵ     ⟧t ≡ isZero ⟦ tᴵ ⟧t
    ⟦+o⟧       : ⟦ uᴵ I.+o vᴵ      ⟧t ≡ ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t
\end{code}
\begin{code}[hide]
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ ⟦num⟧ ⟦isZero⟧ ⟦+o⟧ #-}
\end{code}

Now we can reproduce the evaluation of the example program from the
beginning of Chapter \ref{ch:NatBool} as an equality between terms.
\begin{code}[hide]
module example where
  open I

  ex : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  ex =
\end{code}
\begin{code}
    ite (isZero (num 0 +o num 1))  false (isZero (num 0))
      ≡⟨ cong (λ x → ite (isZero x) false (isZero (num 0))) +β ⟩
    ite (isZero (num 1))           false (isZero (num 0))
      ≡⟨ cong (λ x → ite x false (isZero (num 0))) isZeroβ₂ ⟩
    ite false                      false (isZero (num 0))
      ≡⟨ iteβ₂ ⟩
    isZero (num 0)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎
\end{code}
Two terms which result in the same boolean or number are equal.

The type inference algorithm given for NatBoolWT also works for
NatBool. We can't calculate the height of a term anymore as before
because of the equalities. Every function has to preserve the
equalities. For example, we need that \verb$height (isZero (num 0)) = height true$.
If we view these terms as syntax trees, their height is 1 and 0, respectively.

\subsection{Standard model, trivial model}
\label{sec:natbool-standard}

The term \verb$ite (isZero (num 0 +o num 1)) false (isZero (num 0))$ was built using the operations of our language. Every model supports these operations, hence the term can be built in any model. The equality \verb$ite (isZero (num 0 +o num 1)) false (isZero (num 0)) = true$ was built using the equations in our language which hold in every model. Hence the equality also holds in every model. Such a term or equation is called \emph{derivable}. A derivable term can be constructed in any model, a derivable equation holds in every model. In contrast there are properties that we can only prove using
induction: these hold in the syntax, but might not hold for every model. For example, the fact that for any \verb$t : Tm Bool$ we have \verb$t = true$ or \verb$t = false$ holds in the syntax but not in an arbitrary model (this is a consequence of normalisation, see below). Such properties are called \emph{admissible}.

\begin{exe}[recommended]
  Construct a model in which it does not hold that for a \verb$t : Tm Bool$ we have \verb$t = true$ or \verb$t = false$.
\end{exe}

Another property of the syntax is that \verb$true ≠ false$. We can show this using the standard model.

The operators in the standard model are defined exactly as for NatBoolWT, but we also have to
prove the five equalities \verb$iteβ₁$, \dots, \verb$+β$: they all hold by definition.
\begin{code}
St : Model
St = record
  { Ty        = Set
  ; Tm        = λ A → A
  ; Nat       = ℕ
  ; Bool      = 𝟚
  ; true      = tt
  ; false     = ff
  ; ite       = if_then_else_
  ; num       = λ n → n
  ; isZero    = rec tt (λ _ → ff)
  ; _+o_      = _+_
  ; iteβ₁     = λ {A}{u}{v} → refl {x = u}
  ; iteβ₂     = λ {A}{u}{v} → refl {x = v}
  ; isZeroβ₁  = refl {x = tt}
  ; isZeroβ₂  = refl {x = ff}
  ; +β        = λ {m}{n} → refl {x = m + n}
  }
module St = Model St
\end{code}
From the standard model we obtain \emph{equational consistency} of our
language, that is, there are terms which are not equal. This means
that we didn't add too many equalities when defining the language.
In the syntax, if \verb$true$ was equal to \verb$false$, then their standard
interpretations would also be equal, but then \verb$tt$ would be equal
to \verb$ff$.
\begin{code}[hide]
module EquationalConsistency where
\end{code}
\begin{code}
  true≠false : ¬ (I.true ≡ I.false)
  true≠false = cong St.⟦_⟧t
\end{code}

If we are not careful when defining the equations of the language, it is easy to lose equational consistency.
\begin{exe}[compulsory]
Show that if \verb$true$ = \verb$false$ in a model, then any two \verb$u, v : Tm Nat$ are equal in that model.
\end{exe}

As for any language, we can define a trivial model.
\begin{code}
Triv : Model
Triv = record
  { Ty        = Lift ⊤
  ; Tm        = λ _ → Lift ⊤
  ; Nat       = mk trivi
  ; Bool      = mk trivi
  ; true      = mk trivi
  ; false     = _
  ; ite       = _
  ; num       = _
  ; isZero    = _
  ; _+o_      = _
  ; isZeroβ₁  = _
  ; isZeroβ₂  = _
  ; +β        = _
  ; iteβ₁     = _
  ; iteβ₂     = _
  }
module Triv = Model Triv
\end{code}
In the trivial model, any two terms are equal, hence all the equalities that can be described in the language, hold. For example, in this model we not only have
\verb$true = false$, but also \verb$Nat = Bool$ and \verb$true = num 5$ (this equality only makes sense because we know that \verb$Nat = Bool$). We also have that for any \verb$t : Tm Bool$, \verb$t = true$ or \verb$t = false$, trivially.

\subsection{Normalisation}
\label{sec:natbool-norm}

We call \verb$St.⟦ Aᴵ ⟧T$ the set of \emph{normal forms} of type
\verb$Aᴵ$. For \verb$I.Tm I.Nat$, normal forms are metatheoretic natural numbers, for \verb$I.Tm I.Bool$, normal forms are metatheoretic booleans.  The interpretation of terms in \verb$St$ computes the normal form of a term:
\begin{code}
norm : I.Tm Aᴵ → St.⟦ Aᴵ ⟧T
norm = St.⟦_⟧t 
\end{code}
The function \verb$norm$ which returns a normal form of a term is called \emph{normalisation}.

Normal forms can be quoted back to terms:
\begin{code}
⌜_⌝N : ℕ → I.Tm I.Nat
⌜_⌝N = I.num

⌜_⌝B : 𝟚 → I.Tm I.Bool
⌜ tt ⌝B = I.true
⌜ ff ⌝B = I.false

⌜_⌝ : ∀ {A} → St.⟦ A ⟧T → I.Tm A
⌜_⌝ {I.Nat} = ⌜_⌝N
⌜_⌝ {I.Bool} = ⌜_⌝B
\end{code}

\emph{Completeness of normalisation} says that every term is equal to
its normal form followed by quote. In our case, this means that every
boolean term (element of \verb$I.Tm I.Bool$) is either true or false
and every natural number term is a finite application of \verb$I.suc$s
on \verb$I.zero$.

\begin{code}
⌜isZero⌝ : ∀{n} → ⌜ rec tt (λ _ → ff) n ⌝ ≡ I.isZero ⌜ n ⌝
⌜isZero⌝ {zero}   = sym {A = I.Tm I.Bool} I.isZeroβ₁
⌜isZero⌝ {suc n}  = sym {A = I.Tm I.Bool} I.isZeroβ₂

⌜+⌝ : ∀{m n} → ⌜ m + n ⌝N ≡ ⌜ m ⌝N I.+o ⌜ n ⌝N
⌜+⌝ {m}{n} = sym {A = I.Tm I.Nat} I.+β

⌜ite⌝ : ∀{b A}{u v : St.⟦ A ⟧T} → ⌜_⌝ {A} (if b then u else v) ≡ I.ite ⌜ b ⌝ ⌜ u ⌝ ⌜ v ⌝
⌜ite⌝ {tt}{A} = sym {A = I.Tm A} I.iteβ₁
⌜ite⌝ {ff}{A} = sym {A = I.Tm A} I.iteβ₂

Comp : DepModel
Comp = record
  { Ty        = λ _ → Lift ⊤
  ; Tm        = λ _ t → Lift (⌜ St.⟦ t ⟧t ⌝ ≡ t)
  ; Nat       = _
  ; Bool      = _
  ; num       = λ n → mk (refl {x = I.num n})
  ; isZero    = λ {tᴵ} t →  mk (trans {A = I.Tm I.Bool} ⌜isZero⌝ (cong I.isZero (un t)))
  ; _+o_      = λ {uᴵ}{vᴵ} u v → mk (trans {A = I.Tm I.Nat}
                (⌜+⌝ {St.⟦ uᴵ ⟧t}{St.⟦ vᴵ ⟧t})
                (cong {A = I.Tm I.Nat × I.Tm I.Nat} (λ w → π₁ w I.+o π₂ w) (un u , un v)))
  ; true      = mk (refl {x = I.true})
  ; false     = mk (refl {x = I.false})
  ; ite       = λ {Aᴵ}{tᴵ}{uᴵ}{vᴵ}{A} t u v → mk (trans {A = I.Tm Aᴵ}
                (⌜ite⌝ {St.⟦ tᴵ ⟧t}{Aᴵ}{St.⟦ uᴵ ⟧t}{St.⟦ vᴵ ⟧t})
                (cong {A = I.Tm I.Bool × I.Tm Aᴵ × I.Tm Aᴵ}
                  (λ w → I.ite (π₁ w) (π₁ (π₂ w)) (π₂ (π₂ w)))
                  (un t , un u , un v)))
  ; isZeroβ₁  = mk trivi
  ; isZeroβ₂  = mk trivi
  ; +β        = mk trivi
  ; iteβ₁     = mk trivi
  ; iteβ₂     = mk trivi
  }
module Comp = DepModel Comp

comp : ⌜ norm tᴵ ⌝ ≡ tᴵ
comp {tᴵ = tᴵ} = un (Comp.⟦ tᴵ ⟧t)
\end{code}

\emph{Stability of normalisation} means that there is no junk in normal
forms, for every normal form there is a term which normalises to it.
\begin{code}
stab : {Aᴵ : I.Ty}{t : St.⟦ Aᴵ ⟧T} → norm {Aᴵ} ⌜ t ⌝ ≡ t
stab {I.Nat}{t} = refl {x = t}
stab {I.Bool} {tt} = trivi
stab {I.Bool} {ff} = trivi
\end{code}

\begin{exe}
  Define normalisation and prove its completeness using a modified standard model where \verb$Bool = Maybe 𝟚$. Show that stability does not hold.
\end{exe}

\begin{exe}[recommended]
  Show that in the syntax we have \verb$ite t u u = u$ for any \verb$t$, \verb$u$, and construct a model in which this doesn't hold.
\end{exe}

\begin{code}[hide]
module thisExe where
  A : Model
  A = record
    { Ty = Set
    ; Tm = λ A → Maybe A
    ; Nat = ℕ
    ; Bool = 𝟚
    ; true = Just tt
    ; false = Just ff
    ; ite = λ { (Just tt) u v → u ; (Just ff) u v → v ; _ _ _ → Nothing }
    ; num = λ n → Just n
    ; isZero = λ { (Just zero) → Just tt ; (Just _) → Just ff ; Nothing → Nothing }
    ; _+o_ = λ { (Just m) (Just n) → Just (m + n) ; _ _ → Nothing }
    ; iteβ₁ = λ {_}{u}{v} → refl {x = u}
    ; iteβ₂ = λ {_}{u}{v} → refl {x = v}
    ; isZeroβ₁ = trivi
    ; isZeroβ₂ = trivi
    ; +β = λ {m}{n} → refl {x = m + n}
    }
  open Model A
  t : Tm Bool
  t = Nothing
  u : Tm Nat
  u = Just 3
  itetuu≠u : ¬ (ite t u u ≡ u)
  itetuu≠u = un
\end{code}

We used all the equations of NatBool in the completeness proof. If we
take out e.g.\ the rule \verb$isZeroβ₂$ from the definition of the
language, we cannot prove completeness anymore because we have
elements of \verb$I.Tm I.Bool$ which are neither \verb$I.true$, nor
\verb$I.false$. The following (NatBool without \verb$isZeroβ₂$) model distinguishes \verb$true$,
\verb$false$ and \verb$isZero (num 1)$.
\begin{code}
module NatBoolIncomplete where
  Ty        : Set₁
  Tm        : Ty → Set
  Nat       : Ty
  Bool      : Ty
  num       : ℕ → Tm Nat
  isZero    : Tm Nat → Tm Bool
  _+o_      : Tm Nat → Tm Nat → Tm Nat
  true      : Tm Bool
  false     : Tm Bool
  ite       : ∀{A} → Tm Bool → Tm A → Tm A → Tm A
  isZeroβ₁  : isZero (num 0)     ≡ true
  +β        : ∀{m n} → num m +o num n ≡ num (m + n)
  iteβ₁     : ∀{A}{u v : Tm A} → ite true   u v ≡ u
  iteβ₂     : ∀{A}{u v : Tm A} → ite false  u v ≡ v

  Ty                 = Set
  Tm A               = A
  Nat                = ℕ
  Bool               = Maybe 𝟚
  num                = λ n → n
  isZero zero        = Just tt
  isZero (suc _)     = Nothing
  _+o_               = _+_
  true               = Just tt
  false              = Just ff
  ite Nothing u v    = u
  ite (Just tt) u v  = u
  ite (Just ff) u v  = v
  isZeroβ₁           = refl {x = Just tt}
  +β {m}{n}          = refl {x = m + n}
  iteβ₁ {A}{u}{v}    = refl {x = u}
  iteβ₂ {A}{u}{v}    = refl {x = v}
  
  true≠false     : ¬ (true   ≡ false)
  true≠isZero1   : ¬ (true   ≡ isZero (num 1))
  false≠isZero1  : ¬ (false  ≡ isZero (num 1))
\end{code}
\begin{code}[hide]
  true≠false = λ b → b
  true≠isZero1 = un
  false≠isZero1 = un
\end{code}

\begin{exe}[recommended]
Define a height function on terms which returns the height of the normal form of the term.
\end{exe}

A model of a monoid over \verb$A$ has the following components.
\begin{alignat*}{10}
& \texttt{C} && \texttt{: Set} \\
& \texttt{f} && \texttt{: A → C} \\
& \texttt{\_∘\_} && \texttt{: C → C → C} \\
& \texttt{id} && \texttt{: C} \\
& \texttt{ass} && \texttt{: (a ∘ b) ∘ c ≡ a ∘ (b ∘ c)} \\
& \texttt{idl} && \texttt{: id ∘ a ≡ a} \\
& \texttt{idr} && \texttt{: a ∘ id ≡ a}
\end{alignat*}
The initial model is called the free monoid over \verb$A$.
\begin{exe}[recommended]
  Define normalisation and prove its completeness and stability for the free monoid over a set \verb$A$. Normal forms are simply lists of \verb$A$.
\end{exe}

\subsection{Transition relation}

The traditional way of defining equality of syntactic terms is using
transition relations on syntactic NatBoolAST terms (see e.g.\ \cite{harper}).
\begin{code}
import NatBoolAST
module I' = NatBoolAST.I
data _↦_ : I'.Tm → I'.Tm → Prop where
  isZeroβ₁     :           I'.isZero (I'.num 0) ↦ I'.true
  isZeroβ₂     :           I'.isZero (I'.num (1 + n)) ↦ I'.false
  +β           :           (I'.num m I'.+o I'.num n) ↦ I'.num (m + n)
  iteβ₁        : ∀{u v} →  I'.ite I'.true   u v ↦ u
  iteβ₂        : ∀{u v} →  I'.ite I'.false  u v ↦ v

  isZero-cong  : ∀{t t'}      → t  ↦ t'  → I'.isZero t     ↦ I'.isZero t'
  +-cong₁      : ∀{u v u'}    → u  ↦ u'  → (u I'.+o v)     ↦ (u' I'.+o v)
  +-cong₂      : ∀{u v v'}    → v  ↦ v'  → (u I'.+o v)     ↦ (v  I'.+o v')
  ite-cong     : ∀{t t' u v}  → t  ↦ t'  → (I'.ite t u v)  ↦ (I'.ite t' u v)
\end{code}
The idea is that if there is a \verb$p : t ↦ t'$, this means that the program
\verb$t$ evaluates to \verb$t'$ in one step. Running a program means that we have a
sequence of evaluation steps: \verb$t ↦ t' ↦ t'' ↦ ⋯$ 

There is a rewriting rule corresponding to each equation in the notion
of NatBool-model. Then there are congruence rules (order rules) which
express which parameters and when should be evaluated. In our example
rewriting system above rewriting can happen anywhere except for
\verb$I'.ite$ it only happens in the first parameter.

Taking the reflexive transitive closure of \verb$↦$ we obtain the
multi-step rewriting relation \verb$↦*$, taking the symmetric closure
of \verb$↦*$ we get the conversion relation (definitial equality)
\verb$∼$ which in the case of NatBool, corresponds to equality of
syntactic terms.
\begin{exe}
  Prove that \verb$t ≡ t'$ is logically equivalent to \verb$t ~ t'$ for all \verb$t$ and \verb$t'$.
\end{exe}

For other languages this is not necessarily the case. In general the
transition system contains more information than the description with
equations. Transition is directed and is not necessarily a
congruence. We will only present languages where there is no such
additional information.
