\chapter{System F}\label{ch:systemf}

Church encodings.

\begin{verbatim}
Ty  : Set*                                           Ty : Con*_{PSh(C)}
Tm  : Ty → Set*                                      Tm : Ty*_{PSh(C)} Ty
_⇒_ : Ty → Ty → Ty                                   ⇒  : Tm_{PSh(C)}
∀   : (Ty →* Ty) → Ty
lam : (Tm A →* Tm B) ≅ Tm (A ⇒ B) : app
Lam : (Π* Ty λ* A . Tm (B * A)) ≅ Tm (∀ B) : App
Lam : ((A : Ty) →* Tm (B * A)) ≅ Tm (∀ B) : App      (Π* Ty (Tm[app* B])) ≅
\end{verbatim}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting --guardedness #-}
open import Lib

module SystemF where

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]T
  infixl 5 _▷
  infixl 5 _ʻ_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 5 _$$_

  field
    Con           : Set i
    Sub           : Con → Con → Set j
    _⊚_           : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass           : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id            : ∀{Γ} → Sub Γ Γ
    idl           : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr           : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
    
    ∙              : Con
    ε             : ∀{Γ} → Sub Γ ∙
    ∙η            : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε
    
    Ty            : Con → Set k
    _[_]T         : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T          : ∀{Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ⊚ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T         : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    _▷            : Con → Con
    _ʻ_           : ∀{Γ Δ} → Sub Δ Γ → Ty Δ → Sub Δ (Γ ▷)
    P             : ∀{Γ} → Sub (Γ ▷) Γ
    Q             : ∀{Γ} → Ty (Γ ▷)
    ▷β₁           : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Δ} → P ⊚ (γ ʻ A) ≡ γ
    ▷β₂           : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Δ} → Q [ γ ʻ A ]T ≡ A
    ▷η            : ∀{Γ Δ}{γA : Sub Δ (Γ ▷)} → P ⊚ γA ʻ Q [ γA ]T ≡ γA
    
    Tm            : (Γ : Con) → Ty Γ → Set l
    _[_]          : ∀{Γ Δ A} → Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
    [∘]           : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} → (Tm Θ ~) [∘]T (t [ γ ⊚ δ ]) (t [ γ ] [ δ ])
    [id]          : ∀{Γ A}{t : Tm Γ A} → (Tm Γ ~) [id]T (t [ id ]) t
    _▹_           : (Γ : Con) → Ty Γ → Con
    _,o_          : ∀{Γ Δ A} → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
    p             : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q             : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ (A [ γ ]T)} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ (A [ γ ]T)} → (Tm Δ ~) (trans {A = Ty _} (sym {A = Ty _} [∘]T) (cong (A [_]T) ▹β₁)) (q [ γ ,o t ]) t
    ▹η            : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o coe (Tm Δ) (sym {A = Ty _} [∘]T) (q [ γa ]) ≡ γa

    _⇒_           : ∀{Γ} → Ty Γ → Ty Γ → Ty Γ
    lam           : ∀{Γ A B} → Tm (Γ ▹ A) (B [ p ]T) → Tm Γ (A ⇒ B)
    _$_           : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β            : ∀{Γ A B}{t : Tm (Γ ▹ A) (B [ p ]T)}{u : Tm Γ A} → (Tm Γ ~) (B ≡⟨ sym {A = Ty _} [id]T ⟩ B [ id ]T ≡⟨ cong (B [_]T) (sym {A = Sub _ _} ▹β₁) ⟩ B [ p ⊚ (id ,o coe (Tm Γ) _ u) ]T ≡⟨ [∘]T ⟩ B [ p ]T [ id ,o coe (Tm Γ) _ u ]T ∎)
                    (lam t $ u)
                    (t [ id ,o coe (Tm Γ) (sym {A = Ty _} [id]T) u ])
{-
    ⇒η            : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    ⇒[]           : ∀{Γ}{A B : Ty Γ}{Δ}{γ : Sub Δ Γ} → (A ⇒ B) [ γ ]T ≡ (A [ γ ]T) ⇒ (B [ γ ]T)
    lam[]         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                    (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]           : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                    (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]
-}

    ∀o            : ∀{Γ} → Ty (Γ ▷) → Ty Γ
    ∀[]           : ∀{Γ}{A : Ty (Γ ▷)}{Δ}{γ : Sub Δ Γ} → (∀o A) [ γ ]T ≡ ∀o (A [ γ ⊚ P ʻ Q ]T)
    Lam           : ∀{Γ A} → Tm (Γ ▷) A → Tm Γ (∀o A)
    _$$_          : ∀{Γ A} → Tm Γ (∀o A) → (B : Ty Γ) → Tm Γ (A [ id ʻ B ]T)
    ∀β            : ∀{Γ A}{a : Tm (Γ ▷) A}{B : Ty Γ} → Lam a $$ B ≡ a [ id ʻ B ]
    ∀η            : ∀{Γ A}{t : Tm Γ (∀o A)} → (Tm Γ ~) (cong ∀o (A [ P ⊚ P ʻ Q ]T [ id ʻ Q ]T ≡⟨ sym {A = Ty _} [∘]T ⟩ A [ (P ⊚ P ʻ Q) ⊚ (id ʻ Q) ]T ≡⟨ cong (A [_]T) {!!} ⟩ A [ id ]T ≡⟨ [id]T ⟩ A ∎))
                    (Lam (coe (Tm (Γ ▷)) ∀[] (t [ P ]) $$ Q))
                    t

\end{code}

\begin{code}
    
\end{code}
