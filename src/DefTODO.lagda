\begin{code}
{-# OPTIONS --prop --rewriting #-}
module DefNorm where

open import Lib
open import Def

Nf : I.Ty → Set
Nf I.Nat = ℕ
Nf I.Bool = 𝟚

⌜_⌝ : ∀{A} → Nf A → I.Tm I.∙ A
⌜_⌝ {I.Nat}  n   = I.num n
⌜_⌝ {I.Bool} ff  = I.false
⌜_⌝ {I.Bool} tt  = I.true

dec=Nf : ∀{A}(n₀ n₁ : Nf A) → Decp (n₀ ≡ n₁)
dec=Nf {I.Nat}   n₀ n₁ = dec=ℕ n₀ n₁
dec=Nf {I.Bool}  b₀ b₁ = dec=𝟚 b₀ b₁

{-
  numNf   : ℕ → Nf I.Nat
  trueNf  : Nf I.Bool
  falseNf : Nf I.Bool

_≡Nf_ : ∀{A} → Nf A → Nf A → Prop
numNf n₀  ≡Nf numNf n₁  = n₀ ≡ n₁
trueNf    ≡Nf trueNf    = ⊤
trueNf    ≡Nf falseNf   = ⊥
falseNf   ≡Nf trueNf    = ⊥
falseNf   ≡Nf falseNf   = ⊤

postulate
  Nf≡ : ∀{A} → _≡_ {A = Nf A} ≡ _≡Nf_
  {-# REWRITE Nf≡ #-}

⌜_⌝ : ∀{A} → Nf A → I.Tm I.∙ A
⌜ numNf n  ⌝ = I.num n
⌜ trueNf   ⌝ = I.true
⌜ falseNf  ⌝ = I.false

dec=Nf : ∀{A}(n₀ n₁ : Nf A) → Decp (n₀ ≡ n₁)
dec=Nf (numNf n₀) (numNf n₁) = dec=ℕ n₀ n₁
dec=Nf trueNf     trueNf      = ι₁ (mk trivi)
dec=Nf trueNf     falseNf     = ι₂ λ b → b
dec=Nf falseNf    trueNf      = ι₂ λ b → b
dec=Nf falseNf    falseNf     = ι₁ (mk trivi)
-}

open I

Norm : DepModel
Norm = record
  { Con       = λ Γᴵ → Sub ∙ Γᴵ → Set
  ; Sub       = λ {Δᴵ}{Γᴵ} Δ Γ γᴵ → {δᴵ : Sub ∙ Δᴵ} → Δ δᴵ → Γ (γᴵ ⊚ δᴵ)
  ; _⊚_       = λ {Δᴵ}{Γᴵ}{γᴵ}{Θᴵ}{δᴵ}{Γ}{Δ}{Θ} γ δ {θᴵ} θ* → coe Γ (sym {A = Sub ∙ Γᴵ} ass) (γ (δ θ*))
  ; ass       = {!!}
  ; id        = λ {Γᴵ}{Γ}{γᴵ} → coe Γ (sym {A = Sub ∙ Γᴵ} idl)
  ; idl       = λ {Δᴵ}{Γᴵ}{γᴵ}{Γ}{Δ}{γ} δᴵ₀ δᴵ₁ δᴵ₂ δ*₀ δ*₁ δ*₂ →
                  {!cong {A = Σ (Sub Δᴵ Γᴵ × Sub ∙ Δᴵ) λ x → Δ (π₂ x)} (λ x → γ {π₂ (π₁ x)} (π₂ x)) {(id ⊚ γᴵ , δᴵ₀) , δ*₀} {(γᴵ , δᴵ₁) , δ*₁} ((idl , δᴵ₂) , δ*₂)!}
                  -- cong {A = Σ (Sub Δᴵ Γᴵ × Sub ∙ Δᴵ) λ x → Δ (π₂ x)} (λ x → γ {π₂ (π₁ x)} (π₂ x)) {(id ⊚ γᴵ , δᴵ₀) , δ*₀} {(γᴵ , δᴵ₁) , δ*₁} ((idl , δᴵ₂) , δ*₂)
                  -- f:A→B,  ((λ x → C (f x)) ~) {a₀}{a₁} a₂ (coe C b₂ c₀) c₁
                  --                                         ^:C(f a₀)     ^:C(f a₁)
                  --                                         c₀ : C b₀, b₂:b₀≡f a₀
                  -- f:A→B,  ((λ x → C (f x)) ~) {a₀}{a₁} a₂ = C ~ {f a₀}{f a₁} (cong f a₂) 
  ; idr       = {!!}
  ; ∙         = λ _ → Lift ⊤
  ; ε         = λ _ → mk trivi
  ; ∙η        = λ _ _ → mk trivi
  ; Ty        = λ Aᴵ → Tm ∙ Aᴵ → Set
  ; Tm        = λ {Γᴵ}{Aᴵ} Γ A aᴵ → {γᴵ : Sub ∙ Γᴵ} → Γ γᴵ → A (aᴵ [ γᴵ ])
  ; _[_]      = λ {Γᴵ}{Aᴵ}{aᴵ}{Δᴵ}{γᴵ}{Γ}{Δ}{A} a γ {δᴵ} δ → coe A [∘] (a {γᴵ ⊚ δᴵ} (γ δ)) 
  ; [∘]       = {!!}
  ; [id]      = {!!}
  ; _▹_       = λ Γ A γaᴵ → Γ (p ⊚ γaᴵ) × A (q [ γaᴵ ])
  ; _,o_      = λ {Δᴵ}{Γᴵ}{γᴵ}{Aᴵ}{aᴵ}{Γ}{Δ}{A} γ a {δᴵ} δ* →
                coe Γ (γᴵ ⊚ δᴵ   ≡⟨ cong {A = Sub _ _} (_⊚ δᴵ)   (sym {A = Sub _ _} ▹β₁) ⟩ (p ⊚ (γᴵ ,o aᴵ)) ⊚ δᴵ   ≡⟨ ass                  ⟩ p ⊚ ((γᴵ ,o aᴵ) ⊚ δᴵ) ∎) (γ δ*) ,
                coe A (aᴵ [ δᴵ ] ≡⟨ cong {A = Tm  _ _} (_[ δᴵ ]) (sym {A = Tm  _ _} ▹β₂) ⟩ q [ (γᴵ ,o aᴵ) ] [ δᴵ ] ≡⟨ sym {A = Tm _ _} [∘] ⟩ q [ (γᴵ ,o aᴵ) ⊚ δᴵ ] ∎) (a δ*)
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = {!!}
  ; ▹β₂       = {!!}
  ; ▹η        = {!!}
  ; Bool      = λ tᴵ → Σ (Nf Bool) λ n → Lift (⌜ n ⌝ ≡ tᴵ)
  ; true      = λ _ → tt , mk (sym {A = Tm _ _} true[])
  ; false     = λ _ → ff , mk (sym {A = Tm _ _} false[])
  ; ite       = λ {Γᴵ}{tᴵ}{Aᴵ}{uᴵ}{vᴵ}{Γ}{A} t u v {γᴵ} γ* → ind𝟚
                  (λ b → ⌜ b ⌝ ≡ tᴵ [ γᴵ ] → A (ite tᴵ uᴵ vᴵ [ γᴵ ]))
                  (λ e → coe A (uᴵ [ γᴵ ] ≡⟨ sym {A = Tm _ _} iteβ₁ ⟩ ite true  (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ]) ≡⟨ cong {A = Tm _ _} (λ x → ite x (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ])) e ⟩ ite (tᴵ [ γᴵ ]) (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ])  ≡⟨ sym {A = Tm _ _} ite[] ⟩ ite tᴵ uᴵ vᴵ [ γᴵ ] ∎) (u γ*))
                  (λ e → coe A (vᴵ [ γᴵ ] ≡⟨ sym {A = Tm _ _} iteβ₂ ⟩ ite false (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ]) ≡⟨ cong {A = Tm _ _} (λ x → ite x (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ])) e ⟩ ite (tᴵ [ γᴵ ]) (uᴵ [ γᴵ ]) (vᴵ [ γᴵ ])  ≡⟨ sym {A = Tm _ _} ite[] ⟩ ite tᴵ uᴵ vᴵ [ γᴵ ] ∎) (v γ*))
                  (π₁ (t γ*)) (un (π₂ (t γ*)))
  ; iteβ₁     = {!!}
  ; iteβ₂     = {!!}
  ; true[]    = {!!}
  ; false[]   = {!!}
  ; ite[]     = {!!}
  ; Nat       = λ tᴵ → Σ (Nf Nat) λ n → Lift (⌜ n ⌝ ≡ tᴵ)
  ; num       = λ n _ → n , mk (sym {A = Tm _ _} num[])
  ; isZero    = λ {Γᴵ}{tᴵ}{Γ} t {γᴵ} γ* → indℕ
                  (λ n → num n ≡ tᴵ [ γᴵ ] → Σ 𝟚 (λ n → Lift (⌜ n ⌝ ≡ isZero tᴵ [ γᴵ ])))
                  (λ e     → tt , mk (true  ≡⟨ sym {A = Tm _ _} isZeroβ₁ ⟩ isZero (num 0)       ≡⟨ cong isZero e ⟩ isZero (tᴵ [ γᴵ ]) ≡⟨ sym {A = Tm _ _} isZero[] ⟩ isZero tᴵ [ γᴵ ] ∎))
                  (λ n _ e → ff , mk (false ≡⟨ sym {A = Tm _ _} isZeroβ₂ ⟩ isZero (num (suc n)) ≡⟨ cong isZero e ⟩ isZero (tᴵ [ γᴵ ]) ≡⟨ sym {A = Tm _ _} isZero[] ⟩ isZero tᴵ [ γᴵ ] ∎))
                  (π₁ (t γ*)) (un (π₂ (t γ*)))
  ; _+o_      = λ {Γᴵ}{uᴵ}{vᴵ}{Γ} u v {γᴵ} γ* → π₁ (u γ*) + π₁ (v γ*) , mk (num (π₁ (u γ*) + π₁ (v γ*)) ≡⟨ sym {A = Tm _ _} +β ⟩ num (π₁ (u γ*)) +o num (π₁ (v γ*)) ≡⟨ cong {A = Tm _ _ × Tm _ _} (λ w → π₁ w +o π₂ w) (un (π₂ (u γ*)) , un (π₂ (v γ*))) ⟩ (uᴵ [ γᴵ ]) +o (vᴵ [ γᴵ ]) ≡⟨ sym {A = Tm _ _} +[] ⟩ (uᴵ +o vᴵ) [ γᴵ ] ∎)
  ; isZeroβ₁  = {!!}
  ; isZeroβ₂  = {!!}
  ; +β        = {!!}
  ; num[]     = {!!}
  ; isZero[]  = {!!}
  ; +[]       = {!!}
  }
\end{code}

% \subsection{Normalisation in the empty context}
% 
% For normalisation in arbitrary contexts, see e.g.\ \cite{lmcs:4005}.
% 
% \begin{code}
% normt : I.Tm I.∙ Aᴵ → St.⟦ Aᴵ ⟧T
% normt tᴵ = St.⟦ tᴵ ⟧t _
% 
% norms : I.Sub I.∙ Γᴵ → St.⟦ Γᴵ ⟧C
% norms γᴵ = St.⟦ γᴵ ⟧S _
% \end{code}
% 
% Quoting normal forms to terms:
% \begin{code}
% {-
% ⌜_⌝N : ℕ → Tm ∙ Nat
% ⌜ O ⌝N = zero
% ⌜ S n ⌝N = suc ⌜ n ⌝N
% 
% ⌜_⌝B : 𝟚 → Tm ∙ Bool
% ⌜ O ⌝B = false
% ⌜ I ⌝B = true
% 
% ⌜_⌝ₜ : ∀ {A} → ⟦ A ⟧T → Tm ∙ A
% ⌜_⌝ₜ {Nat} = ⌜_⌝N
% ⌜_⌝ₜ {Bool} = ⌜_⌝B
% -}
% \end{code}
% 
% Quoting commutes with the operations \verb$isZero$, \verb$_+_$ and \verb$ite$:
% \begin{code}
% {-
% isZero-⌜⌝ : ∀ {n} → ⌜ isO n ⌝ₜ ≡ isZero ⌜ n ⌝ₜ
% isZero-⌜⌝ {O} = refl
% isZero-⌜⌝ {S n} = refl
% 
% +-⌜⌝ : ∀ {m n} → ⌜ m +ℕ n ⌝ₜ ≡ ⌜ m ⌝ₜ + ⌜ n ⌝ₜ
% +-⌜⌝ {O} = refl
% +-⌜⌝ {S m} = ap suc (+-⌜⌝ {m})
% 
% ite-⌜⌝ : ∀ {A b} {u v : ⟦ A ⟧T} →
%   ⌜_⌝ₜ {A} (if b then u else v) ≡ ite ⌜ b ⌝ₜ ⌜ u ⌝ₜ ⌜ v ⌝ₜ
% ite-⌜⌝ {b = O} = refl
% ite-⌜⌝ {b = I} = refl
% -}
% \end{code}
% An element of the interpretation of a context can be quoted into
% substitution from the empty context:
% \begin{code}
% {-
% ⌜_⌝ₛ : ∀ {Γ} → ⟦ Γ ⟧C → Sub ∙ Γ
% ⌜_⌝ₛ {∙} _ = id
% ⌜_⌝ₛ {Γ ▹ A} (σ ,Σ t) = ⌜ σ ⌝ₛ , ⌜ t ⌝ₜ
% -}
% \end{code}
% 
% Completeness of normalisation in the empty context is proved by
% induction on the syntax.
% \begin{code}
% {-
% Comp : DepModel
% Comp = record
%          { Con = λ _ → ↑p 𝟙
%          ; Ty = λ _ → ↑p 𝟙
%          ; Sub = λ _ _ σ' →
%                    ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
%                      ↑p (⌜ normₛ (σ' ∘ ν') ⌝ₛ ≡ σ' ∘ ν')
%          ; Tm = λ _ _ t' →
%                   ∀ {ν'} → ↑p (⌜ normₛ ν' ⌝ₛ ≡ ν') →
%                      ↑p (⌜ normₜ (t' [ ν' ]) ⌝ₜ ≡ t' [ ν' ])
%          ; ∙ = _
%          ; _▹_ = _
%          ; Nat = _
%          ; Bool = _
%          ; _∘_ = λ σ δ → σ ∘f δ
%          ; id = idf
%          ; ε = λ _ → ↑[ ∙η ◾ ∙η ⁻¹ ]↑
%          ; _,_ = λ σ t h → ↑[ ap2 _,_ ↓[ σ h ]↓ ↓[ t h ]↓ ]↑
%          ; p = λ h → ↑[ ap (p ∘_) ↓[ h ]↓ ]↑
%          ; q = λ h → ↑[ ap (q [_]) ↓[ h ]↓ ]↑
%          ; _[_] = λ t σ → t ∘f σ
%          ; zero = λ _ → refl↑
%          ; suc = λ n h → ↑[ ap suc ↓[ n h ]↓ ]↑
%          ; isZero = λ n h → ↑[ isZero-⌜⌝
%                              ◾ ap isZero ↓[ n h ]↓ ]↑
%          ; _+_ = λ {_ m'} m n {ν'} h →
%                    ↑[ +-⌜⌝ {normₜ (m' [ ν' ])}
%                     ◾ ap2 _+_ ↓[ m h ]↓ ↓[ n h ]↓ ]↑
%          ; true = λ _ → refl↑
%          ; false = λ _ → refl↑
%          ; ite = λ {_ _ b'} b u v {ν'} h →
%                    ↑[ ite-⌜⌝ {b = normₜ (b' [ ν' ])}
%                     ◾ ap3 ite ↓[ b h ]↓ ↓[ u h ]↓ ↓[ v h ]↓ ]↑
%          ; ass = refl
%          ; idl = refl
%          ; idr = refl
%          ; ∙η = refl
%          ; ▹β₁ = refl
%          ; ▹β₂ = refl
%          ; ▹η = refl
%          ; [id] = refl
%          ; [∘] = refl
%          ; zero[] = refl
%          ; suc[] = refl
%          ; isZero[] = refl
%          ; +[] = refl
%          ; true[] = refl
%          ; false[] = refl
%          ; ite[] = refl
%          ; isZeroβ₁ = refl
%          ; isZeroβ₂ = refl
%          ; +β₁ = refl
%          ; +β₂ = refl
%          ; iteβ₁ = refl
%          ; iteβ₂ = refl
%          }
% module Comp = DepModel Comp
% 
% completenessₜ : ∀ {A} {t : Tm ∙ A} → ⌜ normₜ t ⌝ₜ ≡ t
% completenessₜ {t = t} = ↓[ Comp.⟦ t ⟧t {id} refl↑ ]↓
% 
% completenessₛ : ∀ {Γ} {σ : Sub ∙ Γ} → ⌜ normₛ σ ⌝ₛ ≡ σ
% completenessₛ {σ = σ} = ↓[ Comp.⟦ σ ⟧S {id} refl↑ ]↓
% -}
% \end{code}
% Stability for terms:
% \begin{code}
% {-
% stabilityN : ∀ {n} → normₜ ⌜ n ⌝N ≡ n
% stabilityN {O} = refl
% stabilityN {S n} = ap S stabilityN
% 
% stabilityB : ∀ {b} → normₜ ⌜ b ⌝B ≡ b
% stabilityB {O} = refl
% stabilityB {I} = refl
% 
% stabilityₜ : ∀ {A} {t : ⟦ A ⟧T} → normₜ (⌜_⌝ₜ {A} t) ≡ t
% stabilityₜ {Nat} = stabilityN
% stabilityₜ {Bool} = stabilityB
% -}
% \end{code}
% Stability for substitutions:
% \begin{code}
% {-
% stabilityₛ : ∀ {Γ} {σ : ⟦ Γ ⟧C} → normₛ (⌜_⌝ₛ {Γ} σ) ≡ σ
% stabilityₛ {∙} = refl
% stabilityₛ {Γ ▹ A} = ap2 _,Σ_ (stabilityₛ {Γ}) (stabilityₜ {A})
% -}
% \end{code}
