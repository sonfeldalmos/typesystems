\section{The standard algebra}

The new components of the standard algebra:
\begin{code}[hide]
{-
St : Algebra
St = record
       { Con        = Set
       ; Sub        = λ Γ Δ → Γ → Δ
       ; Ty         = Set
       ; Tm         = λ Γ A → Γ → A
       ; _∘_        = _∘f_
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = _∘f_
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = ↑p 𝟙
       ; ε          = const *↑
       ; ∙η         = refl
       ; _▹_        = _×ₘ_
       ; _,_        = λ σ t γ → σ γ ,Σ t γ
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl

       ; Bool       = 𝟚
       ; true       = const I
       ; false      = const O
       ; ite        = λ b t f γ → if b γ then t γ else f γ
       ; Boolβ₁     = refl
       ; Boolβ₂     = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
\end{code}
\begin{code}
       ; Nat        = ℕ
       ; zero       = const O
       ; suc        = S ∘f_
       ; iteNat     = λ z s n γ → ite-ℕ (z γ) (λ n → s (γ ,Σ n)) (n γ)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; iteNat[]   = refl
\end{code}
\begin{code}[hide]
       ; _⇒_        = λ A B → A → B
       ; lam        = λ t γ a → t (γ ,Σ a)
       ; app        = λ { t (γ ,Σ a) → t γ a }
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       
       ; Unit       = ↑p 𝟙
       ; tt         = const *↑
       ; Unitη      = refl
       
       ; _×_        = _×ₘ_
       ; ⟨_,_⟩      = λ a b γ → (a γ ,Σ b γ)
       ; proj₁      = λ t γ → π₁ (t γ)
       ; proj₂      = λ t γ → π₂ (t γ)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
\end{code}
\begin{code}
       ; List       = λ A → ⁅ A ⁆
       ; nil        = λ Γ → ⁅⁆
       ; cons       = λ t ts γ → t γ ∷ ts γ
       ; iteList    = λ b f l γ → ite-⁅⁆ (b γ) (λ a b → f (γ ,Σ b ,Σ a)) (l γ)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; iteList[]  = refl

       ; Tree      = 𝕋3
       ; leaf      = λ a γ → Leaf (a γ)
       ; node      = λ ll rr γ → Node (ll γ) (rr γ)
       ; iteTree   = λ l n t γ → ite-𝕋3 (λ a → l (γ ,Σ a)) (λ ll rr → n (γ ,Σ ll ,Σ rr)) (t γ)
       ; Treeβ₁    = refl
       ; Treeβ₂    = refl
       ; leaf[]    = refl
       ; node[]    = refl
       ; iteTree[] = refl
\end{code}
\begin{code}[hide]
       }
\end{code}

\begin{code}[hide]
record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×_

  field
    Con : I.Con → Set i
    Ty  : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ

    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙ : Con I.∙
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ cong (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

    Bool : Ty I.Bool
    true : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.true
    false : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Bool I.false
    ite : ∀ {Γ' A' b' t' f'}  {Γ : Con Γ'}{A : Ty A'} →
      Tm Γ Bool b' → Tm Γ A t' → Tm Γ A f' → Tm Γ A (I.ite b' t' f')
    true[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      true [ σ ] ≡ true
    false[] : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      false [ σ ] ≡ false
    ite[] : ∀ {Γ' Δ' A' b' t' f' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {b : Tm Δ Bool b'}{t : Tm Δ A t'}{f : Tm Δ A f'}
      {σ : Sub Γ Δ σ'} →
      (ite b t f) [ σ ] ≡ ite (b [ σ ]) (t [ σ ]) (f [ σ ])
    iteβ₁ : ∀ {Γ' A' t' f'} {Γ : Con Γ'}{A : Ty A'}
      {t : Tm Γ A t'}{f : Tm Γ A f'} → ite true t f ≡ t
    iteβ₂ : ∀ {Γ' A' t' f'} {Γ : Con Γ'}{A : Ty A'}
      {t : Tm Γ A t'}{f : Tm Γ A f'} → ite false t f ≡ f

    Nat        : Ty I.Nat
    zero       : ∀{Γ'}{Γ : Con Γ'} → Tm Γ Nat I.zero
    suc        : ∀{Γ' n'}{Γ : Con Γ'} →
                 Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    iteNat     : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A z' → Tm (Γ ▹ A) A s' → Tm Γ Nat t' → Tm Γ A (I.iteNat z' s' t')
    Natβ₁      : ∀{Γ' A' z' s'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'} → iteNat z s zero ≡ z
    Natβ₂      : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'}{t : Tm Γ Nat t'} → iteNat z s (suc t) ≡ s [ id , iteNat z s t ]
    zero[]     : ∀{Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
                 zero [ σ ] ≡ zero
    suc[]      : ∀{Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
                 (suc n) [ σ ] ≡ suc (n [ σ ])
    iteNat[]   : ∀{Γ' Δ' A' z' s' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{z : Tm Δ A z'}{s : Tm (Δ ▹ A) A s'}{t : Tm Δ Nat t'}{σ : Sub Γ Δ σ'} →
                 iteNat z s t [ σ ] ≡ iteNat (z [ σ ]) (s [ σ ∘ p , q ]) (t [ σ ])

    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')
    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
      
    Unit : Ty I.Unit
    tt : ∀ {Γ'} {Γ : Con Γ'} → Tm Γ Unit I.tt
    Unitη : ∀ {Γ' t'} {Γ : Con Γ'} {t : Tm Γ Unit t'} →
      t =[ cong (Tm Γ Unit) I.Unitη ]= tt

    _×_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.× B')
    ⟨_,_⟩ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ A u' → Tm Γ B v' → Tm Γ (A × B) I.⟨ u' , v' ⟩
    proj₁ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ A (I.proj₁ t')
    proj₂ : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A × B) t' → Tm Γ B (I.proj₂ t')
    ×β₁ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₁ ⟨ u , v ⟩ ≡ u
    ×β₂ : ∀ {Γ' A' B' u' v'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Γ A u'}{v : Tm Γ B v'} → proj₂ ⟨ u , v ⟩ ≡ v
    ×η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A × B) t'} → ⟨ proj₁ t , proj₂ t ⟩ ≡ t
    ⟨,⟩[] : ∀ {Γ' Δ' A' B' u' v' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {u : Tm Δ A u'}{v : Tm Δ B v'}{σ : Sub Γ Δ σ'} →
      ⟨ u , v ⟩ [ σ ] ≡ ⟨ u [ σ ] , v [ σ ] ⟩
      
    List       : ∀{A'} → Ty A' → Ty (I.List A')
    nil        : ∀{Γ' A'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (List A) I.nil
    cons       : ∀{Γ' A' a' as'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A a' → Tm Γ (List A) as' → Tm Γ (List A) (I.cons a' as')
    iteList    : ∀{Γ' A' B' e' f' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm Γ B e' → Tm (Γ ▹ A ▹ B) B f'  → Tm Γ (List A) t' → Tm Γ B (I.iteList e' f' t')
    Listβ₁     : ∀{Γ' A' B' e' f'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{e : Tm Γ B e'}{f : Tm (Γ ▹ A ▹ B) B f'} → iteList e f nil ≡ e
    Listβ₂     : ∀{Γ' A' B' e' f' a' as'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{e : Tm Γ B e'}{f : Tm (Γ ▹ A ▹ B) B f'}{a : Tm Γ A a'}{as : Tm Γ (List A) as'} →
                 iteList e f (cons a as) ≡ (f [ id , a , iteList e f as ])
    nil[]      : ∀{Γ' Δ' A' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{σ : Sub Γ Δ σ'} → nil {Γ = Δ}{A = A} [ σ ] ≡ nil
    cons[]     : ∀{Γ' Δ' A' a' as' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{as : Tm Δ (List A) as'}{a : Tm Δ A a'}{σ : Sub Γ Δ σ'} →
                 (cons a as) [ σ ] ≡ cons (a [ σ ]) (as [ σ ])
    iteList[]  : ∀{Γ' Δ' A' B' u' v' as' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}{u : Tm Δ B u'}{v : Tm (Δ ▹ A ▹ B) B v'}{as : Tm Δ (List A) as'}{σ : Sub Γ Δ σ'} →
                 iteList u v as [ σ ] ≡ iteList (u [ σ ]) (v [ (σ ∘ p , q) ∘ p , q ]) (as [ σ ])
                 
    Tree       : ∀{A} → Ty A → Ty (I.Tree A)
    leaf       : ∀{Γ' A' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A t' → Tm Γ (Tree A) (I.leaf t')
    node       : ∀{Γ' A' ll' rr'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ (Tree A) ll' → Tm Γ (Tree A) rr' → Tm Γ (Tree A) (I.node ll' rr')
    iteTree    : ∀{Γ' A' B' l' n' t'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} → Tm (Γ ▹ A) B l' → Tm (Γ ▹ B ▹ B) B n' → Tm Γ (Tree A) t' → Tm Γ B (I.iteTree l' n' t')
    Treeβ₁     : ∀{Γ' A' B' l' n' a'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{l : Tm (Γ ▹ A) B l'}{n : Tm (Γ ▹ B ▹ B) B n'}{a : Tm Γ A a'} → iteTree l n (leaf a) ≡ l [ id , a ]
    Treeβ₂     : ∀{Γ' A' B' l' n' ll' rr'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{l : Tm (Γ ▹ A) B l'}{n : Tm (Γ ▹ B ▹ B) B n'}{ll : Tm Γ (Tree A) ll'}{rr : Tm Γ (Tree A) rr'} →
                  iteTree l n (node ll rr) ≡ n [ id , iteTree l n ll , iteTree l n rr ]
    leaf[]     : ∀{Γ' Δ' A' a' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{a : Tm Δ A a'}{σ : Sub Γ Δ σ'} → leaf {A = A} a [ σ ] ≡ leaf (a [ σ ])
    node[]     : ∀{Γ' Δ' A' ll' rr' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{ll : Tm Δ (Tree A) ll'}{rr : Tm Δ (Tree A) rr'}{σ : Sub Γ Δ σ'} →
                  (node ll rr) [ σ ] ≡ node (ll [ σ ]) (rr [ σ ])
    iteTree[]  : ∀{Γ' Δ' A' B' l' n' t' σ'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'}{Δ : Con Δ'}{l : Tm (Δ ▹ A) B l'}{n : Tm (Δ ▹ B ▹ B) B n'}{t : Tm Δ (Tree A) t'}{σ : Sub Γ Δ σ'} →
                  iteTree l n t [ σ ] ≡ iteTree (l [ (σ ∘ p) , q ]) (n [ (σ ∘ p ∘ p) , (q [ p ]) , q ]) (t [ σ ])

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.List A ⟧T = List ⟦ A ⟧T
  ⟦ I.Tree A ⟧T = Tree ⟦ A ⟧T
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ I.Unit ⟧T = Unit
  ⟦ A I.× B ⟧T = ⟦ A ⟧T × ⟦ B ⟧T

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦iteNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.iteNat u v t ⟧t ≡ iteNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    ⟦true⟧ : ∀ {Γ} → ⟦ I.true {Γ} ⟧t ≡ true
    ⟦false⟧ : ∀ {Γ} → ⟦ I.false {Γ} ⟧t ≡ false
    ⟦ite⟧ : ∀ {Γ A} {b : I.Tm Γ I.Bool}{u v : I.Tm Γ A} →
      ⟦ I.ite b u v ⟧t ≡ ite ⟦ b ⟧t ⟦ u ⟧t ⟦ v ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦ite⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}
    
    ⟦tt⟧ : ∀ {Γ} → ⟦ I.tt {Γ} ⟧t ≡ tt
    {-# REWRITE ⟦tt⟧ #-}

    ⟦⟨,⟩⟧ : ∀ {Γ A B} {u : I.Tm Γ A}{v : I.Tm Γ B} →
      ⟦ I.⟨ u , v ⟩ ⟧t ≡ ⟨ ⟦ u ⟧t , ⟦ v ⟧t ⟩
    ⟦proj₁⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₁ t ⟧t ≡ proj₁ ⟦ t ⟧t
    ⟦proj₂⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.× B)} →
      ⟦ I.proj₂ t ⟧t ≡ proj₂ ⟦ t ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦proj₁⟧ ⟦proj₂⟧ #-}
    
    ⟦nil⟧ : ∀ {Γ A} → ⟦ I.nil {Γ}{A} ⟧t ≡ nil
    ⟦cons⟧ : ∀ {Γ A}{a : I.Tm Γ A}{as : I.Tm Γ (I.List A)} →
      ⟦ I.cons a as ⟧t ≡ cons ⟦ a ⟧t ⟦ as ⟧t
    ⟦iteList⟧ : ∀{Γ A B}{e : I.Tm Γ B}{f : I.Tm (Γ I.▹ A I.▹ B) B}{as : I.Tm Γ (I.List A)} →
      ⟦ I.iteList e f as ⟧t ≡ iteList ⟦ e ⟧t ⟦ f ⟧t ⟦ as ⟧t
    {-# REWRITE ⟦nil⟧ ⟦cons⟧ ⟦iteList⟧ #-}
    
    ⟦leaf⟧ : ∀ {Γ A}{t : I.Tm Γ A} → ⟦ I.leaf {Γ}{A} t ⟧t ≡ leaf ⟦ t ⟧t
    ⟦node⟧ : ∀ {Γ A} {t t' : I.Tm Γ (I.Tree A)} →
      ⟦ I.node t t' ⟧t ≡ node ⟦ t ⟧t ⟦ t' ⟧t
    ⟦iteTree⟧ : ∀{Γ A B}{u : I.Tm (Γ I.▹ A) B}{v : I.Tm (Γ I.▹ B I.▹ B) B}{t : I.Tm Γ (I.Tree A)} →
      ⟦ I.iteTree u v t ⟧t ≡ iteTree ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦leaf⟧ ⟦node⟧ ⟦iteTree⟧ #-}

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
\end{code}

\section{Canonicity}

\begin{code}[hide]
open I

-- Bool

data PBool : Tm ∙ Bool → Set where
  Ptrue  : PBool true
  Pfalse : PBool false

Pite :
  {A' : Ty}{t' : Tm ∙ Bool}{u' v' : Tm ∙ A'}(A : Tm ∙ A' → Set) →
  PBool t' → A u' → A v' → A (ite t' u' v')
Pite {u' = u'} {v'} A Ptrue  u v = u
Pite {u' = u'} {v'} A Pfalse u v = v

PcanBool : ∀{t'} → PBool t' → ↑p (t' ≡ true) ⊎ ↑p (t' ≡ false)
PcanBool Ptrue  = ι₁ ↑[ refl ]↑
PcanBool Pfalse = ι₂ ↑[ refl ]↑

-- Nat

\end{code}

To prove canonicity for natural numbers as, the predicate for natural
numbers \verb$PNat$ is given inductively saying that the preicate
holds for \verb$zero$ and if it holds for a number, it holds for its
successor.
\begin{code}
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀{n'} → PNat n' → PNat (suc n')
\end{code}
To define \verb$iteNat$ in the canonicity dependent algebra, we will need
that if the terms \verb$u'$ and \verb$v'$ preserve the predicate for a type
\verb$A'$, then if \verb$PNat$ holds for \verb$t'$, then the predicate for \verb$A'$
holds for \verb$iteNat u' v' t'$:
\begin{code}
PiteNat :
  {A' : Ty}{u' : Tm ∙ A'}{v' : Tm (∙ ▹ A') A'}{t' : Tm ∙ Nat}
  (A : Tm ∙ A' → Set)(u : A u')(v : ∀{w'} → A w' → A (v' [ id , w' ]))
  (t : PNat t') →
  A (iteNat u' v' t')
PiteNat {A'}{u'}{v'}{.zero}    A u v Pzero    = u
PiteNat {A'}{u'}{v'}{.(suc _)} A u v (Psuc t) = v (PiteNat A u v t)
\end{code}
If the predicate \verb$PNat$ holds for a term, it is either zero or the successor of
another term.
\begin{code}
PcanNat : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat Pzero    = ι₁ ↑[ refl ]↑
PcanNat (Psuc t) with PcanNat t
... | ι₁ e = ι₂ (zero ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
... | ι₂ (t' ,Σ e) = ι₂ (suc t' ,Σ ↑[ ap suc ↓[ e ]↓ ]↑)
\end{code}
\begin{code}[hide]
-- This is simpler ;)
PcanNat' : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat' Pzero         = ι₁ refl↑
PcanNat' (Psuc {n'} t) = ι₂ (n' ,Σ refl↑)
\end{code}

\begin{code}[hide]

-- List

\end{code}
We have a similar predicate for lists and a function which shows that the recursor
preserves this predicate.
\begin{code}
data PList {A'}(A : Tm ∙ A' → Set) : Tm ∙ (List A') → Set where
  Pnil : PList A nil
  Pcons : ∀{u' t'} → A u' → PList A t' → PList A (cons u' t')

PiteList :
  {A' B' : Ty}{u' : Tm ∙ B'}{v' : Tm (∙ ▹ A' ▹ B') B'}{t' : Tm ∙ (List A')}
  {A : Tm ∙ A' → Set}(B : Tm ∙ B' → Set)(u : B u')(v : ∀{w₁' w₂'} → A w₁' → B w₂' → B (v' [ id , w₁' , w₂' ]))(t : PList A t') →
  B (iteList u' v' t')
PiteList B u v Pnil = u
PiteList B u v (Pcons w t) = v w (PiteList B u v t)
\end{code}
Canonicity for lists follows from the predicate;
\begin{code}
PcanList :
  {A' : Ty} → {t' : Tm ∙ (List A')} → {A : Tm ∙ A' → Set} → PList A t' →
  ↑p (t' ≡ nil) ⊎ Σ (Tm ∙ A') λ a' → Σ (Tm ∙ (List A')) λ as' → ↑p (t' ≡ cons a' as')
PcanList Pnil = ι₁ refl↑
PcanList (Pcons {a'} {as'} a as) = ι₂ (a' ,Σ (as' ,Σ refl↑))
\end{code}
\begin{code}[hide]

-- Tree

\end{code}
The same for trees:
\begin{code}
data PTree {A' : Ty} (A : Tm ∙ A' → Set) : Tm ∙ (Tree A') → Set where
  Pleaf : {a' : Tm ∙ A'} → A a' → PTree A (leaf a')
  Pnode : {ll' rr' : Tm ∙ (Tree A')} →
           PTree A ll' → PTree A rr' → PTree A (node ll' rr')

PiteTree :
  {A' B' : Ty} → {l' : Tm (∙ ▹ A') B'} → {n' : Tm (∙ ▹ B' ▹ B') B'} → {t' : Tm ∙ (Tree A')} →
  {A : Tm ∙ A' → Set} → (B : Tm ∙ B' → Set) →
  (l : {a' : Tm ∙ A'} → A a' → B (l' [ id , a' ])) →
  (n : {ll rr : Tm ∙ B'} → B ll → B rr → B (n' [ id , ll , rr ])) →
  (t : PTree A t') → B (iteTree l' n' t')
PiteTree B l n (Pleaf a) = l a
PiteTree B l n (Pnode ll rr) = n (PiteTree B l n ll) (PiteTree B l n rr)

PcanTree :
  {A' : Ty} → {t' : Tm ∙ (Tree A')} → {A : Tm ∙ A' → Set} → PTree A t' →
  (Σ (Tm ∙ A') λ a' → ↑p (t' ≡ leaf a')) ⊎
  Σ (Tm ∙ (Tree A')) λ ll' → Σ (Tm ∙ (Tree A')) λ rr' → ↑p (t' ≡ node ll' rr')
PcanTree (Pleaf {a'} a) = ι₁ (a' ,Σ refl↑)
PcanTree (Pnode {ll'} {rr'} ll rr) = ι₂ (ll' ,Σ (rr' ,Σ refl↑))
\end{code}
Now we can define the new components of the canonicity dependent algebra:
\begin{code}
Can : DepAlgebra
Can = iteord
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = refl
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ×ₘ A (q [ σ' ])
       ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
\end{code}
\begin{code}[hide]
       ; Bool       = PBool
       ; true       = λ _ → Ptrue
       ; false      = λ _ → Pfalse
       ; ite        = λ where {A = A} t u v {ν'} ν̂ → Pite A (t ν̂) (u ν̂) (v ν̂)
       ; iteβ₁      = refl
       ; iteβ₂      = refl
       ; true[]     = refl
       ; false[]    = refl
       ; ite[]      = refl
\end{code}
\begin{code}
       ; Nat        = PNat
       ; zero       = λ _ → Pzero
       ; suc        = λ t ν̂ → Psuc (t ν̂)
       ; iteNat     = λ where {A = A} u v t {ν'} ν̂ → PiteNat A (u ν̂) (λ ŵ → v (ν̂ ,Σ ŵ)) (t ν̂)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; iteNat[]   = refl
\end{code}
\begin{code}[hide]
       ; _⇒_        = λ {A'}{B'} A B t' → (u' : Tm ∙ A') → A u' → B (t' $ u')
       ; lam        = λ t {ν'} ν̂ u' û → t {ν' , u'} (ν̂ ,Σ û)
       ; app        = λ t {ν'} ν̂ → t {p ∘ ν'} (π₁ ν̂) (q [ ν' ]) (π₂ ν̂)
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl

       ; Unit       = λ _ → ↑p 𝟙
       ; tt         = _
       ; Unitη      = refl

       ; _×_        = λ A B t' → A (proj₁ t') ×ₘ B (proj₂ t')
       ; ⟨_,_⟩      = λ u v ν̂ → u ν̂ ,Σ v ν̂
       ; proj₁      = λ t ν̂ → π₁ (t ν̂)
       ; proj₂      = λ t ν̂ → π₂ (t ν̂)
       ; ×β₁        = refl
       ; ×β₂        = refl
       ; ×η         = refl
       ; ⟨,⟩[]      = refl
\end{code}
\begin{code}
       ; List       = PList
       ; nil        = λ _ → Pnil
       ; cons       = λ u t ν̂ → Pcons (u ν̂) (t ν̂)
       ; iteList    = λ where {B = B} u v t ν̂ → PiteList B (u ν̂) (λ w₁ w₂ → v (ν̂ ,Σ w₁ ,Σ w₂)) (t ν̂)
       ; Listβ₁     = refl
       ; Listβ₂     = refl
       ; nil[]      = refl
       ; cons[]     = refl
       ; iteList[]  = refl

       ; Tree       = PTree
       ; leaf       = λ a ν̂ → Pleaf (a ν̂)
       ; node       = λ ll rr ν̂ → Pnode (ll ν̂) (rr ν̂)
       ; iteTree    = λ where {B = B} l n t ν̂ → PiteTree B (λ b → l (ν̂  ,Σ b)) (λ ll rr → n (ν̂ ,Σ ll ,Σ rr)) (t ν̂ )
       ; Treeβ₁     = refl
       ; Treeβ₂     = refl
       ; leaf[]     = refl
       ; node[]     = refl
       ; iteTree[]  = refl
\end{code}
\begin{code}[hide]
       }
module Can = DepAlgebra Can
\end{code}
Interpretating into the canonicity dependent algebra gives actual canonicity for
terms:
\begin{code}
canNat : (t : Tm ∙ Nat) → ↑p (t ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t ≡ suc u)
canNat t = PcanNat (Can.⟦ t ⟧t {id} ↑[ * ]↑)

canList : ∀{A}(t : Tm ∙ (List A)) →
  ↑p (t ≡ nil) ⊎ Σ (Tm ∙ A) λ u → Σ (Tm ∙ (List A)) λ v → ↑p (t ≡ cons u v)
canList t = PcanList (Can.⟦ t ⟧t {id} *↑)

canTree : ∀{A}(t : Tm ∙ (Tree A)) →
  (Σ (Tm ∙ A) λ a → ↑p (t ≡ leaf a)) ⊎
  Σ (Tm ∙ (Tree A)) λ ll → Σ (Tm ∙ (Tree A)) λ rr → ↑p (t ≡ node ll rr)
canTree t = PcanTree (Can.⟦ t ⟧t {id} *↑)
-}
\end{code}
