\chapter{Coinductive types}
\label{ch:Coind}

\begin{tcolorbox}[title=Learning goals of this chapter]
  Coinductive types by example: streams, machines.
\end{tcolorbox}

TODO: products as coinductive types.

TODO: functions as coinductive types, streams as functions. \cite{DBLP:conf/tlca/AhrensCS15}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting --guardedness #-}
open import Lib

module Coind where

module I where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 ⟨_,_⟩
  
  data Ty         : Set where
    _⇒_           : Ty → Ty → Ty
    _×o_          : Ty → Ty → Ty
    Bool          : Ty
    Nat           : Ty
    Stream        : Ty → Ty
    Machine       : Ty

  data Con        : Set where
    ∙             : Con
    _▹_           : Con → Ty → Con

  postulate
    Sub           : Con → Con → Set
    _⊚_           : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass           : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id            : ∀{Γ} → Sub Γ Γ
    idl           : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr           : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε             : ∀{Γ} → Sub Γ ∙
    ∙η            : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm            : Con → Ty → Set
    _[_]          : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]           : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]          : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_          : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p             : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q             : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η            : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    lam           : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_           : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β            : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η            : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                    (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]           : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                    (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    ⟨_,_⟩         : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
    ×β₂           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
    ×η            : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
    ,[]           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                    ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩

    true          : ∀{Γ} → Tm Γ Bool
    false         : ∀{Γ} → Tm Γ Bool
    iteBool       : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁        : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂        : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]       : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                    iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

    zeroo         : ∀{Γ} → Tm Γ Nat
    suco          : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat        : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                    iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
    zero[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]         : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

    head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
    tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
    genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
    Streamβ₁      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    head (genStream u v t) ≡ u [ id ,o t ]
    Streamβ₂      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    tail (genStream u v t) ≡ genStream u v (v [ id ,o t ])
    head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                    head as [ γ ] ≡ head (as [ γ ])
    tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
    genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                    {γ : Sub Δ Γ} →
                    genStream u v t [ γ ] ≡ genStream (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])

    put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
    set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
    get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
    genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                    Tm Γ C → Tm Γ Machine
    Machineβ₁     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{t' : Tm Γ Nat} →
                    put (genMachine u v w t) t' ≡ genMachine u v w (u [ id ,o t ,o t' ])
    Machineβ₂     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    set (genMachine u v w t) ≡ genMachine u v w (v [ id ,o t ])
    Machineβ₃     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    get (genMachine u v w t) ≡ w [ id ,o t ]
    put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
    set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
    get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
    genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                    genMachine u v w t [ γ ] ≡
                    genMachine (u [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (w [ γ ⊚ p ,o q ]) (t [ γ ]) 

variable
  Aᴵ Bᴵ : I.Ty
  Γᴵ Δᴵ Θᴵ : I.Con
  γᴵ δᴵ θᴵ σᴵ γaᴵ : I.Sub Δᴵ Γᴵ
  tᴵ uᴵ vᴵ wᴵ : I.Tm Γᴵ Aᴵ

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 ⟨_,_⟩

  field
    Con           : Set i
    Sub           : Con → Con → Set j
    _⊚_           : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass           : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id            : ∀{Γ} → Sub Γ Γ
    idl           : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr           : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
    
    ∙              : Con
    ε             : ∀{Γ} → Sub Γ ∙
    ∙η            : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε
    
    Ty            : Set k
    Tm            : Con → Ty → Set l
    _[_]          : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]           : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]          : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_            : Con → Ty → Con
    _,o_          : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p             : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q             : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η            : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa
    
    _⇒_            : Ty → Ty → Ty
    lam           : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_           : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β            : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η            : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                    (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]           : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                    (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    _×o_          : Ty → Ty → Ty
    ⟨_,_⟩         : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
    ×β₂           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
    ×η            : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
    ,[]           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                    ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩

    Bool          : Ty
    true          : ∀{Γ} → Tm Γ Bool
    false         : ∀{Γ} → Tm Γ Bool
    iteBool       : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁        : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂        : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]       : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                    iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

    Nat           : Ty
    zeroo         : ∀{Γ} → Tm Γ Nat
    suco          : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat        : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                    iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
    zero[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]         : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])
\end{code}

Coinductive types are dual to inductive types. Inductive types are
specified by their constructors, coinductive types are specified by
their destructors. Their constructor is determined by the destructors:
if we have a type \verb$C$ (the seed) together with terms which have
the shape of the destructors (but using \verb$C$ instead of the
coinductive type), we obtain a function (called corecursor, generator,
anamorphism) from the seed to the coinductive type. The seed is called
like that because the generator makes the coinductive type "grow" out
of it. While elements of inductive type are trees of finite depth,
elements of coinductive types are potentially infinitely deep
trees. Functions out of inductive types are all terminating, functions
producing elements of coinductive types are \emph{productive}.

The usual example is infinite lists or streams. There are two ways to
destruct a stream: look at its first element (head), or forget its
first element (tail). To generate a stream, we need to fix a seed type
\verb$C$ which represents the internal state of the stream; we have to
provide an \verb$A$ from any \verb$C$; we have to be able to step the
internal state when forgetting the first element; and finally, we need
a state to start with. The computation rules say what happens if a
destructor is applied to the constructor (generator): we use the
corresponding method with the actual internal state, and corecursively
call the generator on the new state, if needed.
\begin{code}
    Stream        : Ty → Ty
    head          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ A
    tail          : ∀{Γ A} → Tm Γ (Stream A) → Tm Γ (Stream A)
    genStream     : ∀{Γ A C} → Tm (Γ ▹ C) A → Tm (Γ ▹ C) C → Tm Γ C → Tm Γ (Stream A)
    Streamβ₁      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    head (genStream u v t) ≡ u [ id ,o t ]
    Streamβ₂      : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C} →
                    tail (genStream u v t) ≡ genStream u v (v [ id ,o t ])
    head[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} →
                    head as [ γ ] ≡ head (as [ γ ])
    tail[]        : ∀{Γ A}{as : Tm Γ (Stream A)}{Δ}{γ : Sub Δ Γ} → tail as [ γ ] ≡ tail (as [ γ ])
    genStream[]   : ∀{Γ A C}{u : Tm (Γ ▹ C) A}{v : Tm (Γ ▹ C) C}{t : Tm Γ C}{Δ}
                    {γ : Sub Δ Γ} →
                    genStream u v t [ γ ] ≡ genStream (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])
\end{code}
State machines, virtual machines, Turing machines, servers, operating
systems are all coinductive types. An example simple machine has three
operations: (i) we can put in a natural number, (ii) we can press a button on it,
(iii) we can ask it to output a natural number.
\begin{code}
    Machine       : Ty
    put           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat → Tm Γ Machine
    set           : ∀{Γ} → Tm Γ Machine → Tm Γ Machine
    get           : ∀{Γ} → Tm Γ Machine → Tm Γ Nat
    genMachine    : ∀{Γ C} → Tm (Γ ▹ C ▹ Nat) C → Tm (Γ ▹ C) C → Tm (Γ ▹ C) Nat →
                    Tm Γ C → Tm Γ Machine
    Machineβ₁     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{t' : Tm Γ Nat} →
                    put (genMachine u v w t) t' ≡ genMachine u v w (u [ id ,o t ,o t' ])
    Machineβ₂     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    set (genMachine u v w t) ≡ genMachine u v w (v [ id ,o t ])
    Machineβ₃     : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C} →
                    get (genMachine u v w t) ≡ w [ id ,o t ]
    put[]         : ∀{Γ}{t : Tm Γ Machine}{u : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    put t u [ γ ] ≡ put (t [ γ ]) (u [ γ ])
    set[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → set t [ γ ] ≡ set (t [ γ ])
    get[]         : ∀{Γ}{t : Tm Γ Machine}{Δ}{γ : Sub Δ Γ} → get t [ γ ] ≡ get (t [ γ ])
    genMachine[]  : ∀{Γ C}{u : Tm (Γ ▹ C ▹ Nat) C}{v : Tm (Γ ▹ C) C}{w : Tm (Γ ▹ C) Nat}
                    {t : Tm Γ C}{Δ}{γ : Sub Δ Γ} →
                    genMachine u v w t [ γ ] ≡
                    genMachine (u [ (γ ⊚ p ,o q) ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ]) (w [ γ ⊚ p ,o q ]) (t [ γ ]) 
\end{code}

\begin{code}[hide]
  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]
  v0 : ∀{Γ A}        → Tm (Γ ▹ A) A
  v0 = q
  v1 : ∀{Γ A B}      → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : ∀{Γ A B C}    → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : ∀{Γ A B C D}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]
  ▹η' : ∀{Γ A} → p ,o q ≡ id {Γ ▹ A}
  ▹η' {Γ}{A} =
    p ,o q
      ≡⟨ sym {A = Sub (Γ ▹ A) (Γ ▹ A)}
           (cong {A = Sub (Γ ▹ A) Γ × Tm (Γ ▹ A) A} (λ w → π₁ w ,o π₂ w) (idr , [id])) ⟩
    p ⊚ id ,o q [ id ]
      ≡⟨ ▹η ⟩
    id
      ∎

  ,∘ : ∀{Γ Δ Θ A}{γ : Sub Δ Γ}{t : Tm Δ A}{δ : Sub Θ Δ} →
    (γ ,o t) ⊚ δ ≡ γ ⊚ δ ,o t [ δ ]
  ,∘ {Γ}{Δ}{Θ}{A}{γ}{t}{δ} =
    (γ ,o t) ⊚ δ
      ≡⟨ sym {A = Sub Θ (Γ ▹ A)} ▹η ⟩
    (p ⊚ ((γ ,o t) ⊚ δ) ,o q [ (γ ,o t) ⊚ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w) (sym {A = Sub Θ Γ} ass , [∘]) ⟩
    ((p ⊚ (γ ,o t)) ⊚ δ ,o q [ γ ,o t ] [ δ ])
      ≡⟨ cong {A = Sub Θ Γ × Tm Θ A} (λ w → π₁ w ,o π₂ w)
           (cong (_⊚ δ) ▹β₁ , cong (_[ δ ]) ▹β₂) ⟩
    γ ⊚ δ ,o t [ δ ]
      ∎

  ⟦_⟧T : I.Ty → Ty
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T
  ⟦ A I.×o B ⟧T = ⟦ A ⟧T ×o ⟦ B ⟧T
  ⟦ I.Bool ⟧T = Bool
  ⟦ I.Nat ⟧T = Nat
  ⟦ I.Stream A ⟧T = Stream ⟦ A ⟧T
  ⟦ I.Machine ⟧T = Machine

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S          : I.Sub  Δᴵ  Γᴵ  → Sub  ⟦ Δᴵ ⟧C  ⟦ Γᴵ ⟧C
    ⟦_⟧t          : I.Tm   Γᴵ  Aᴵ  → Tm   ⟦ Γᴵ ⟧C  ⟦ Aᴵ ⟧T
    ⟦∘⟧           : ⟦ γᴵ I.⊚ δᴵ ⟧S                 ≡ ⟦ γᴵ ⟧S ⊚ ⟦ δᴵ ⟧S
    ⟦id⟧          : ⟦ I.id {Γᴵ} ⟧S                 ≡ id
    ⟦ε⟧           : ⟦ I.ε {Γᴵ} ⟧S                  ≡ ε
    ⟦[]⟧          : ⟦ tᴵ I.[ γᴵ ] ⟧t               ≡ ⟦ tᴵ ⟧t [ ⟦ γᴵ ⟧S ]
    ⟦,⟧           : ⟦ γᴵ I.,o tᴵ ⟧S                ≡ ⟦ γᴵ ⟧S ,o ⟦ tᴵ ⟧t
    ⟦p⟧           : ⟦ I.p {Γᴵ}{Aᴵ} ⟧S              ≡ p
    ⟦q⟧           : ⟦ I.q {Γᴵ}{Aᴵ} ⟧t              ≡ q
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦[]⟧ ⟦,⟧ ⟦p⟧ ⟦q⟧ #-}

    ⟦lam⟧         : ⟦ I.lam tᴵ ⟧t                  ≡ lam ⟦ tᴵ ⟧t
    ⟦$⟧           : ⟦ tᴵ I.$ uᴵ ⟧t                 ≡ ⟦ tᴵ ⟧t $ ⟦ uᴵ ⟧t
    {-# REWRITE ⟦lam⟧ ⟦$⟧ #-}
    
    ⟦⟨,⟩⟧         : ⟦ I.⟨ uᴵ , vᴵ ⟩ ⟧t             ≡ ⟨ ⟦ uᴵ ⟧t , ⟦ vᴵ ⟧t ⟩
    ⟦fst⟧         : ⟦ I.fst tᴵ ⟧t                  ≡ fst  ⟦ tᴵ ⟧t
    ⟦snd⟧         : ⟦ I.snd tᴵ ⟧t                  ≡ snd  ⟦ tᴵ ⟧t
    {-# REWRITE ⟦⟨,⟩⟧ ⟦fst⟧ ⟦snd⟧ #-}

    ⟦true⟧        : ⟦ I.true {Γᴵ} ⟧t               ≡ true
    ⟦false⟧       : ⟦ I.false {Γᴵ} ⟧t              ≡ false
    ⟦iteBool⟧     : ⟦ I.iteBool uᴵ vᴵ tᴵ ⟧t        ≡ iteBool ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦true⟧ ⟦false⟧ ⟦iteBool⟧ #-}

    ⟦zero⟧        : ⟦ I.zeroo {Γᴵ} ⟧t              ≡ zeroo
    ⟦suc⟧         : ⟦ I.suco tᴵ ⟧t                 ≡ suco ⟦ tᴵ ⟧t
    ⟦iteNat⟧      : ⟦ I.iteNat uᴵ vᴵ tᴵ ⟧t         ≡ iteNat ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦iteNat⟧ #-}

    ⟦head⟧        : ⟦ I.head tᴵ ⟧t                 ≡ head ⟦ tᴵ ⟧t
    ⟦tail⟧        : ⟦ I.tail tᴵ ⟧t                 ≡ tail ⟦ tᴵ ⟧t
    ⟦genStream⟧   : ⟦ I.genStream uᴵ vᴵ tᴵ ⟧t      ≡ genStream ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦head⟧ ⟦tail⟧ ⟦genStream⟧ #-}

    ⟦put⟧         : ⟦ I.put tᴵ uᴵ ⟧t               ≡ put ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t
    ⟦set⟧         : ⟦ I.set tᴵ ⟧t                  ≡ set ⟦ tᴵ ⟧t
    ⟦get⟧         : ⟦ I.get tᴵ ⟧t                  ≡ get ⟦ tᴵ ⟧t
    ⟦genMachine⟧  : ⟦ I.genMachine uᴵ vᴵ wᴵ tᴵ ⟧t  ≡ genMachine ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t ⟦ wᴵ ⟧t ⟦ tᴵ ⟧t
    {-# REWRITE ⟦put⟧ ⟦set⟧ ⟦get⟧ ⟦genMachine⟧ #-}
\end{code}

\begin{code}[hide]
module Examples {i}{j}{k}{l}(M : Model {i}{j}{k}{l}) where
  open Model M

  plus : Tm ∙ (Nat ⇒ Nat ⇒ Nat)
  plus = lam (lam (iteNat v0 (suco v0) v1))
\end{code}
The stream which contains the number \verb$0,1,2,...$:
\begin{code}
  numbers : Tm ∙ (Stream Nat)
  numbers = genStream v0 (suco v0) zeroo

  testNumbers0 : head numbers ≡ zeroo
  testNumbers0 =
    head numbers
      ≡⟨ refl {A = Tm _ _} ⟩
    head (genStream q (suco q) zeroo)
      ≡⟨ Streamβ₁ ⟩
    q [ id ,o zeroo ]
      ≡⟨ ▹β₂ ⟩
    zeroo
      ∎

  testNumbers2 : head (tail (tail numbers)) ≡ suco (suco zeroo)
  testNumbers2 = exercisep
\end{code}
A machine which adds the numbers that we input unless we press the button, then it adds one when inputting a number. If we
toggle the button again, it will start adding the numbers again. The number it outputs is the current sum.
\begin{code}
  aMachine : Tm ∙ Machine
  aMachine = genMachine ⟨ fst v1 , plus [ ε ] $ snd v1 $ v0 ⟩
    ⟨ iteBool false true (fst v0) , snd v0 ⟩ (snd v0) ⟨ true , zeroo ⟩
\end{code}

\begin{exe}[compulsory]
  For types \verb$A$, \verb$B$, define the coinductive type of machines from which we can get out
  an \verb$A$ and a \verb$B$. List all the rules. Derive these rules from those of binary products.
\end{exe}
\begin{exe}[compulsory]
  Define the coinductive types of machines for which there are two different ways to input a natural number and if we press a button, they output a natural number.
  Implement a machine which has two numbers in its inner state and outputs one or the other alternating.
  If we input a number in the first way, the machine adds that to its first inner number. 
  If we input a number in the second way, the machine adds that to its second inner number. 
\end{exe}

\section{Standard model}

We define streams and machines using Agda's coinductive types.
\begin{code}
record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A
open Stream
genStream : {A C : Set} → (C → A) → (C → C) → C → Stream A
head (genStream f g c) = f c
tail (genStream f g c) = genStream f g (g c)

record Machine : Set where
  coinductive
  field
    put : ℕ → Machine
    set : Machine
    get : ℕ
open Machine
genMachine : {C : Set} → (C → ℕ → C) → (C → C) → (C → ℕ) → C → Machine
put (genMachine f g h c) n = genMachine f g h (f c n)
set (genMachine f g h c) = genMachine f g h (g c)
get (genMachine f g h c) = h c
\end{code}

\begin{code}[hide]
St : Model
St = record
  { Con       = Set
  ; Sub       = λ Δ Γ → Δ → Γ
  ; _⊚_       = λ γ δ θ* → γ (δ θ*)
  ; ass       = λ {Γ}{Δ}{Θ}{Ξ} → refl {A = Ξ → Γ}
  ; id        = λ γ* → γ*
  ; idl       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; idr       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  
  ; ∙         = Lift ⊤
  ; ε         = _
  ; ∙η        = λ {Γ}{σ} → refl {A = Γ → Lift ⊤}
  
  ; Ty        = Set
  
  ; Tm        = λ Γ A → Γ → A
  ; _[_]      = λ a γ δ* → a (γ δ*) 
  ; [∘]       = λ {Γ}{Δ}{Θ}{A} → refl {A = Θ → A}
  ; [id]      = λ {Γ}{A}{a} → refl {A = Γ → A}
  ; _▹_       = _×_
  ; _,o_      = λ γ t δ* → γ δ* , t δ*
  ; p         = π₁
  ; q         = π₂
  ; ▹β₁       = λ {Γ}{Δ} → refl {A = Δ → Γ}
  ; ▹β₂       = λ {Γ}{Δ}{A} → refl {A = Δ → A}
  ; ▹η        = λ {Γ}{Δ}{A} → refl {A = Δ → Γ × A}
  
  ; _⇒_       = λ A B → A → B
  ; lam       = λ t γ* α* → t (γ* , α*)
  ; _$_       = λ t u γ* → t γ* (u γ*)
  ; ⇒β        = λ {Γ}{A}{B}{t}{u} → refl {A = Γ → B}
  ; ⇒η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A → B}
  ; lam[]     = λ {Γ}{A}{B}{t}{Δ}{γ} → refl {A = Δ → A → B}
  ; $[]       = λ {Γ}{A}{B}{t}{u}{Δ}{γ} → refl {A = Δ → B}

  ; _×o_      = _×_
  ; ⟨_,_⟩     = λ a b γ* → (a γ* , b γ*)
  ; fst       = λ t γ* → π₁ (t γ*)
  ; snd       = λ t γ* → π₂ (t γ*)
  ; ×β₁       = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → A}
  ; ×β₂       = λ {Γ}{A}{B}{u}{v} → refl {A = Γ → B}
  ; ×η        = λ {Γ}{A}{B}{t} → refl {A = Γ → A × B}
  ; ,[]       = λ {Γ}{A}{B}{u}{v}{Δ}{γ} → refl {A = Δ → A × B}

  ; Bool      = 𝟚
  ; true      = λ _ → tt
  ; false     = λ _ → ff
  ; iteBool   = λ u v t γ* → if t γ* then u γ* else v γ*
  ; Boolβ₁    = λ {Γ}{A} → refl {A = Γ → A}
  ; Boolβ₂    = λ {Γ}{A} → refl {A = Γ → A}
  ; true[]    = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → tt}
  ; false[]   = λ {Γ}{Δ} → refl {A = Δ → 𝟚}{x = λ _ → ff}
  ; iteBool[] = λ {Γ}{A}{t}{u}{v}{Δ}{γ} → refl {A = Δ → A}

  ; Nat        = ℕ
  ; zeroo      = λ _ → zero
  ; suco       = λ t γ* → suc (t γ*)
  ; iteNat     = λ u v t γ* → rec (u γ*) (λ x → v (γ* , x)) (t γ*)
  ; Natβ₁      = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
  ; Natβ₂      = λ {Γ}{A}{u}{v} → refl {A = Γ → A}
  ; zero[]     = λ {Γ}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ _ → zero}
  ; suc[]      = λ {Γ}{t}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ δ* → suc (t (γ δ*))}
  ; iteNat[]   = λ {Γ}{A}{u}{v}{t}{Δ}{γ} → refl {A = Δ → A}
\end{code}

The new components in the standard model:
\begin{code}
  ; Stream        = Stream
  ; head          = λ t γ* → head (t γ*)
  ; tail          = λ t γ* → tail (t γ*)
  ; genStream     = λ u v t γ* → genStream (λ x → u (γ* , x)) (λ x → v (γ* , x)) (t γ*)
  ; Streamβ₁      = λ {Γ}{A}{C}{u}{v}{t} → refl {A = Γ → A}
  ; Streamβ₂      = λ {Γ}{A}{C}{u}{v}{t} → refl {A = Γ → Stream A}
  ; head[]        = λ {Γ}{A}{t}{Δ}{γ} → refl {A = Δ → A}
  ; tail[]        = λ {Γ}{A}{t}{Δ}{γ} → refl {A = Δ → Stream A}
  ; genStream[]   = λ {Γ}{A}{C}{u}{v}{t}{Δ}{γ} → refl {A = Δ → Stream A}

  ; Machine       = Machine
  ; put           = λ t u γ* → put (t γ*) (u γ*) 
  ; set           = λ t γ* → set (t γ*)
  ; get           = λ t γ* → get (t γ*)
  ; genMachine    = λ u v w t γ* → genMachine (λ x y → u ((γ* , x) , y))
                    (λ x → v (γ* , x)) (λ x → w (γ* , x)) (t γ*)
  ; Machineβ₁     = λ {Γ}{C}{u}{v}{w}{t}{t'} → refl {A = Γ → Machine}
  ; Machineβ₂     = λ {Γ}{C}{u}{v}{w}{t} → refl {A = Γ → Machine}
  ; Machineβ₃     = λ {Γ}{C}{u}{v}{w}{t} → refl {A = Γ → ℕ}{x = λ γ* → w (γ* , t γ*)}
  ; put[]         = λ {Γ}{t}{u}{Δ}{γ} → refl {A = Δ → Machine}
  ; set[]         = λ {Γ}{t}{Δ}{γ} → refl {A = Δ → Machine}
  ; get[]         = λ {Γ}{t}{Δ}{γ} → refl {A = Δ → ℕ}{x = λ γ* → get (t (γ γ*))}
  ; genMachine[]  = λ {Γ}{C}{u}{v}{w}{t}{Δ}{γ} → refl {A = Δ → Machine}
\end{code}
\begin{code}[hide]
  }
\end{code}
