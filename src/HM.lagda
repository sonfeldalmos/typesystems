\chapter{Hindley-Milner polymorphism}\label{ch:hm}

Type inference. Subtyping?

\begin{verbatim}
MTy : Set*
Ty  : Set
Tm  : Ty → Set*
_⇒_ : MTy → MTy → MTy
∀   : (MTy →* Ty) → Ty
i   : MTy -> Ty
lam : (Tm (i A) →* Tm (i B)) ≅ Tm (i (A ⇒ B)) : app
Lam : ((A : MTy) →* Tm (B * A)) ≅ Tm (∀ B) : App
\end{verbatim}

\begin{code}[hide]
{-# OPTIONS --prop --rewriting --guardedness #-}
open import Lib

module HM where

record Model {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _⊚_
  infixl 6 _[_]T
  infixl 5 _▷
  infixl 5 _ʻ_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_
  infixl 7 _×o_
  infixl 5 ⟨_,_⟩

  field
    Con           : Set i
    Sub           : Con → Con → Set j
    _⊚_           : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass           : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id            : ∀{Γ} → Sub Γ Γ
    idl           : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr           : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ
    
    ∙              : Con
    ε             : ∀{Γ} → Sub Γ ∙
    ∙η            : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    -- TODO: add MTy etc.

    Ty            : Con → Set k
    _[_]T         : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T          : ∀{Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ⊚ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T         : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    _▷            : Con → Con
    _ʻ_           : ∀{Γ Δ} → Sub Δ Γ → Ty Δ → Sub Δ (Γ ▷)
    P             : ∀{Γ} → Sub (Γ ▷) Γ
    Q             : ∀{Γ} → Ty (Γ ▷)
    ▷β₁           : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Δ} → P ⊚ (γ ʻ A) ≡ γ
    ▷β₂           : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Δ} → Q [ γ ʻ A ]T ≡ A
    ▷η            : ∀{Γ Δ}{γA : Sub Δ (Γ ▷)} → P ⊚ γA ʻ Q [ γA ]T ≡ γA
    
    Tm            : Con → Ty → Set l
{-
    _[_]          : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]           : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]          : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _▹_            : Con → Ty → Con
    _,o_          : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p             : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q             : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂           : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η            : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa
    
    _⇒_            : Ty → Ty → Ty
    lam           : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_           : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β            : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η            : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]         : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{Δ}{γ : Sub Δ Γ} →
                    (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]           : ∀{Γ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{Δ}{γ : Sub Δ Γ} →
                    (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    _×o_          : Ty → Ty → Ty
    ⟨_,_⟩         : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (A ×o B)
    fst           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ A
    snd           : ∀{Γ A B} → Tm Γ (A ×o B) → Tm Γ B
    ×β₁           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst ⟨ u , v ⟩ ≡ u
    ×β₂           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd ⟨ u , v ⟩ ≡ v
    ×η            : ∀{Γ A B}{t : Tm Γ (A ×o B)} → ⟨ fst t , snd t ⟩ ≡ t
    ,[]           : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B}{Δ}{γ : Sub Δ Γ} →
                    ⟨ u , v ⟩ [ γ ] ≡ ⟨ u [ γ ] , v [ γ ] ⟩

    Bool          : Ty
    true          : ∀{Γ} → Tm Γ Bool
    false         : ∀{Γ} → Tm Γ Bool
    iteBool       : ∀{Γ A} → Tm Γ A → Tm Γ A → Tm Γ Bool → Tm Γ A
    Boolβ₁        : ∀{Γ A u v} → iteBool {Γ}{A} u v true ≡ u
    Boolβ₂        : ∀{Γ A u v} → iteBool {Γ}{A} u v false ≡ v
    true[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
    false[]       : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
    iteBool[]     : ∀{Γ A t u v Δ}{γ : Sub Δ Γ} →
                    iteBool {Γ}{A} u v t [ γ ] ≡ iteBool (u [ γ ]) (v [ γ ]) (t [ γ ])

    Nat           : Ty
    zeroo         : ∀{Γ} → Tm Γ Nat
    suco          : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    iteNat        : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → iteNat u v zeroo ≡ u
    Natβ₂         : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} →
                    iteNat u v (suco t) ≡ v [ id ,o iteNat u v t ]
    zero[]        : ∀{Γ Δ}{γ : Sub Δ Γ} → zeroo [ γ ] ≡ zeroo
    suc[]         : ∀{Γ}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} → (suco t) [ γ ] ≡ suco (t [ γ ])
    iteNat[]      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat}{Δ}{γ : Sub Δ Γ} →
                    iteNat u v t [ γ ] ≡ iteNat (u [ γ ]) (v [ γ ⊚ p ,o q ]) (t [ γ ])
-}
\end{code}

\begin{code}
    
\end{code}
