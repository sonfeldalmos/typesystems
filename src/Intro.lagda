\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

These are the notes for the course on type systems for programming languages given at Eötvös Loránd University (autumn semesters of 2020, 2021, master's course on computer science). Previous versions of this course used Robert Harper's book \cite{harper} (between 2016 autumn and 2019 autumn) and István Csörnyei's books \cite{csornyei,csornyeiLambda} (before 2016). Other excellent books on the topic are \cite{pierce,Pierce:SF2,plfa20.07}.

The uniqueness of these notes is that they define (programming) languages as algebraic structures. This is described in Chapter \ref{ch:NatBool} and \cite{godelTalk}. We don't yet know how to describe all languages at this level of abstraction, this is one reason for covering much less material than the above mentioned books. Most of the material is formalised in Agda, it relies on a shallowly embedded version of setoid type theory \cite{setoid} using rewrite rules \cite{cockxsprinkles}.

These notes are compiled from literate Agda files and are available at \url{https://bitbucket.org/akaposi/typesystems}. They are under continuous development.

Special thanks to Bálint Kocsis and Márk Széles who did the first version of the formalisation during a summer internship in 2020. Thanks to István Donkó who developed materials for the tutorials and contributed significantly to the formalisation. Thanks to the following students who fixed errors and typos: András Kovács, Kálmán Kostenszky. Thanks to the students of the couse for their excellent questions.
