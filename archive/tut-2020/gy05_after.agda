{-# OPTIONS --prop #-}
module gy05_after where

open import Lib

import NatBoolAST as AST
import NatBoolWT as WT
import NatBool as NB

module NB' where

  open NB.I

  eq : ite false true (isZero (zero + zero)) ≡ isZero (ite true zero (zero + zero))
  eq =
    ite false true (isZero (zero + zero))
      ≡⟨ iteβ₂ ⟩
    isZero (zero + zero)
      ≡⟨ cong isZero +β₁ ⟩
    isZero zero
      ≡⟨ cong isZero iteβ₁ ⁻¹ ⟩ -- \^- \^1
    isZero (ite true zero (zero + zero))
      ∎

  -- There are terms that can be distinguished

  isz : ℕ → 𝟚
  isz = λ { 0 → I ; _ → O }

  lem : (x : ℕ) → isz (x +ℕ 1) ≡ O
  lem O = refl
  lem (S x) = refl

  Diff : NB.Algebra
  Diff = record
    { Ty       = Set
    ; Tm       = λ ty → ty
    ; Nat      = ℕ
    ; Bool     = 𝟚
    ; zero     = 0
    ; suc      = S
    -- ; suc      = λ x → x +ℕ 1
    ; isZero   = isz
    ; _+_      = _+ℕ_
    ; true     = I
    ; false    = O
    ; ite      = if_then_else_
    ; isZeroβ₁ = refl
    ; isZeroβ₂ = refl
    -- Only needed if the second version of `suc` is used:
    -- ; isZeroβ₂ = refl λ where {n} → lem n
    ; +β₁      = refl
    ; +β₂      = refl
    ; iteβ₁    = refl
    ; iteβ₂    = refl
    }
  module Diff = NB.Algebra Diff

  neq' : ¬ I ≡ O
  neq' ()

  neq'' : ¬ Diff.⟦ true ⟧t ≡ Diff.⟦ false ⟧t
  neq'' eq = neq' (
      I
        ≡⟨ Diff.⟦true⟧ ⁻¹ ⟩
      Diff.⟦ true ⟧t
        ≡⟨ eq ⟩
      Diff.⟦ false ⟧t
        ≡⟨ Diff.⟦false⟧ ⟩
      O
        ∎
    )

  neq : ¬ true ≡ false
  neq eq = neq'' (cong Diff.⟦_⟧t eq)

