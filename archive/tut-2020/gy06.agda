{-# OPTIONS --prop --rewriting #-}
module gy06 where

open import Lib

module I where
  postulate
    Z     : Set
    zero  : Z
    suc   : Z → Z
    pred  : Z → Z
    sucpred : (i : Z) → suc (pred i) ≡ i
    predsuc : (i : Z) → pred (suc i) ≡ i

record Algebra {i} : Set (lsuc i) where
  field
    Z        : Set i
    zero     : Z
    suc      : Z → Z
    pred     : Z → Z
    sucpred  : (i : Z) → suc (pred i) ≡ i
    predsuc  : (i : Z) → pred (suc i) ≡ i

  postulate
    ⟦_⟧      : I.Z → Z
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦pred⟧ #-}

record DepAlgebra {i} : Set (lsuc i) where
  field
    Z        : I.Z → Set i
    zero     : Z I.zero
    suc      : {i' : I.Z} → Z i' → Z (I.suc  i')
    pred     : {i' : I.Z} → Z i' → Z (I.pred i')
    sucpred  : {i' : I.Z} → (i : Z i') → transport Z (I.sucpred i') (suc (pred i)) ≡ i
    predsuc  : {i' : I.Z} → (i : Z i') → transport Z (I.predsuc i') (pred (suc i)) ≡ i
  postulate
    ⟦_⟧      : (n : I.Z) → Z n
    ⟦zero⟧   : ⟦ I.zero ⟧ ≡ zero
    ⟦suc⟧    : {i : I.Z} → ⟦ I.suc i ⟧ ≡ suc ⟦ i ⟧
    ⟦pred⟧   : {i : I.Z} → ⟦ I.pred i ⟧ ≡ pred ⟦ i ⟧
    {-# REWRITE ⟦zero⟧ #-}


-- Example equalities:

module Examples {i} (Alg : Algebra {i}) where
  open Algebra Alg

  eq1 : pred (suc (pred zero)) ≡ pred zero
  eq1 = {!!}

  eq2 : pred (suc (suc (pred zero))) ≡ zero
  eq2 =
    pred (suc (suc (pred zero)))
      ≡⟨ {!!} ⟩
    zero
      ∎

  eq3 : suc (suc (suc (pred (pred zero)))) ≡ suc zero
  eq3 =
    suc (suc (suc (pred (pred zero))))
      ≡⟨ {!!} ⟩
    suc zero
      ∎


-- Addition:

M : I.Z → Algebra
M i = record
  { Z        = I.Z
  ; zero     = i
  ; suc      = I.suc
  ; pred     = I.pred
  ; sucpred  = I.sucpred
  ; predsuc  = I.predsuc
  }

_+ℤ_ : I.Z → I.Z → I.Z
j +ℤ i = {!!}

+-test-1 : {i : I.Z} →
           I.suc (I.suc (I.pred I.zero)) +ℤ i
           ≡
           I.suc (I.suc (I.pred i))
+-test-1 = refl

+-test-2 : {i : I.Z} →
           I.pred (I.suc (I.suc (I.pred I.zero))) +ℤ i
           ≡
           I.pred (I.suc (I.suc (I.pred i)))
+-test-2 = refl


-- Normal forms:

data Nf  : Set where
  -suc   : ℕ → Nf
  zero   : Nf
  +suc   : ℕ → Nf

minusThree minusFive plusSix : Nf
minusThree  = -suc 2
minusFive   = +suc 4
plusSix     = +suc 5

sucNf : Nf → Nf
sucNf nf = {!!}

predNf : Nf → Nf
predNf nf = {!!}


-- Normalisation:

N : Algebra
N = record
  { Z       = Nf
  ; zero    = {!!}
  ; suc     = {!!}
  ; pred    = {!!}
  ; sucpred = {!!}
  ; predsuc = {!!}
  }
module N = Algebra N

⌜_⌝ : Nf → I.Z
⌜ nf ⌝ = {!!}

norm-test : ⌜ N.⟦ (I.pred (I.pred (I.suc (I.pred (I.pred (I.suc I.zero)))))) ⟧ ⌝
            ≡
            I.pred (I.pred I.zero)
norm-test = refl


-- Stability:

stab : (nf : Nf) → N.⟦ ⌜ nf ⌝ ⟧ ≡ nf
stab nf = {!!}


-- Lemmas for completeness:

⌜suc⌝ : (nf : Nf) → ⌜ sucNf nf ⌝ ≡ I.suc ⌜ nf ⌝
⌜suc⌝ nf = {!!}

⌜pred⌝ : (nf : Nf) → ⌜ predNf nf ⌝ ≡ I.pred ⌜ nf ⌝
⌜pred⌝ nf = {!!}


-- Completeness:

Comp : DepAlgebra
Comp = record
  { Z       = λ t → ↑p (⌜ N.⟦ t ⟧ ⌝ ≡ t)
  ; zero    = {!!}
  ; suc     = {!!}
  ; pred    = {!!}
  ; sucpred = {!!}
  ; predsuc = {!!}
  }
module Comp = DepAlgebra Comp

comp : (t : I.Z) → ⌜ N.⟦ t ⟧ ⌝ ≡ t
comp i = {!!}
