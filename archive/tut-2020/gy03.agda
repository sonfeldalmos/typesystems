{-# OPTIONS --prop #-}
module gy03 where

open import Lib

module Nat where

  -- module I where
  --   Tm : Set
  --   zero : Tm
  --   suc : Tm → Tm

  --   Tm = ℕ
  --   zero = O
  --   suc = S

  record Algebra {i} : Set (lsuc i) where
    field
      Tm : Set i
      zero : Tm
      suc : Tm → Tm

    ⟦_⟧ : ℕ → Tm
    ⟦ O   ⟧ = zero
    ⟦ S t ⟧ = suc ⟦ t ⟧

  -- How many algebras exists, where terms get interpreted as elements of the
  -- empty set?
  A0 : Algebra
  A0 = record
    { Tm   = ↑p 𝟘
    ; zero = ?
    ; suc  = ?
    }
  module A0 = Algebra A0

  -- How many algebras exists, where terms get interpreted as elements of the
  -- one element set?
  A1 : Algebra
  A1 = record
    { Tm   = ↑p 𝟙
    ; zero = ?
    ; suc  = ?
    }
  module A1 = Algebra A1

  -- Product algebra:
  prod : ∀ {i} → (M N : Algebra {i}) → Algebra
  prod M N = record
    { Tm   = ?
    ; zero = ?
    ; suc  = ?
    }

  -- Dependent algebra structure of natural numbers:
  record DepAlgebra {i} : Set (lsuc i) where
    field
      Tm      : ℕ → Set i
      zero    : Tm O
      suc     : ∀{t} → Tm t → Tm (S t)

    ⟦_⟧ : (t : ℕ) → Tm t
    ⟦ O   ⟧ = zero
    ⟦ S t ⟧ = suc ⟦ t ⟧

  -- Define addition over the natural numbers as terms using an algebra!
  Plus : ℕ → Algebra
  Plus n = record
    { Tm = ℕ
    ; zero = ?
    ; suc = ?
    }
  module Plus (n : ℕ) = Algebra (Plus n)

  -- Helper function for using the algebraic addition:
  plus : ℕ → ℕ → ℕ
  plus a b = Plus.⟦ a ⟧ b

  -- Prove the left and right identity of the plus function defined using this
  -- algebra!

  idr : ℕ → DepAlgebra
  idr n = record
    { Tm   = ?
    ; zero = ?
    ; suc  = ?
    }

  idl : DepAlgebra
  idl = record
    { Tm   = ?
    ; zero = ?
    ; suc  = ?
    }

module NatBoolAST where

  open import NatBoolAST

  -- Define an algebra, in which the interpretation of true and false are equal,
  -- but the interpretation of false and zero are different!
  T≡F≢Z : Algebra
  T≡F≢Z = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module T≡F≢Z = Algebra T≡F≢Z

  T≡F : T≡F≢Z.⟦ I.true ⟧ ≡ T≡F≢Z.⟦ I.false ⟧
  T≡F = refl

  F≢Z : ¬ T≡F≢Z.⟦ I.false ⟧ ≡ T≡F≢Z.⟦ I.zero ⟧
  F≢Z ()

  -- Degenerate Standard Algebra:
  DSA : Algebra
  DSA = record
    { Tm      = ℕ
    ; true    = 1
    ; false   = 0
    ; ite     = λ { (S _) t f → t ; O t f → f }
    ; zero    = 0
    ; suc     = S
    ; isZero  = λ { O → 1 ; (S _) → 0 }
    ; _+_     = _+ℕ_
    }
  module DSA = Algebra DSA

  -- Define an algebra that only returns I if a certain term is I.zero,
  -- otherwise O!
  z? : Algebra
  z? = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module z? = Algebra z?

  -- Define an algebra that optimizes terms by eliminating zero additions from
  -- the left side, such that e.g. `zero + n` becomes `n`!
  -- (Tip: Use the previously defined z? algebra!)
  Opt : Algebra
  Opt = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module Opt = Algebra Opt

  -- Example programs for testing optimization
  p : I.Tm
  p =
    isZero (
      ite
        (isZero (suc zero))
        (zero + zero)
        (ite true zero (suc (suc zero)))
      )
    where open I

  q : I.Tm
  q = ite (isZero (zero + suc zero)) false (isZero zero)
    where open I

  r : I.Tm
  r = (zero + zero) + (zero + (suc zero + zero))
    where open I

  test : I.Tm
  test = {!Opt.⟦ p ⟧!}
    where open I

  -- Prove the correcthess of this optimization over the degenerate standard
  -- algebra using a dependent algebra!

  plusHelper : ∀ {t} {t'} →
    ↑p (DSA.⟦ t ⟧ ≡ DSA.⟦ Opt.⟦ t ⟧ ⟧) →
    ↑p (DSA.⟦ t' ⟧ ≡ DSA.⟦ Opt.⟦ t' ⟧ ⟧) →
    ↑p (DSA.⟦ t I.+ t' ⟧ ≡ DSA.⟦ Opt.⟦ t I.+ t' ⟧ ⟧)
  plusHelper {t} {t'} ↑[ eq1 ]↑ ↑[ eq2 ]↑ = ?

  OptCorrect : DepAlgebra
  OptCorrect = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }

  -- Define an algebra that counts the number of trues and falses!
  T+F : Algebra
  T+F = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module T+F = Algebra T+F

  import Lemmas

  -- Prove that the number of trues is less than or equal to the
  -- sum of trues and falses!
  T≤T+F : DepAlgebra
  T≤T+F = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }

-- Extra task:
-- Improve the optimizer, so it also simplifies from the right.
-- (n + zero = n, not only zero + n = n)
