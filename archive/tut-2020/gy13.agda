{-# OPTIONS --prop --rewriting #-}
module gy13 where

open import Lib renaming (_∘_ to _∘f_ ; _,_ to _,Σ_; _×_ to _⊗_)

module I where
  data Ty      : Set where
    Nat        : Ty           -- <Nat>
    _⇒_        : Ty → Ty → Ty -- <Fun>

  data Con     : Set where
    ∙          : Con
    _▹_        : Con → Ty → Con

  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_ -- <Fun>
  infixl 5 _$_ -- <Fun>

  postulate
    Sub        : Con → Con → Set
    Tm         : Con → Ty → Set

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    -- [ Nat )>
    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])
    -- <( Nat ]

    -- [ Fun )>
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
    -- <( Fun ]

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  -- [ Fun )>
  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  f $ x = app f [ id , x ]
  -- <( Fun ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = cong-2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ cong-2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ cong-2 _,_ (cong (_∘ δ) ▹β₁) (cong (_[ δ ]) ▹β₂)

  -- [ Fun )>
  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = cong (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ cong app lam[]
                ◾ ⇒β
  -- <( Fun ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  {-# REWRITE ass idl idr #-}
  {-# REWRITE ▹β₁ ▹β₂ ▹η ▹η' ,∘ [id] [∘] #-}
  {-# REWRITE Natβ₁ Natβ₂  zero[] suc[] recNat[] #-}         -- <Nat>
  {-# REWRITE ⇒β ⇒η lam[] app[] #-}                          -- <Fun>

record Algebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_
  infixl 5 _$_

  field
    Con        : Set i
    Sub        : Con → Con → Set k
    Ty         : Set j
    Tm         : Con → Ty → Set l

    _∘_        : ∀{Γ Δ Θ} → Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    id         : ∀{Γ} → Sub Γ Γ
    ass        : ∀{Γ Δ Θ Λ}{σ : Sub Θ Λ}{δ : Sub Δ Θ}{ν : Sub Γ Δ} →
                 (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl        : ∀{Γ Δ} {σ : Sub Γ Δ} → id ∘ σ ≡ σ
    idr        : ∀{Γ Δ} {σ : Sub Γ Δ} → σ ∘ id ≡ σ

    _[_]       : ∀{Γ Δ A} → Tm Δ A → Sub Γ Δ → Tm Γ A
    [id]       : ∀{Γ A} {t : Tm Γ A} → t [ id ] ≡ t
    [∘]        : ∀{Γ Δ Θ A} {t : Tm Θ A}{σ : Sub Δ Θ}{δ : Sub Γ Δ} →
                 t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙          : Con
    ε          : ∀{Γ} → Sub Γ ∙
    ∙η         : ∀{Γ} {σ : Sub Γ ∙} → σ ≡ ε

    _▹_        : Con → Ty → Con
    _,_        : ∀{Γ Δ A} → Sub Γ Δ → Tm Γ A → Sub Γ (Δ ▹ A)
    p          : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q          : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → p ∘ (σ , t) ≡ σ
    ▹β₂        : ∀{Γ Δ A} {σ : Sub Γ Δ}{t : Tm Γ A} → q [ σ , t ] ≡ t
    ▹η         : ∀{Γ Δ A} {σ : Sub Γ (Δ ▹ A)} → p ∘ σ , q [ σ ] ≡ σ

    Nat        : Ty
    zero       : ∀{Γ} → Tm Γ Nat
    suc        : ∀{Γ} → Tm Γ Nat → Tm Γ Nat
    recNat     : ∀{Γ A} → Tm Γ A → Tm (Γ ▹ A) A → Tm Γ Nat → Tm Γ A
    Natβ₁      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A} → recNat u v zero ≡ u
    Natβ₂      : ∀{Γ A}{u : Tm Γ A}{v : Tm (Γ ▹ A) A}{t : Tm Γ Nat} → recNat u v (suc t) ≡ v [ id , recNat u v t ]
    zero[]     : ∀{Γ Δ}{σ : Sub Γ Δ} → zero [ σ ] ≡ zero
    suc[]      : ∀{Γ Δ}{n : Tm Δ Nat}{σ : Sub Γ Δ} → (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ Δ A}{u : Tm Δ A}{v : Tm (Δ ▹ A) A}{t : Tm Δ Nat}{σ : Sub Γ Δ} →
                 recNat u v t [ σ ] ≡ recNat (u [ σ ]) (v [ σ ∘ p , q ]) (t [ σ ])

    _⇒_        : Ty → Ty → Ty
    lam        : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    app        : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm (Γ ▹ A) B
    ⇒β         : ∀{Γ A B}{t : Tm (Γ ▹ A) B} → app (lam t) ≡ t
    ⇒η         : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (app t) ≡ t
    lam[]      : ∀{Γ Δ A B}{t : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
                 (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])

  def : ∀ {Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id , t ]

  _$_ : ∀ {Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
  t $ u = app t [ id , u ]

  ▹η' : ∀ {Γ A} → p {Γ}{A} , q ≡ id
  ▹η' = ap2 _,_ idr [id] ⁻¹ ◾ ▹η

  ,∘ : ∀ {Γ Δ Θ A} {σ : Sub Δ Θ}{t : Tm Δ A}{δ : Sub Γ Δ} →
    (σ , t) ∘ δ ≡ σ ∘ δ , t [ δ ]
  ,∘ {δ = δ} = ▹η ⁻¹
              ◾ ap2 _,_ (ass ⁻¹) ([∘] ⁻¹)
              ◾ ap2 _,_ (ap (_∘ δ) ▹β₁) (ap (_[ δ ]) ▹β₂)

  app[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{σ : Sub Γ Δ} →
    app (t [ σ ]) ≡ (app t) [ σ ∘ p , q ]
  app[] {σ = σ} = ap (λ u → app (u [ σ ])) ⇒η ⁻¹
                ◾ ap app lam[]
                ◾ ⇒β

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ∘ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ∘ p ∘ p ]

  def⇒ : ∀ {Γ A B} {t : Tm Γ A}{u : Tm (Γ ▹ A) B} → def t u ≡ lam u $ t
  def⇒ = ap (def _) ⇒β ⁻¹

  ⇒η' : ∀ {Γ A B} {t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
  ⇒η' {t = t} = ap lam
                ( ap (_[ id , q ]) app[]
                ◾ [∘]
                ◾ ap ((app t) [_])
                  ( ,∘
                  ◾ ap2 _,_
                    ( ass
                    ◾ ap (p ∘_) ▹β₁)
                    ( ▹β₂
                    ◾ [id] ⁻¹)
                  ◾ ▹η)
                ◾ [id])
              ◾ ⇒η

  def[] : ∀ {Γ Δ A B} {t : Tm Δ A}{u : Tm (Δ ▹ A) B}{σ : Sub Γ Δ} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ∘ p , q ])
  def[] {t = t}{u}{σ} = rail
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap (_, t [ σ ]) idl))
    ([∘]
      ◾ ap (u [_])
        ( ,∘
        ◾ ap2 _,_
          (ass
            ◾ ap (σ ∘_) ▹β₁
            ◾ idr)
          ▹β₂))
    refl

  $[] : ∀ {Γ Δ A B} {t : Tm Δ (A ⇒ B)}{u : Tm Δ A}{σ : Sub Γ Δ} →
    (t $ u) [ σ ] ≡ t [ σ ] $ u [ σ ]
  $[] = def[] ◾ ap (def _) app[] ⁻¹

  -------------------------------------------
  -- recursor
  -------------------------------------------

  ⟦_⟧T : I.Ty → Ty
  ⟦ I.Nat ⟧T = Nat
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T

  ⟦_⟧C : I.Con → Con
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} → I.Sub Γ Δ → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C
    ⟦_⟧t : ∀ {Γ A} → I.Tm Γ A → Tm ⟦ Γ ⟧C ⟦ A ⟧T

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}

    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}

  ⟦def⟧ : ∀ {Γ A B} {t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl

record DepAlgebra {i j k l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,_
  infixr 5 _⇒_ -- <Fun>
  infixl 5 _$_ -- <Fun>

  field
    Con : I.Con → Set i
    Ty  : I.Ty → Set j
    Sub : ∀ {Γ' Δ'} → Con Γ' → Con Δ' → I.Sub Γ' Δ' → Set k
    Tm  : ∀ {Γ' A'} → Con Γ' → Ty A' → I.Tm Γ' A' → Set l

    _∘_ : ∀ {Γ' Δ' Θ' σ' δ'} {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'} →
      Sub Δ Θ σ' → Sub Γ Δ δ' → Sub Γ Θ (σ' I.∘ δ')
    id : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ Γ I.id
    ass : ∀ {Γ' Δ' Θ' Λ' σ' δ' ν'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{Λ : Con Λ'}
      {σ : Sub Θ Λ σ'}{δ : Sub Δ Θ δ'}{ν : Sub Γ Δ ν'} →
      (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
    idl : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      id ∘ σ ≡ σ
    idr : ∀ {Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
      σ ∘ id ≡ σ

    _[_] : ∀ {Γ' Δ' A' t' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Tm Δ A t' → Sub Γ Δ σ' → Tm Γ A (t' I.[ σ' ])
    [id] : ∀ {Γ' A' t'} {Γ : Con Γ'}{A : Ty A'}{t : Tm Γ A t'} →
      t [ id ] ≡ t
    [∘] : ∀ {Γ' Δ' Θ' A' t' σ' δ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{Θ : Con Θ'}{A : Ty A'}
      {t : Tm Θ A t'}{σ : Sub Δ Θ σ'}{δ : Sub Γ Δ δ'} →
      t [ σ ] [ δ ] ≡ t [ σ ∘ δ ]

    ∙ : Con I.∙
    ε : ∀ {Γ'} {Γ : Con Γ'} → Sub Γ ∙ I.ε
    ∙η : ∀ {Γ' σ'} {Γ : Con Γ'}{σ : Sub Γ ∙ σ'} →
      σ =[ cong (Sub Γ ∙) I.∙η ]= ε

    _▹_ : ∀ {Γ' A'} → Con Γ' → Ty A' → Con (Γ' I.▹ A')
    _,_ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'} →
      Sub Γ Δ σ' → Tm Γ A t' → Sub Γ (Δ ▹ A) (σ' I., t')
    p : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Sub (Γ ▹ A) Γ I.p
    q : ∀ {Γ' A'} {Γ : Con Γ'}{A : Ty A'} → Tm (Γ ▹ A) A I.q
    ▹β₁ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → p ∘ (σ , t) ≡ σ
    ▹β₂ : ∀ {Γ' Δ' A' σ' t'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ Δ σ'}{t : Tm Γ A t'} → q [ σ , t ] ≡ t
    ▹η : ∀ {Γ' Δ' A' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}
      {σ : Sub Γ (Δ ▹ A) σ'} → p ∘ σ , q [ σ ] ≡ σ

    -- [ Nat )>
    Nat        : Ty I.Nat
    zero       : ∀{Γ'}{Γ : Con Γ'} → Tm Γ Nat I.zero
    suc        : ∀{Γ' n'}{Γ : Con Γ'} →
                 Tm Γ Nat n' → Tm Γ Nat (I.suc n')
    recNat     : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'} → Tm Γ A z' → Tm (Γ ▹ A) A s' → Tm Γ Nat t' → Tm Γ A (I.recNat z' s' t')
    Natβ₁      : ∀{Γ' A' z' s'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'} → recNat z s zero ≡ z
    Natβ₂      : ∀{Γ' A' z' s' t'}{Γ : Con Γ'}{A : Ty A'}{z : Tm Γ A z'}{s : Tm (Γ ▹ A) A s'}{t : Tm Γ Nat t'} → recNat z s (suc t) ≡ s [ id , recNat z s t ]
    zero[]     : ∀{Γ' Δ' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{σ : Sub Γ Δ σ'} →
                 zero [ σ ] ≡ zero
    suc[]      : ∀{Γ' Δ' n' σ'} {Γ : Con Γ'}{Δ : Con Δ'}{n : Tm Δ Nat n'}{σ : Sub Γ Δ σ'} →
                 (suc n) [ σ ] ≡ suc (n [ σ ])
    recNat[]   : ∀{Γ' Δ' A' z' s' t' σ'}{Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{z : Tm Δ A z'}{s : Tm (Δ ▹ A) A s'}{t : Tm Δ Nat t'}{σ : Sub Γ Δ σ'} →
                 recNat z s t [ σ ] ≡ recNat (z [ σ ]) (s [ σ ∘ p , q ]) (t [ σ ])
    -- <( Nat ]

    -- [ Fun )>
    _⇒_ : ∀ {A' B'} → Ty A' → Ty B' → Ty (A' I.⇒ B')
    lam : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm (Γ ▹ A) B t' → Tm Γ (A ⇒ B) (I.lam t')
    app : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
      Tm Γ (A ⇒ B) t' → Tm (Γ ▹ A) B (I.app t')
    ⇒β : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Γ ▹ A) B t'} → app (lam t) ≡ t
    ⇒η : ∀ {Γ' A' B' t'} {Γ : Con Γ'}{A : Ty A'}{B : Ty B'}
      {t : Tm Γ (A ⇒ B) t'} → lam (app t) ≡ t
    lam[] : ∀ {Γ' Δ' A' B' t' σ'}
      {Γ : Con Γ'}{Δ : Con Δ'}{A : Ty A'}{B : Ty B'}
      {t : Tm (Δ ▹ A) B t'}{σ : Sub Γ Δ σ'} →
      (lam t) [ σ ] ≡ lam (t [ σ ∘ p , q ])
    -- <( Fun ]

  def : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ A t' → Tm (Γ ▹ A) B u' → Tm Γ B (I.def t' u')
  def t u = u [ id , t ]

  -- [ Fun )>
  _$_ : ∀ {Γ' A' B' t' u'}{Γ : Con Γ'}{A : Ty A'}{B : Ty B'} →
    Tm Γ (A ⇒ B) t' → Tm Γ A u' → Tm Γ B (t' I.$ u')
  t $ u = def u (app t)
  -- <( Fun ]

  -------------------------------------------
  -- eliminator
  -------------------------------------------

  ⟦_⟧T : (A : I.Ty) → Ty A
  ⟦ I.Nat ⟧T = Nat                       -- <Nat>
  ⟦ A I.⇒ B ⟧T = ⟦ A ⟧T ⇒ ⟦ B ⟧T         -- <Fun>

  ⟦_⟧C : (Γ : I.Con) → Con Γ
  ⟦ I.∙ ⟧C = ∙
  ⟦ Γ I.▹ A ⟧C = ⟦ Γ ⟧C ▹ ⟦ A ⟧T

  postulate
    ⟦_⟧S : ∀ {Γ Δ} (σ : I.Sub Γ Δ) → Sub ⟦ Γ ⟧C ⟦ Δ ⟧C σ
    ⟦_⟧t : ∀ {Γ A} (t : I.Tm Γ A) → Tm ⟦ Γ ⟧C ⟦ A ⟧T t

    ⟦∘⟧ : ∀ {Γ Δ Θ} {σ : I.Sub Δ Θ}{δ : I.Sub Γ Δ} →
      ⟦ σ I.∘ δ ⟧S ≡ ⟦ σ ⟧S ∘ ⟦ δ ⟧S
    ⟦id⟧ : ∀ {Γ} → ⟦ I.id {Γ} ⟧S ≡ id
    ⟦ε⟧ : ∀ {Γ} → ⟦ I.ε {Γ} ⟧S ≡ ε
    ⟦,⟧ : ∀ {Γ Δ A} {σ : I.Sub Γ Δ}{t : I.Tm Γ A} →
      ⟦ σ I., t ⟧S ≡ ⟦ σ ⟧S , ⟦ t ⟧t
    ⟦p⟧ : ∀ {Γ A} → ⟦ I.p {Γ}{A} ⟧S ≡ p
    {-# REWRITE ⟦∘⟧ ⟦id⟧ ⟦ε⟧ ⟦,⟧ ⟦p⟧ #-}

    ⟦q⟧ : ∀ {Γ A} → ⟦ I.q {Γ}{A} ⟧t ≡ q
    ⟦[]⟧ : ∀ {Γ Δ A} {t : I.Tm Δ A}{σ : I.Sub Γ Δ} →
      ⟦ t I.[ σ ] ⟧t ≡ ⟦ t ⟧t [ ⟦ σ ⟧S ]
    {-# REWRITE ⟦q⟧ ⟦[]⟧ #-}

    -- [ Nat )>
    ⟦zero⟧ : ∀ {Γ} → ⟦ I.zero {Γ} ⟧t ≡ zero
    ⟦suc⟧ : ∀ {Γ} {n : I.Tm Γ I.Nat} →
      ⟦ I.suc n ⟧t ≡ suc ⟦ n ⟧t
    ⟦recNat⟧ : ∀{Γ A}{u : I.Tm Γ A}{v : I.Tm (Γ I.▹ A) A}{t : I.Tm Γ I.Nat} →
      ⟦ I.recNat u v t ⟧t ≡ recNat ⟦ u ⟧t ⟦ v ⟧t ⟦ t ⟧t
    {-# REWRITE ⟦zero⟧ ⟦suc⟧ ⟦recNat⟧ #-}
    -- <( Nat ]

    -- [ Fun )>
    ⟦lam⟧ : ∀ {Γ A B} {t : I.Tm (Γ I.▹ A) B} →
      ⟦ I.lam t ⟧t ≡ lam ⟦ t ⟧t
    ⟦app⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)} →
      ⟦ I.app t ⟧t ≡ app ⟦ t ⟧t
    {-# REWRITE ⟦lam⟧ ⟦app⟧ #-}
    -- <( Fun ]

  ⟦def⟧ : ∀ {Γ A B}{t : I.Tm Γ A}{u : I.Tm (Γ I.▹ A) B} →
    ⟦ I.def t u ⟧t ≡ def ⟦ t ⟧t ⟦ u ⟧t
  ⟦def⟧ = refl

  -- [ Fun )>
  ⟦$⟧ : ∀ {Γ A B} {t : I.Tm Γ (A I.⇒ B)}{u : I.Tm Γ A} →
    ⟦ t I.$ u ⟧t ≡ ⟦ t ⟧t $ ⟦ u ⟧t
  ⟦$⟧ = refl
  -- <( Fun ]

--

open I

-- [ Nat )>
data PNat : Tm ∙ Nat → Set where
  Pzero : PNat zero
  Psuc : ∀{n'} → PNat n' → PNat (suc n')

PrecNat :
  {A' : Ty}{z' : Tm ∙ A'}{s' : Tm (∙ ▹ A') A'}{t' : Tm ∙ Nat}
  (A : Tm ∙ A' → Set)(z : A z')(s : ∀{w'} → A w' → A (s' [ id , w' ]))
  (t : PNat t') →
  A (recNat z' s' t')
PrecNat {A'}{z'}{s'}{.zero}    A z s Pzero    = z
PrecNat {A'}{z'}{s'}{.(suc _)} A z s (Psuc t) = s (PrecNat A z s t)

PcanNat : ∀{t'} → PNat t' → ↑p (t' ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t' ≡ suc u)
PcanNat Pzero    = ι₁ refl↑
PcanNat (Psuc t) with PcanNat t
... | ι₁ e = ι₂ (zero ,Σ ↑[ cong suc ↓[ e ]↓ ]↑)
... | ι₂ (t' ,Σ e) = ι₂ (suc t' ,Σ ↑[ cong suc ↓[ e ]↓ ]↑)
-- <( Nat ]

Can : DepAlgebra
Can = record
       { Con        = λ Γ' → Sub ∙ Γ' → Set
       ; Sub        = λ Γ Δ σ' → ∀ {ν'} → Γ ν' → Δ (σ' ∘ ν')
       ; Ty         = λ A' → Tm ∙ A' → Set
       ; Tm         = λ Γ A t' → ∀ {ν'} → Γ ν' → A (t' [ ν' ])
       ; _∘_        = λ σ δ x → σ (δ x)
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = λ t σ x → t (σ x)
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = λ _ → ↑p 𝟙
       ; ε          = λ _ → *↑
       ; ∙η         = refl
       ; _▹_        = λ Γ A σ' → Γ (p ∘ σ') ⊗ A (q [ σ' ])
       ; _,_        = λ σ t x → σ x ,Σ t x
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl

       -- [ Nat )>
       ; Nat        = ?
       ; zero       = ?
       ; suc        = ?
       ; recNat     = ?
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
       -- <( Nat ]

       -- [ Fun )>
       ; _⇒_        = ?
       ; lam        = ?
       ; app        = ?
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       -- <( Fun ]

       }
module Can = DepAlgebra Can

-- [ Nat )>
canNat : (t : Tm ∙ Nat) → ↑p (t ≡ zero) ⊎ Σ (Tm ∙ Nat) λ u → ↑p (t ≡ suc u)
canNat t = ?
-- <( Nat ]

-- [ Fun )>
canFun : ∀{A B}(t : Tm ∙ (A ⇒ B)) → Σ (Tm (∙ ▹ A) B) λ u → ↑p (t ≡ lam u)
canFun t = ?
-- <( Fun ]

--

{-

open I

St : Algebra
St = record
       { Con        = Set
       ; Sub        = λ Γ Δ → Γ → Δ
       ; Ty         = Set
       ; Tm         = λ Γ A → Γ → A
       ; _∘_        = _∘f_
       ; id         = idf
       ; ass        = refl
       ; idl        = refl
       ; idr        = refl
       ; _[_]       = _∘f_
       ; [id]       = refl
       ; [∘]        = refl
       ; ∙          = ↑p 𝟙
       ; ε          = const *↑
       ; ∙η         = refl
       ; _▹_        = _⊗_
       ; _,_        = λ σ t γ → σ γ ,Σ t γ
       ; p          = π₁
       ; q          = π₂
       ; ▹β₁        = refl
       ; ▹β₂        = refl
       ; ▹η         = refl
       ; Nat        = ℕ
       ; zero       = const O
       ; suc        = S ∘f_
       ; recNat     = λ z s n γ → rec-ℕ (z γ) (λ n → s (γ ,Σ n)) (n γ)
       ; Natβ₁      = refl
       ; Natβ₂      = refl
       ; zero[]     = refl
       ; suc[]      = refl
       ; recNat[]   = refl
       ; _⇒_        = λ A B → A → B
       ; lam        = λ t γ a → t (γ ,Σ a)
       ; app        = λ { t (γ ,Σ a) → t γ a }
       ; ⇒β         = refl
       ; ⇒η         = refl
       ; lam[]      = refl
       }
open Algebra St using (⟦_⟧T ; ⟦_⟧t)

norm : {A : Ty} → Tm ∙ A → ⟦ A ⟧T
norm t = ⟦ t ⟧t *↑

--
one two three four five : {Γ : Con} → Tm Γ Nat
one   = suc zero
two   = suc one
three = suc two
four  = suc three
five  = suc four

add : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
add = ?

add-test-1 : norm (add $ two $ three) ≡ 5
add-test-1 = refl

add-test-2 : norm (add $ four $ five) ≡ 9
add-test-2 = refl

mult : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat)
mult = ?

-- mult-test : ℕ
-- mult-test = {!norm (mult $ zero $ zero)!}

-- mult-test-1 : norm (mult $ two $ three) ≡ 6
-- mult-test-1 = refl

-- mult-test-2 : norm (mult $ one $ four) ≡ 4
-- mult-test-2 = refl

ite : {Γ : Con} → Tm Γ (Nat ⇒ Nat ⇒ Nat ⇒ Nat)
ite = ?

ite-test-1 : norm (ite $ zero $ two $ three) ≡ 3
ite-test-1 = refl

ite-test-2 : norm (ite $ five $ one $ four) ≡ 1
ite-test-2 = refl

isEven : {Γ : Con} → Tm Γ (Nat ⇒ Nat)
isEven = ?

isEven-test-1 : norm (isEven $ two) ≡ 1
isEven-test-1 = refl

isEven-test-2 : norm (isEven $ three) ≡ 0
isEven-test-2 = refl

-}
