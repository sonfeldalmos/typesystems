{-# OPTIONS --prop #-}
module gy02_b where

open import Lib

-- Bool

module Bool where

  module I where
    data Tm : Set where
      F T : Tm

  record Algebra {i} : Set (lsuc i) where
    field
      Tm : Set i
      F T : Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.F ⟧ = F
    ⟦ I.T ⟧ = T

  Diff : Algebra
  Diff = record
    { Tm = Prop
    ; F = 𝟘
    ; T = 𝟙
    }
  module Diff = Algebra Diff

  T≢F : ¬ (I.T ≡ I.F)
  T≢F eq = ?

-- Nat

module Nat where

  module I where
    data Tm : Set where
      zero : Tm
      suc : Tm → Tm

  record Algebra {i} : Set (lsuc i) where
    field
      Tm : Set i
      zero : Tm
      suc : Tm → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.zero ⟧ = zero
    ⟦ I.suc t ⟧ = suc ⟦ t ⟧

  Xmod5=2 : Algebra
  Xmod5=2 = record
    { Tm = ℕ
    ; zero = ?
    ; suc = ?
    }
  module Xmod5=2 = Algebra Xmod5=2

  a : Xmod5=2.Tm
  a = ⟦ (suc zero) ⟧
    where
      open Xmod5=2 using (⟦_⟧)
      open I

  test-a : a ≡ ?
  test-a = refl

-- NatBoolAST

module NatBoolAST where

  open import NatBoolAST

  program =
    isZero (
      ite
        (isZero (suc zero))
        (zero + zero)
        (ite true zero (suc (suc zero)))
      )
      where open I

  -- isZero
  -- - ite
  --   - isZero
  --     - suc
  --       - zero
  --   - (+)
  --     - zero
  --     - zero
  --   - ite
  --     - true
  --     - zero
  --     - suc
  --       - suc
  --         - zero

  Hp = ⟦ program ⟧
    where open H

  Hex = ⟦ ex ⟧
    where open H

  Tp = ⟦ program ⟧
    where open T

  Tex = ⟦ ex ⟧
    where open T

  -- Exercise 1.8 (page 6)
  -- Define an algebra which gives the number of nodes in a tree,
  -- e.g. zero = 1, suc zero = 2, suc zero + zero = 4.

  N : Algebra
  N = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module N = Algebra N

  module N-test where
    open N using (⟦_⟧)
    open I

    N-test-1 : ⟦ suc (zero + zero) ⟧ ≡ 4
    N-test-1 = refl

    N-test-2 : ⟦ program ⟧ ≡ 14
    N-test-2 = refl

    N-test-3 : ⟦ ex ⟧ ≡ 9
    N-test-3 = refl


  -- Exercise 1.9. Define a NatBool-AST algebra where terms are natural numbers,
  -- booleans are interpreted in the C style: false is 0, true is 1 and for
  -- if-then-else, anything that is not 0 is interpreted as true. We can call
  -- this algebra the degenerate standard algebra.

  DSA : Algebra
  DSA = record
    { Tm      = ?
    ; true    = ?
    ; false   = ?
    ; ite     = ?
    ; zero    = ?
    ; suc     = ?
    ; isZero  = ?
    ; _+_     = ?
    }
  module DSA = Algebra DSA

  module DSA-test where
    open DSA using (⟦_⟧)
    open I

    DSA-test-1 : ⟦ (suc zero) + (suc (zero + zero)) ⟧ ≡ 2
    DSA-test-1 = refl

    DSA-test-2 : ⟦ program ⟧ ≡ 1
    DSA-test-2 = refl

    DSA-test-3 : ⟦ ex ⟧ ≡ 1
    DSA-test-3 = refl

    DSA-test-4 : ⟦ ite zero false (suc (suc (suc zero))) ⟧ ≡ 3
    DSA-test-4 = refl


-- Extra exercise:
-- Given and extended boolean initial algebra with the `and`
-- and `or` operations give an interpretation which
-- evaluates an expression!

module BoolWithOperations where

  module I where
    data Tm : Set where
      F T : Tm
      and or : Tm → Tm → Tm

  record Algebra {i} : Set (lsuc i) where
    field
      Tm : Set i
      F T : Tm
      and or : Tm → Tm → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.F ⟧ = F
    ⟦ I.T ⟧ = T
    ⟦ I.and t₁ t₂ ⟧ = and ⟦ t₁ ⟧ ⟦ t₂ ⟧
    ⟦ I.or t₁ t₂ ⟧ = or ⟦ t₁ ⟧ ⟦ t₂ ⟧

  Eval : Algebra
  Eval = record
    { Tm = 𝟚
    ; F = ?
    ; T = ?
    ; and = ?
    ; or = ?
    }
  open module Eval = Algebra Eval

  test-1 : ⟦ I.and I.T (I.or I.F I.T) ⟧ ≡ I
  test-1 = refl

  test-2 : ⟦ I.or (I.and I.F I.T) I.F ⟧ ≡ O
  test-2 = refl
