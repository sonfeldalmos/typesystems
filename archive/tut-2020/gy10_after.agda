{-# OPTIONS --prop --rewriting #-}
module gy10_after where

open import Lib renaming (_∘_ to _∘f_; _,_ to _,Σ_)
import Lemmas

open import Def

open I

Std : Algebra
Std = record
       { Con      = Set
       ; Ty       = Set
       ; Sub      = λ Γ Δ → (Γ → Δ)
       ; Tm       = λ Γ A → (Γ → A)
       ; ∙        = ↑p 𝟙
       ; _▹_      = λ Γ A → Γ × A
       ; Nat      = ℕ
       ; Bool     = 𝟚
       ; _∘_      = _∘f_ -- λ σ δ γ → σ (δ γ)
       ; id       = λ γ → γ
       ; ε        = λ _ → *↑
       ; _,_      = λ σ t γ → (σ γ) ,Σ (t γ)
       ; p        = π₁ -- λ { (γ ,Σ a) → γ }
       ; q        = π₂ -- λ { (γ ,Σ a) → a }
       ; _[_]     = λ t σ γ → t (σ γ)
       ; zero     = λ _ → O
       ; suc      = λ t γ → S (t γ)
       ; isZero   = λ t γ → O ≟ℕ (t γ)
       ; _+_      = λ a b γ → (a γ +ℕ b γ)
       ; true     = λ _ → I
       ; false    = λ _ → O
       ; ite      = λ b t f γ → if b γ then t γ else f γ
       ; ass      = refl
       ; idl      = refl
       ; idr      = refl
       ; ∙η       = refl
       ; ▹β₁      = refl
       ; ▹β₂      = refl
       ; ▹η       = refl
       ; [id]     = refl
       ; [∘]      = refl
       ; zero[]   = refl
       ; suc[]    = refl
       ; isZero[] = refl
       ; +[]      = refl
       ; true[]   = refl
       ; false[]  = refl
       ; ite[]    = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁      = refl
       ; +β₂      = refl
       ; iteβ₁    = refl
       ; iteβ₂    = refl
       }
module St = Algebra St

normBool : Tm ∙ Bool → 𝟚
normBool t = St.⟦ t ⟧t *↑

normNat : Tm ∙ Nat → ℕ
normNat t = St.⟦ t ⟧t *↑

norm : {A : Ty} → Tm ∙ A → St.⟦ A ⟧T
norm t = St.⟦ t ⟧t *↑

-- norm-test = {!norm (def zero (def (suc v0) (ite (isZero v0) v1 (suc v1 + suc v0))))!}
-- norm-test-2 = {!!}

--

-- VarDiff : Algebra
-- VarDiff = record
--        { Con      = {!!}
--        ; Ty       = {!!}
--        ; Sub      = {!!}
--        ; Tm       = {!!}
--        ; ∙        = {!!}
--        ; _▹_      = {!!}
--        ; Nat      = {!!}
--        ; Bool     = {!!}
--        ; _∘_      = {!!}
--        ; id       = {!!}
--        ; ε        = {!!}
--        ; _,_      = {!!}
--        ; p        = {!!}
--        ; q        = {!!}
--        ; _[_]     = {!!}
--        ; zero     = {!!}
--        ; suc      = {!!}
--        ; isZero   = {!!}
--        ; _+_      = {!!}
--        ; true     = {!!}
--        ; false    = {!!}
--        ; ite      = {!!}
--        ; ass      = {!!}
--        ; idl      = {!!}
--        ; idr      = {!!}
--        ; ∙η       = {!!}
--        ; ▹β₁      = {!!}
--        ; ▹β₂      = {!!}
--        ; ▹η       = {!!}
--        ; [id]     = {!!}
--        ; [∘]      = {!!}
--        ; zero[]   = {!!}
--        ; suc[]    = {!!}
--        ; isZero[] = {!!}
--        ; +[]      = {!!}
--        ; true[]   = {!!}
--        ; false[]  = {!!}
--        ; ite[]    = {!!}
--        ; isZeroβ₁ = {!!}
--        ; isZeroβ₂ = {!!}
--        ; +β₁      = {!!}
--        ; +β₂      = {!!}
--        ; iteβ₁    = {!!}
--        ; iteβ₂    = {!!}
--        }
-- module VarDiff = Algebra VarDiff

O≢I : ¬ O ≡ I
O≢I ()

v0≢v1 : ¬ v0 ≡ v1
v0≢v1 eq = O≢I (
  O
    ≡⟨ refl ⟩
  norm (def true (def false v0))
    ≡⟨ cong (λ x → norm (def true (def false x))) eq ⟩
  norm (def true (def false v1))
    ≡⟨ refl ⟩
  I
    ∎
  )

--

ConDiff : Algebra -- Counts the length of contexts
ConDiff = record
       { Con      = ℕ
       ; Ty       = ↑p 𝟙
       ; Sub      = λ _ _ → ↑p 𝟙
       ; Tm       = λ _ _ → ↑p 𝟙
       ; ∙        = O
       ; _▹_      = λ Γ _ → S Γ
       ; Nat      = *↑
       ; Bool     = *↑
       ; _∘_      = λ _ _ → *↑
       ; id       = *↑
       ; ε        = *↑
       ; _,_      = λ _ _ → *↑
       ; p        = *↑
       ; q        = *↑
       ; _[_]     = λ _ _ → *↑
       ; zero     = *↑
       ; suc      = λ _ → *↑
       ; isZero   = λ _ → *↑
       ; _+_      = λ _ _ → *↑
       ; true     = *↑
       ; false    = *↑
       ; ite      = λ _ _ _ → *↑
       ; ass      = refl
       ; idl      = refl
       ; idr      = refl
       ; ∙η       = refl
       ; ▹β₁      = refl
       ; ▹β₂      = refl
       ; ▹η       = refl
       ; [id]     = refl
       ; [∘]      = refl
       ; zero[]   = refl
       ; suc[]    = refl
       ; isZero[] = refl
       ; +[]      = refl
       ; true[]   = refl
       ; false[]  = refl
       ; ite[]    = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁      = refl
       ; +β₂      = refl
       ; iteβ₁    = refl
       ; iteβ₂    = refl
       }
module ConDiff = Algebra ConDiff

0≢S : {n : ℕ} → (¬ 0 ≡ S n)
0≢S ()

∙≢▹ : {Γ : Con} → {A : Ty} → (¬ ∙ ≡ (Γ ▹ A))
∙≢▹ {Γ} {A} eq = 0≢S (
  O
    ≡⟨ refl ⟩
  ConDiff.⟦ ∙ ⟧C
    ≡⟨ cong ConDiff.⟦_⟧C eq ⟩
  ConDiff.⟦ Γ ▹ A ⟧C
    ≡⟨ refl ⟩
  S ConDiff.⟦ Γ ⟧C
    ∎
  )

--

ConEq : Algebra
ConEq = record
       { Con      = ↑p 𝟙
       ; Ty       = ↑p 𝟙
       ; Sub      = λ _ _ → ↑p 𝟙
       ; Tm       = λ _ _ → ↑p 𝟙
       ; ∙        = *↑
       ; _▹_      = λ _ _ → *↑
       ; Nat      = *↑
       ; Bool     = *↑
       ; _∘_      = λ _ _ → *↑
       ; id       = *↑
       ; ε        = *↑
       ; _,_      = λ _ _ → *↑
       ; p        = *↑
       ; q        = *↑
       ; _[_]     = λ _ _ → *↑
       ; zero     = *↑
       ; suc      = λ _ → *↑
       ; isZero   = λ _ → *↑
       ; _+_      = λ _ _ → *↑
       ; true     = *↑
       ; false    = *↑
       ; ite      = λ _ _ _ → *↑
       ; ass      = refl
       ; idl      = refl
       ; idr      = refl
       ; ∙η       = refl
       ; ▹β₁      = refl
       ; ▹β₂      = refl
       ; ▹η       = refl
       ; [id]     = refl
       ; [∘]      = refl
       ; zero[]   = refl
       ; suc[]    = refl
       ; isZero[] = refl
       ; +[]      = refl
       ; true[]   = refl
       ; false[]  = refl
       ; ite[]    = refl
       ; isZeroβ₁ = refl
       ; isZeroβ₂ = refl
       ; +β₁      = refl
       ; +β₂      = refl
       ; iteβ₁    = refl
       ; iteβ₂    = refl
       }
module ConEq = Algebra ConEq

∙≡▹ : {Γ : Con} → {A : Ty} → (ConEq.⟦ ∙ ⟧C ≡ ConEq.⟦ (Γ ▹ A) ⟧C)
∙≡▹ = refl

--

SUC-INJ : Prop
SUC-INJ = {t t' : Tm ∙ Nat} → suc t ≡ suc t' → t ≡ t'

suc-inj : SUC-INJ
suc-inj {t} {t'} eq =
  t
    ≡⟨ completenessₜ ⁻¹ ⟩
  ⌜ normₜ t ⌝ₜ
    ≡⟨ cong ⌜_⌝ₜ (Lemmas.S⁻¹-≡ (cong normₜ eq)) ⟩
  ⌜ normₜ t' ⌝ₜ
    ≡⟨ completenessₜ ⟩
  t'
    ∎

--

ISZERO-INJ : Prop
ISZERO-INJ = {t t' : Tm ∙ Nat} → isZero t ≡ isZero t' → t ≡ t'

1≢2 : ¬ 1 ≡ 2
1≢2 ()

one-neq-two : ¬ (suc zero) ≡ (suc (suc zero))
one-neq-two eq = 1≢2 (cong normₜ eq)

not-isZero-inj : ¬ ISZERO-INJ
not-isZero-inj isZero-inj = one-neq-two (isZero-inj refl)

--

decEqℕ : (m n : ℕ) → (m ≡ n ⊎p (¬ m ≡ n))
decEqℕ O O = ι₁ refl
decEqℕ O (S n) = ι₂ (λ ())
decEqℕ (S m) O = ι₂ (λ ())
decEqℕ (S m) (S n) with decEqℕ m n
... | ι₁ eq = ι₁ (cong S eq)
... | ι₂ neq = ι₂ λ Seq → neq (Lemmas.S⁻¹-≡ Seq)

decEq𝟚 : (a b : 𝟚) → (a ≡ b ⊎p (¬ a ≡ b))
decEq𝟚 O O = ι₁ refl
decEq𝟚 O I = ι₂ (λ ())
decEq𝟚 I O = ι₂ (λ ())
decEq𝟚 I I = ι₁ refl

decEqTm : {A : Ty} → (t t' : Tm ∙ A) → (t ≡ t' ⊎p (¬ t ≡ t'))
decEqTm {Nat} t t' with decEqℕ (normₜ t) (normₜ t')
... | ι₁ eq = ι₁ (
  t
    ≡⟨ completenessₜ ⁻¹ ⟩
  ⌜ normₜ t ⌝ₜ
    ≡⟨ cong ⌜_⌝ₜ eq ⟩
  ⌜ normₜ t' ⌝ₜ
    ≡⟨ completenessₜ ⟩
  t'
    ∎
  )
... | ι₂ neq = ι₂ λ eq → neq (cong normₜ eq)
decEqTm {Bool} t t' with decEq𝟚 (normₜ t) (normₜ t')
... | ι₁ eq = ι₁ (
  t
    ≡⟨ completenessₜ ⁻¹ ⟩
  ⌜ normₜ t ⌝ₜ
    ≡⟨ cong ⌜_⌝ₜ eq ⟩
  ⌜ normₜ t' ⌝ₜ
    ≡⟨ completenessₜ ⟩
  t'
    ∎
  )
... | ι₂ neq = ι₂ λ eq → neq (cong normₜ eq)
