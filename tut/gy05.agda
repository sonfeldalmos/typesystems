{-# OPTIONS --prop --rewriting #-}
module gy05 where

open import Lib

module NatBoolWT where
  import NatBoolAST
  open import NatBoolWT
  open I

  -- Type inference

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : NatBoolAST.Model {i = lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A))
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     = {!   !}
    ; num     = λ x → Ok (Nat , num x)
    ; isZero  = λ where
                    (Ok (Nat , tm)) → Ok (Bool , isZero tm)
                    (Ok (Bool , tm)) → Err 3
                    (Err e) → Err e
    ; _+o_    = {!   !}
    }

  module INF = NatBoolAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ isZero false ⟧  !}
    where open NatBoolAST.I

  -- Standard model

  Standard : Model {i = lsuc lzero} {j = lzero}
  Standard = record
    { Ty      = {!   !}
    ; Tm      = {!   !}
    ; Nat     = {!   !}
    ; Bool    = {!   !}
    ; true    = {!   !}
    ; false   = {!   !}
    ; ite     = {!   !}
    ; num     = {!   !}
    ; isZero  = {!   !}
    ; _+o_    = {!   !}
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → {!   !}
  eval = {!   !}

  run : (t : NatBoolAST.I.Tm) → {!   !}
  run t = {!   !}


module NatBool where

  module I where

    infixl 7 _+o_

    data Ty     : Set where
      Nat       : Ty
      Bool      : Ty

    postulate
      Tm        : Ty → Set
      true      : Tm Bool
      false     : Tm Bool
      ite       : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
      num       : ℕ → Tm Nat
      isZero    : Tm Nat → Tm Bool
      _+o_      : Tm Nat → Tm Nat → Tm Nat
      iteβ₁     : ∀{A}{u v : Tm A} → ite true u v ≡ u
      iteβ₂     : ∀{A}{u v : Tm A} → ite false u v ≡ v
      isZeroβ₁  : isZero (num 0) ≡ true
      isZeroβ₂  : isZero (num (1 + n)) ≡ false
      +β        : num m +o num n ≡ num (m + n)

  open I

  -- Equalities

  eq-0 : true ≡ true
  eq-0 = {!   !}

  eq-1 : isZero (num 0) ≡ true
  eq-1 = {!   !}

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = {!   !}

  eq-3 : isZero (num 4) ≡ false
  eq-3 = {!   !}

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = {!   !}

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' =
    isZero (num 3 +o num 1)
      ≡⟨ {!   !} ⟩ -- \== \< \>
    {!   !}
      ≡⟨ {!   !} ⟩
    false
      ∎ -- \qed

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = {!   !}

  eq-6 : ite true (isZero (num 0)) false ≡ true
  eq-6 = {!   !}

  eq-6' : ite true (isZero (num 0)) false ≡ true
  eq-6' =
    ite true (isZero (num 0)) false
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-7 : num 3 +o num 0 +o num 1 ≡ num 4
  eq-7 =
    (num 3 +o num 0 +o num 1)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 4
      ∎

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 2
      ∎

  eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  eq-9 =
    (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 3
      ∎

  eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  eq-10 =
    (ite false (num 1 +o num 1) (num 0) +o num 0)
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    num 0
      ∎

  eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  eq-11 =
    ite (isZero (num 0 +o num 1)) false (isZero (num 0))
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    true
      ∎

  eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  eq-12 =
    num 3 +o num 2
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    ite true (num 5) (num 1)
      ∎