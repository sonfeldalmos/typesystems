{-# OPTIONS --prop --rewriting #-}
module gy01_after where

infix 4 _≡_
postulate
  _≡_  : ∀{i}{A : Set i}(x : A) → A → Prop i
  refl : ∀{i}{A : Set i}{x : A} → x ≡ x
  cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A}(a= : a ≡ a') → f a ≡ f a'
{-# BUILTIN REWRITE _≡_ #-}

-- Booleans

data 𝟚 : Set where
  tt ff : 𝟚

example-bool : 𝟚
example-bool = tt

another-bool : 𝟚
another-bool = ff

-- idb

idb : 𝟚 → 𝟚
idb x = x

idb-test-1 : idb tt ≡ tt
idb-test-1 = refl

idb-test-2 : idb ff ≡ ff
idb-test-2 = refl

-- neg

neg : 𝟚 → 𝟚
neg tt = ff
neg ff = tt

neg-test-1 : neg tt ≡ ff
neg-test-1 = refl

neg-test-2 : neg ff ≡ tt
neg-test-2 = refl

-- How many 𝟚 → 𝟚 functions are there?

-- or

or : 𝟚 → 𝟚 → 𝟚
or tt _ = tt
or ff b = b

or-test-1 : or tt tt ≡ tt
or-test-1 = refl

or-test-2 : or tt ff ≡ tt
or-test-2 = refl

or-test-3 : or ff tt ≡ tt
or-test-3 = refl

or-test-4 : or ff ff ≡ ff
or-test-4 = refl

-- and

and : 𝟚 → 𝟚 → 𝟚
and tt b = b
and ff _ = ff

and-test-1 : and tt tt ≡ tt
and-test-1 = refl

and-test-2 : and tt ff ≡ ff
and-test-2 = refl

and-test-3 : and ff tt ≡ ff
and-test-3 = refl

and-test-4 : and ff ff ≡ ff
and-test-4 = refl

-- xor

xor : 𝟚 → 𝟚 → 𝟚
xor = λ a b → or (and a (neg b)) (and (neg a) b)

xor-test-1 : xor tt tt ≡ ff
xor-test-1 = refl

xor-test-2 : xor tt ff ≡ tt
xor-test-2 = refl

xor-test-3 : xor ff tt ≡ tt
xor-test-3 = refl

xor-test-4 : xor ff ff ≡ ff
xor-test-4 = refl

-- Natural numbers

data ℕ : Set where
  zero : ℕ
  suc : ℕ → ℕ

three : ℕ
three = suc (suc (suc zero))

{-# BUILTIN NATURAL ℕ #-}

addTwo : ℕ → ℕ
addTwo n = suc (suc n)

addTwo-test-1 : addTwo 0 ≡ 2
addTwo-test-1 = refl

addTwo-test-2 : addTwo 3 ≡ 5
addTwo-test-2 = refl

_*2+1 : ℕ → ℕ
zero *2+1 = 1
suc n *2+1 = suc (suc (n *2+1))
-- What we have:    n    * 2 + 1 = 2n     + 1
-- What we need: (n + 1) * 2 + 1 = 2n + 2 + 1

*2+1-test-1 : 3 *2+1 ≡ 7
*2+1-test-1 = refl

plus : ℕ → ℕ → ℕ
plus zero b = b
plus (suc a) b = suc (plus a b)
-- plus (suc a) b = plus a (suc b)

plus-idl : (n : ℕ) → plus 0 n ≡ n
plus-idl n = refl

plus-idr : (n : ℕ) → plus n 0 ≡ n
plus-idr n = ?
