{-# OPTIONS --prop --rewriting #-}
module gy07 where

open import Lib

module IntOperations where

  open import Int using (Model ; ℤ)
  open Int.I

  +One +Two +Three -One -Two -Three : ℤ

  +One   = Suc Zero
  +Two   = Suc +One
  +Three = Suc +Two

  -One   = Pred Zero
  -Two   = Pred -One
  -Three = Pred -Two

  Plus : ℤ → Model {ℓ = lzero}
  Plus a = record
    { Z       = {!   !}
    ; Zero    = {!   !}
    ; Suc     = {!   !}
    ; Pred    = {!   !}
    ; SucPred = {!   !}
    ; PredSuc = {!   !}
    }

  _+ℤ_ : ℤ → ℤ → ℤ
  a +ℤ b = ⟦ b ⟧ where open Model (Plus a)

  +ℤ-test = {! +Two +ℤ +One !}

  Minus : ℤ → Model {ℓ = lzero}
  Minus a = record
    { Z       = {!   !}
    ; Zero    = {!   !}
    ; Suc     = {!   !}
    ; Pred    = {!   !}
    ; SucPred = {!   !}
    ; PredSuc = {!   !}
    }

  _-ℤ_ : ℤ → ℤ → ℤ
  a -ℤ b = ⟦ b ⟧ where open Model (Minus a)

  -ℤ-test = {! -Three -ℤ -One  !}


module DefWT where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty

    data Con : Set where
      ∙ : Con
      _▹_ : Con → Ty → Con

    infixl 5 _▹_

    data Var : Con → Ty → Set where
      vz : ∀{Γ A} → Var (Γ ▹ A) A
      vs : ∀{Γ A B} → Var Γ A → Var (Γ ▹ B) A

    data Tm (Γ : Con) : Ty → Set where
      var     : ∀{A} → Var Γ A → Tm Γ A
      def     : ∀{A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B

      true    : Tm Γ Bool
      false   : Tm Γ Bool
      ite     : ∀{A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
      num     : ℕ → Tm Γ Nat
      isZero  : Tm Γ Nat → Tm Γ Bool
      _+o_    : Tm Γ Nat → Tm Γ Nat → Tm Γ Nat

    v0 : {Γ : Con}{A : Ty}        → Tm (Γ ▹ A) A
    v0 = var vz
    v1 : {Γ : Con}{A B : Ty}      → Tm (Γ ▹ A ▹ B) A
    v1 = var (vs vz)
    v2 : {Γ : Con}{A B C : Ty}    → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = var (vs (vs vz))
    v3 : {Γ : Con}{A B C D : Ty}  → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = var (vs (vs (vs vz)))

  open I

  -- ∙ = \.
  -- ▹ = \tw + right arrow key

  tm-0 : Tm {!   !} {!   !}
  tm-0 = isZero (ite v2 (v1 +o v0) v1)

  tm-1 : Tm {!   !} {!   !}
  tm-1 = def (v1 +o num 5) (ite (isZero v0) v2 (num 0))

  tm-2 : Tm (∙ ▹ Nat ▹ Bool ▹ Nat) Bool
  tm-2 = {!   !}

  tm-3 : {A : Ty} → Tm (∙ ▹ Bool ▹ A ▹ Nat) A
  tm-3 = {!   !}


module Def where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty

    data Con  : Set where
      ∙       : Con
      _▹_     : Con → Ty → Con

    infixl 5 _▹_
    infixl 6 _⊚_
    infixl 5 _,o_
    infixl 6 _[_]

    postulate
      Sub       : Con → Con → Set
      _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id        : ∀{Γ} → Sub Γ Γ
      idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ε         : ∀{Γ} → Sub Γ ∙
      ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Tm        : Con → Ty → Set
      _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q         : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      true      : ∀{Γ} → Tm Γ Bool
      false     : ∀{Γ} → Tm Γ Bool
      ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
      iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
      iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
      true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
      false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
      ite[]     : ∀{Γ Δ A t u v}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

      num       : ∀{Γ} → ℕ → Tm Γ Nat
      isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
      _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
      isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
      isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
      +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
      num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
      isZero[]  : ∀{Γ Δ t}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
      +[]       : ∀{Γ Δ u v}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
    v0 = q
    v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

  open I

  module Examples where
    con-1 : Con
    con-1 = {!   !}

    sub-1 : Sub ∙ con-1
    sub-1 = {!   !}

    con-2 : Con
    con-2 = {!   !}

    sub-2 : Sub con-1 con-2
    sub-2 = {!   !}

    sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
    sub-swap = {!   !}

  var-test-1 : def {Γ = ∙} true v0 ≡ true
  var-test-1 =
    def true v0
      ≡⟨ {!   !} ⟩
    true
      ∎

  var-test-2 : def {Γ = ∙} true (def false v0) ≡ false
  var-test-2 =
    def true (def false v0)
      ≡⟨ {!   !} ⟩
    false
      ∎

  var-test-3 : def {Γ = ∙} true (def false v1) ≡ true
  var-test-3 =
    def true (def false v1)
      ≡⟨ {!   !} ⟩
    true
      ∎

  -- Task

  task-sub : Sub ∙ (∙ ▹ Bool ▹ Nat)
  task-sub = {!   !}

  task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
  task-eq = {!   !}