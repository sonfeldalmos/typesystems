{-# OPTIONS --prop --rewriting #-}
module gy05_after where

open import Lib

module NatBoolWT where
  import NatBoolAST
  open import NatBoolWT
  open I

  -- Type inference

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : NatBoolAST.Model {i = lzero}
  Infer = record
    { Tm      = Result (Σ Ty (λ A → Tm A))
    ; true    = Ok (Bool , true)
    ; false   = Ok (Bool , false)
    ; ite     = λ where (Ok (Bool , co)) (Ok (Nat  , tr)) (Ok (Nat   , fa)) → Ok (Nat , ite co tr fa)
                        (Ok (Bool , _ )) (Ok (Nat  , _ )) (Ok (Bool  , _ )) → Err 2
                        (Ok (Bool , _ )) (Ok (Bool , _ )) (Ok (Nat   , _ )) → Err 2
                        (Ok (Bool , co)) (Ok (Bool , tr)) (Ok (Bool  , fa)) → Ok (Bool , (ite co tr fa))
                        (Ok (Nat  , _ )) (Ok r)           (Ok r₁)           → Err 1
                        (Ok r)           (Ok r₁)          (Err e)           → Err e
                        (Ok r)           (Err e)          _                 → Err e
                        (Err e)          _                _                 → Err e
    ; num     = λ x → Ok (Nat , num x)
    ; isZero  = λ where (Ok (Nat  , tm)) → Ok (Bool , isZero tm)
                        (Ok (Bool , tm)) → Err 3
                        (Err e)          → Err e
    ; _+o_    = λ where (Ok (Nat  , l)) (Ok (Nat  , r)) → Ok (Nat , (l +o r))
                        (Ok (Nat  , _)) (Ok (Bool , _)) → Err 4
                        (Ok (Bool , _)) (Ok (_    , _)) → Err 4
                        (Ok r)          (Err e)         → Err e
                        (Err e)         _               → Err e
    }

  module INF = NatBoolAST.Model Infer

  -- inf-test : Result (Σ Ty (λ T → Tm T))
  -- inf-test = {! INF.⟦ ite (isZero (num 0)) false (num 3 +o num 2) ⟧  !}
  --   where open NatBoolAST.I

  -- Standard model

  Standard : Model {i = lsuc lzero} {j = lzero}
  Standard = record
    { Ty      = Set
    ; Tm      = λ ty → ty
    ; Nat     = ℕ
    ; Bool    = 𝟚
    ; true    = tt
    ; false   = ff
    ; ite     = if_then_else_
    ; num     = λ n → n
    ; isZero  = λ where zero → tt
                        (suc n) → ff
    ; _+o_    = _+_
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → STD.⟦ A ⟧T
  eval t = STD.⟦ t ⟧t

  getType : Result (Σ Ty (λ A → Tm A)) → Set
  getType (Ok (ty , _)) = STD.⟦ ty ⟧T
  getType (Err e) = Lift ⊥

  run : (t : NatBoolAST.I.Tm) → Result (getType (INF.⟦ t ⟧))
  run t with INF.⟦ t ⟧
  ... | Ok (_ , tm) = Ok (STD.⟦ tm ⟧t)
  ... | Err e = Err e

  -- run-test = {! run (ite (isZero (num 0 +o num 1)) (num 5) (num 2))  !}
  --   where open NatBoolAST.I

module NatBool where

  module I where

    infixl 7 _+o_

    data Ty     : Set where
      Nat       : Ty
      Bool      : Ty

    postulate
      Tm        : Ty → Set
      true      : Tm Bool
      false     : Tm Bool
      ite       : ∀ {A} → Tm Bool → Tm A → Tm A → Tm A
      num       : ℕ → Tm Nat
      isZero    : Tm Nat → Tm Bool
      _+o_      : Tm Nat → Tm Nat → Tm Nat
      iteβ₁     : ∀{A}{u v : Tm A} → ite true u v ≡ u
      iteβ₂     : ∀{A}{u v : Tm A} → ite false u v ≡ v
      isZeroβ₁  : isZero (num 0) ≡ true
      isZeroβ₂  : isZero (num (1 + n)) ≡ false
      +β        : num m +o num n ≡ num (m + n)

  open I

  -- Equalities

  eq-0 : true ≡ true
  eq-0 = true ∎ -- \qed -- refl {x = true}

  eq-1 : isZero (num 0) ≡ true
  eq-1 = isZeroβ₁

  eq-2 : isZero (num 3 +o num 1) ≡ isZero (num 4)
  eq-2 = cong (λ t → isZero t) +β --\Gb

  eq-3 : isZero (num 4) ≡ false
  eq-3 = isZeroβ₂

  eq-4 : isZero (num 3 +o num 1) ≡ false
  eq-4 = trans {a' = isZero (num 4)} eq-2 eq-3

  eq-4' : isZero (num 3 +o num 1) ≡ false
  eq-4' =
    isZero (num 3 +o num 1)
      -- ≡⟨ cong isZero +β ⟩ -- \== \< \>
      ≡⟨ eq-2 ⟩ -- \== \< \>
    isZero (num 4)
      ≡⟨ eq-3 ⟩
    false
      ∎ -- \qed

  eq-5 : ite false (num 2) (num 5) ≡ num 5
  eq-5 = iteβ₂

  eq-6 : ite true (isZero (num 0)) false ≡ true
  -- eq-6 = trans {a' = ite true true false} (cong (λ t → ite true t false) isZeroβ₁) iteβ₁
  eq-6 = trans {a' = isZero (num 0)} iteβ₁ isZeroβ₁

  eq-6' : ite true (isZero (num 0)) false ≡ true
  eq-6' =
    ite true (isZero (num 0)) false
      ≡⟨ iteβ₁ ⟩
    isZero (num 0)
      ≡⟨ isZeroβ₁ ⟩
    true
      ∎

  eq-7 : num 3 +o num 0 +o num 1 ≡ num 4
  eq-7 =
    ((num 3 +o num 0) +o num 1)
      -- ≡⟨ cong (λ t → t +o num 1) +β ⟩
      ≡⟨ cong (_+o num 1) +β ⟩
    (num 3 +o num 1)
      ≡⟨ +β ⟩
    num 4
      ∎

  eq-8 : ite (isZero (num 0)) (num 1 +o num 1) (num 0) ≡ num 2
  eq-8 =
    ite (isZero (num 0)) (num 1 +o num 1) (num 0)
      ≡⟨ cong (λ t → ite t (num 1 +o num 1) (num 0)) isZeroβ₁ ⟩
    ite true (num 1 +o num 1) (num 0)
      ≡⟨ iteβ₁ ⟩
    (num 1 +o num 1)
      ≡⟨ +β ⟩
    num 2
      ∎

  -- eq-9 : num 3 +o ite (isZero (num 2)) (num 1) (num 0) ≡ num 3
  -- eq-9 =
  --   (num 3 +o ite (isZero (num 2)) (num 1) (num 0))
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   num 3
  --     ∎

  -- eq-10 : ite false (num 1 +o num 1) (num 0) +o num 0 ≡ num 0
  -- eq-10 =
  --   (ite false (num 1 +o num 1) (num 0) +o num 0)
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   num 0
  --     ∎

  -- eq-11 : ite (isZero (num 0 +o num 1)) false (isZero (num 0)) ≡ true
  -- eq-11 =
  --   ite (isZero (num 0 +o num 1)) false (isZero (num 0))
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   true
  --     ∎

  -- eq-12 : num 3 +o num 2 ≡ ite true (num 5) (num 1)
  -- eq-12 =
  --   num 3 +o num 2
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   ite true (num 5) (num 1)
  --     ∎
