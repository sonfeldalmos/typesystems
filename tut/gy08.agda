{-# OPTIONS --prop --rewriting #-}
module gy08 where

open import Lib

module Def where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty

    data Con  : Set where
      ∙       : Con
      _▹_     : Con → Ty → Con

    infixl 7 _+o_
    infixl 5 _▹_
    infixl 6 _⊚_
    infixl 5 _,o_
    infixl 6 _[_]

    postulate
      Sub       : Con → Con → Set
      _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id        : ∀{Γ} → Sub Γ Γ
      idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ε         : ∀{Γ} → Sub Γ ∙
      ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Tm        : Con → Ty → Set
      _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q         : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      true      : ∀{Γ} → Tm Γ Bool
      false     : ∀{Γ} → Tm Γ Bool
      ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
      iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
      iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
      true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
      false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
      ite[]     : ∀{Γ Δ A t u v}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

      num       : ∀{Γ} → ℕ → Tm Γ Nat
      isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
      _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
      isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
      isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
      +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
      num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
      isZero[]  : ∀{Γ Δ t}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
      +[]       : ∀{Γ Δ u v}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
    v0 = q
    v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

  open I

  con-1 : Con
  con-1 = ∙ ▹ Bool

  tm-1 : Tm con-1 Nat
  tm-1 = ite v0 (num 1) (num 0)

  sub-1 : Sub ∙ con-1
  sub-1 = ε ,o true

  -- eq-0 : isZero (num 3 +o num 1) ≡ false {∙}
  -- eq-0 =
  --   isZero (num 3 +o num 1)
  --     ≡⟨ {!   !} ⟩
  --   false
  --     ∎

  tm-1-sub-1 : tm-1 [ sub-1 ] ≡ num 1
  tm-1-sub-1 =
    (tm-1 [ sub-1 ])
      ≡⟨ {!   !} ⟩
    num 1
      ∎

  con-2 : Con
  con-2 = {!   !}

  sub-2 : Sub con-1 con-2
  sub-2 = {!   !}

  tm-2 : Tm con-2 {!   !}
  tm-2 = {!   !}

  tm-2-sub-2 : tm-2 [ sub-2 ] ≡ {!   !}
  tm-2-sub-2 =
    (tm-2 [ sub-2 ])
      ≡⟨ {!   !} ⟩
    {!   !}
      ∎


  -- Equalities with variables

  var-eq-1 : def true v0 ≡ true {∙}
  var-eq-1 =
    def true v0
      ≡⟨ {!   !} ⟩
    true
      ∎

  var-eq-2 : def true (def false v0) ≡ false {∙}
  var-eq-2 =
    def true (def false v0)
      ≡⟨ {!   !} ⟩
    false
      ∎

  var-eq-3 : def true (def false v1) ≡ true {∙}
  var-eq-3 =
    def true (def false v1)
      ≡⟨ {!   !} ⟩
    true
      ∎

  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {∙} 2
  var-eq-4 =
    def (num 1) (v0 +o v0)
      ≡⟨ {!   !} ⟩
    num 2
      ∎

  var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {∙} 3
  var-eq-5 =
    def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
      ≡⟨ {!   !} ⟩
    num 3
      ∎

  var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {∙}
  var-eq-6 =
    def (num 0) (def (isZero v0) (ite v0 false true))
      ≡⟨ {!   !} ⟩
    false
      ∎

  var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {∙} 2
  var-eq-7 =
    def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
      ≡⟨ {!   !} ⟩
    num 2
      ∎

  var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {∙}
  var-eq-8 =
    def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
      ≡⟨ {!   !} ⟩
    false
      ∎

  var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {∙} 2
  var-eq-9 =
    def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
      ≡⟨ {!   !} ⟩
    num 2
      ∎


  -- Task

  task-sub : Sub ∙ (∙ ▹ Bool ▹ Nat)
  task-sub = {!   !}

  task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
  task-eq = {!   !}


  -- Equalities with substitutions

  sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  sub-swap = {!   !}

  sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
  sub-eq-1 = {!  !}

  sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  sub-eq-2 = {!  !}

  sub-eq-3 :
    {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
    (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
    (σ ,o t) ⊚ δ
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    (σ ⊚ δ ,o t [ δ ])
      ∎

  sub-eq-4 :
    {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
    (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  sub-eq-4 {σ = σ} {t = t} {u = u} =
    (def t u) [ σ ]
      ≡⟨ {!   !} ⟩
    {!   !}
      ≡⟨ {!   !} ⟩
    def (t [ σ ]) (u [ σ ⊚ p ,o q ])
      ∎