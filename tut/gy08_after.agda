{-# OPTIONS --prop --rewriting #-}
module gy08_after where

open import Lib

module Def where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty

    data Con  : Set where
      ∙       : Con
      _▹_     : Con → Ty → Con

    infixl 7 _+o_
    infixl 5 _▹_
    infixl 6 _⊚_
    infixl 5 _,o_
    infixl 6 _[_]

    postulate
      Sub       : Con → Con → Set
      _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
      ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
      id        : ∀{Γ} → Sub Γ Γ
      idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
      idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

      ε         : ∀{Γ} → Sub Γ ∙
      ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

      Tm        : Con → Ty → Set
      _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
      [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
      [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
      _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
      p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
      q         : ∀{Γ A} → Tm (Γ ▹ A) A
      ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
      ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
      ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

      true      : ∀{Γ} → Tm Γ Bool
      false     : ∀{Γ} → Tm Γ Bool
      ite       : ∀{Γ A} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
      iteβ₁     : ∀{Γ A u v} → ite {Γ}{A} true u v ≡ u
      iteβ₂     : ∀{Γ A u v} → ite {Γ}{A} false u v ≡ v
      true[]    : ∀{Γ Δ}{γ : Sub Δ Γ} → true [ γ ] ≡ true
      false[]   : ∀{Γ Δ}{γ : Sub Δ Γ} → false [ γ ] ≡ false
      ite[]     : ∀{Γ Δ A t u v}{γ : Sub Δ Γ} → (ite {Γ}{A} t u v) [ γ ] ≡ ite (t [ γ ]) (u [ γ ]) (v [ γ ])

      num       : ∀{Γ} → ℕ → Tm Γ Nat
      isZero    : ∀{Γ} → Tm Γ Nat → Tm Γ Bool
      _+o_      : ∀{Γ} → Tm Γ Nat → Tm Γ Nat → Tm Γ Nat
      isZeroβ₁  : ∀{Γ} → isZero (num {Γ} 0) ≡ true
      isZeroβ₂  : ∀{Γ n} → isZero (num {Γ} (1 + n)) ≡ false
      +β        : ∀{Γ m n} → num {Γ} m +o num n ≡ num (m + n)
      num[]     : ∀{Γ Δ n}{γ : Sub Δ Γ} → num n [ γ ] ≡ num n
      isZero[]  : ∀{Γ Δ t}{γ : Sub Δ Γ} → isZero t [ γ ] ≡ isZero (t [ γ ])
      +[]       : ∀{Γ Δ u v}{γ : Sub Δ Γ} → (u +o v) [ γ ] ≡ (u [ γ ]) +o (v [ γ ])

    def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
    def t u = u [ id ,o t ]

    -- let x = 1 in x + 3
    -- q + 3 [_, 1] ≡⟨ +[] ⟩
    -- q [_, 1] + 3 [_, 1] ≡⟨ cong (_+(3 [_, 1])) ▹β₂ ⟩
    -- 1 + 3 [_, 1] ≡⟨ cong (1 +_) num[] ⟩
    -- 1 + 3 ≡⟨ +β ⟩
    -- 4

    v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
    v0 = q
    v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
    v1 = q [ p ]
    v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
    v2 = q [ p ⊚ p ]
    v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
    v3 = q [ p ⊚ p ⊚ p ]

  open I

  example : Tm (∙ ▹ Bool) Bool
  example = v0

  example-2 : Tm (∙ ▹ Nat) Bool
  example-2 = example [ ε ,o isZero v0 ]

  con-1 : Con
  con-1 = ∙ ▹ Bool

  tm-1 : Tm con-1 Nat
  tm-1 = ite v0 (num 1) (num 0)

  sub-1 : Sub ∙ con-1
  sub-1 = ε ,o true

  eq-0 : isZero (num 1 +o num 3) ≡ false {∙}
  eq-0 =
    isZero (num 1 +o num 3)
      ≡⟨ cong {A = Tm _ _} (λ a → isZero a) +β ⟩
    isZero (num 4)
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎

  tm-1-sub-1 : tm-1 [ sub-1 ] ≡ num 1
  tm-1-sub-1 =
    -- (tm-1 [ sub-1 ])
    (ite v0 (num 1) (num 0) [ sub-1 ])
      ≡⟨ ite[] ⟩
    ite (q [ ε ,o true ]) (num 1 [ sub-1 ]) (num 0 [ sub-1 ])
      ≡⟨ cong {A = Tm _ _}
           (λ a → ite a (num 1 [ sub-1 ]) (num 0 [ sub-1 ]))
           ▹β₂
      ⟩
    ite true (num 1 [ ε ,o true ]) (num zero [ ε ,o true ])
      ≡⟨ iteβ₁ ⟩
    (num 1 [ ε ,o true ])
      ≡⟨ num[] ⟩
    num 1
      ∎


  {- 2-a -}

  con-2-a : Con
  con-2-a = ∙ ▹ Nat ▹ Bool

  sub-2-a : Sub con-1 con-2-a
  sub-2-a = ε ,o num 0 ,o true

  tm-2-a : Tm con-2-a Nat
  tm-2-a = ite v0 v1 (num 5)

  tm-2-sub-2-a : tm-2-a [ sub-2-a ] ≡ num 0
  tm-2-sub-2-a =
    -- (tm-2-a [ sub-2-a ])
    ((ite v0 v1 (num 5)) [ sub-2-a ])
      ≡⟨ ite[] ⟩
    ite (q [ ε ,o num 0 ,o true ]) ((q [ p ]) [ sub-2-a ]) (num 5 [ sub-2-a ])
      ≡⟨ cong {A = Tm _ _}
           (λ a → ite a ((q [ p ]) [ sub-2-a ]) (num 5 [ sub-2-a ]))
           ▹β₂
      ⟩
    ite true ((q [ p ]) [ sub-2-a ]) (num 5 [ sub-2-a ])
      ≡⟨ iteβ₁ ⟩
    ((q [ p ]) [ (ε ,o num zero) ,o true ])
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    (q [ p ⊚ ((ε ,o num zero) ,o true) ])
      ≡⟨ cong {A = Sub _ _} (λ a → q [ a ]) ▹β₁ ⟩
    (q [ ε ,o num zero ])
      ≡⟨ ▹β₂ ⟩
    num 0
      ∎


  {- 2-b -}

  con-2-b : Con
  con-2-b = ∙ ▹ Nat ▹ Nat

  sub-2-b : Sub ∙ con-2-b
  sub-2-b = ε ,o num 1 ,o num 3

  tm-2-b : Tm con-2-b Bool
  tm-2-b = ite (isZero v0) (isZero v1) (isZero (v1 +o v0))

  tm-2-sub-2-b : tm-2-b [ sub-2-b ] ≡ false
  tm-2-sub-2-b =
    (tm-2-b [ sub-2-b ])
      ≡⟨ refl {x = (tm-2-b [ sub-2-b ])} ⟩
    (ite (isZero q)
        (isZero (q [ p ]))
        (isZero ((q [ p ]) +o q)) [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ ite[] ⟩
    ite (isZero q [ (ε ,o num 1) ,o num 3 ])
      (isZero (q [ p ]) [ sub-2-b ])
      (isZero ((q [ p ]) +o q) [ sub-2-b ])
      ≡⟨ cong {A = Tm _ _} (λ a → ite
        a
        (isZero (q [ p ]) [ sub-2-b ])
        (isZero ((q [ p ]) +o q) [ sub-2-b ])
        )
        (isZero q [ ε ,o num 1 ,o num 3 ]
           ≡⟨ isZero[] ⟩
         isZero (q [ ε ,o num 1 ,o num 3 ])
           ≡⟨ cong {A = Tm _ _} isZero ▹β₂ ⟩
         isZero (num 3)
           ≡⟨ isZeroβ₂ ⟩
         false
           ∎) ⟩
    ite false (isZero (q [ p ]) [ (ε ,o num 1) ,o num 3 ]) (isZero ((q [ p ]) +o q) [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ iteβ₂ ⟩
    (isZero ((q [ p ]) +o q) [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ isZero[] ⟩
    isZero (((q [ p ]) +o q) [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ cong isZero +[] ⟩
    isZero (((q [ p ]) [ sub-2-b ]) +o (q [ (ε ,o num 1) ,o num 3 ]))
      ≡⟨ cong {A = Tm _ _}
        (λ a → isZero (((q [ p ]) [ sub-2-b ]) +o a))
        ▹β₂ ⟩
    isZero (((q [ p ]) [ (ε ,o num 1) ,o num 3 ]) +o num 3)
      ≡⟨ cong {A = Tm _ _}
        (λ a → isZero (a +o num 3))
        (sym {A = Tm _ _} [∘]) ⟩
    isZero ((q [ p ⊚ ((ε ,o num 1) ,o num 3) ]) +o num 3)
      ≡⟨ cong {A = Sub _ _}
        (λ a → isZero ((q [ a ]) +o num 3))
        ▹β₁ ⟩
    isZero ((q [ ε ,o num 1 ]) +o num 3)
      ≡⟨ cong {A = Tm _ _} (λ a → isZero (a +o num 3)) ▹β₂ ⟩
    isZero (num 1 +o num 3)
      ≡⟨ eq-0 ⟩
    false
      ∎

  tm-2-sub-2-b-isti-lem-1 : isZero v0 [ sub-2-b ] ≡ false
  tm-2-sub-2-b-isti-lem-1 =
    (isZero q [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ isZero[] ⟩
    isZero (q [ (ε ,o num 1) ,o num 3 ])
      ≡⟨ cong isZero ▹β₂ ⟩
    isZero (num 3)
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎

  cong-ite₁ : {Γ : Con} → {A : Ty} → {co co' : Tm Γ Bool} → {tr fa : Tm Γ A} →
              (eq : co ≡ co') → ite co tr fa ≡ ite co' tr fa
  cong-ite₁ {tr = tr} {fa = fa} eq = cong (λ a → ite a tr fa) eq

  cong-+₁ : {Γ : Con} → {l l' r : Tm Γ Nat} →
            (eq : l ≡ l') → l +o r ≡ l' +o r
  cong-+₁ {r = r} eq = cong (_+o r) eq

  cong-+₂ : {Γ : Con} → {l r r' : Tm Γ Nat} →
            (eq : r ≡ r') → l +o r ≡ l +o r'
  cong-+₂ {l = l} eq = cong (l +o_) eq

  cong-+ : {Γ : Con} → {l l' r r' : Tm Γ Nat} →
           (eql : l ≡ l') → (eqr : r ≡ r') → l +o r ≡ l' +o r'
  cong-+ {l = l} {l' = l'} {r = r} {r' = r'} eql eqr =
    (l +o r)
      ≡⟨ cong-+₁ eql ⟩
    (l' +o r)
      ≡⟨ cong-+₂ eqr ⟩
    (l' +o r')
      ∎

  ▹v1 : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {a : Tm Γ A} → {b : Tm Γ B} →
        v1 [ σ ,o a ,o b ] ≡ a
  ▹v1 {σ = σ} {a = a} {b = b} =
    ((q [ p ]) [ (σ ,o a) ,o b ])
      ≡⟨ sym {A = Tm _ _} [∘] ⟩
    (q [ p ⊚ ((σ ,o a) ,o b) ])
      ≡⟨ cong {A = Sub _ _} (q [_]) ▹β₁ ⟩
    (q [ σ ,o a ])
      ≡⟨ ▹β₂ ⟩
    a
      ∎

  tm-2-sub-2-b-isti : tm-2-b [ sub-2-b ] ≡ false
  tm-2-sub-2-b-isti =
    (tm-2-b [ sub-2-b ])
      ≡⟨ refl {x = tm-2-b [ sub-2-b ]} ⟩
    ite (isZero v0) (isZero v1) (isZero (v1 +o v0)) [ sub-2-b ]
      ≡⟨ ite[] ⟩
    ite (isZero q [ sub-2-b ]) (isZero v1 [ sub-2-b ]) (isZero (v1 +o v0) [ sub-2-b ])
      ≡⟨ cong-ite₁ tm-2-sub-2-b-isti-lem-1 ⟩
    ite false (isZero v1 [ sub-2-b ]) (isZero (v1 +o v0) [ sub-2-b ])
      ≡⟨ iteβ₂ ⟩
    (isZero (v1 +o q) [ sub-2-b ])
      ≡⟨ isZero[] ⟩
    isZero ((v1 +o q) [ sub-2-b ])
      ≡⟨ cong isZero (
        (v1 +o q) [ sub-2-b ]
          ≡⟨ +[] ⟩
        (v1 [ sub-2-b ]) +o (q [ sub-2-b ])
          ≡⟨ cong-+ ▹v1 ▹β₂ ⟩
        (num 1 +o num 3)
          ≡⟨ +β ⟩
        num 4
          ∎
      )⟩
    isZero (num 4)
      ≡⟨ isZeroβ₂ ⟩
    false
      ∎


  -- Equalities with variables

  var-eq-1 : def true v0 ≡ true {∙}
  var-eq-1 =
    -- def true v0
    (q [ id ,o true ])
      ≡⟨ ▹β₂ ⟩
    true
      ∎

  var-eq-2 : def true (def false v0) ≡ false {∙}
  var-eq-2 =
    -- def true (def false v0)
    ((q [ id ,o false ]) [ id ,o true ])
      -- ≡⟨ cong {A = Tm _ _} (_[ id ,o true ]) ▹β₂ ⟩
      ≡⟨ cong {A = Tm _ _} (λ t → t [ id ,o true ]) ▹β₂ ⟩
    (false [ id ,o true ])
      ≡⟨ false[] ⟩
    false
      ∎

  var-eq-3 : def true (def false v1) ≡ true {∙}
  var-eq-3 =
    -- def true (def false v1)
    (((q [ p ]) [ id ,o false ]) [ id ,o true ])
      ≡⟨ cong {A = Tm _ _} (λ a → a [ id ,o true ])
              (sym {A = Tm _ _} [∘]) ⟩
    ((q [ p ⊚ (id ,o false) ]) [ id ,o true ])
      ≡⟨ cong {A = Sub _ _} (λ a → q [ a ] [ id ,o true ]) ▹β₁ ⟩
    ((q [ id ]) [ id ,o true ])
      ≡⟨ cong {A = Tm _ _} (λ a → a [ id ,o true ]) [id] ⟩
    (q [ id ,o true ])
      ≡⟨ ▹β₂ ⟩
    true
      ∎

  var-eq-4 : def (num 1) (v0 +o v0) ≡ num {∙} 2
  var-eq-4 =
    -- def (num 1) (v0 +o v0)
    ((q +o q) [ id ,o num 1 ])
      ≡⟨ +[] ⟩
    ((q [ id ,o num 1 ]) +o (q [ id ,o num 1 ]))
      ≡⟨ cong {A = Tm _ _} (λ a → a +o (q [ id ,o num 1 ])) ▹β₂ ⟩
    (num 1 +o (q [ id ,o num 1 ]))
      ≡⟨ cong {A = Tm _ _} (num 1 +o_) ▹β₂ ⟩
    (num 1 +o num 1)
      ≡⟨ +β ⟩
    num 2
      ∎

  -- var-eq-5 : def false (def (num 2) (ite v1 (num 0) (num 1 +o v0))) ≡ num {∙} 3
  -- var-eq-5 =
  --   -- def false (def (num 2) (ite v1 (num 0) (num 1 +o v0)))
  --   ((ite (q [ p ]) (num zero) (num 1 +o q) [ id ,o num 2 ]) [ id ,o false ])
  --     ≡⟨ cong {A = Tm _ _} (_[ id ,o false ]) ite[] ⟩
  --   (ite ((q [ p ]) [ id ,o num 2 ]) (num zero [ id ,o num 2 ]) ((num 1 +o q) [ id ,o num 2 ]) [ id ,o false ])
  --     ≡⟨ ite[] ⟩
  --   ite (((q [ p ]) [ id ,o num 2 ]) [ id ,o false ]) ((num zero [ id ,o num 2 ]) [ id ,o false ]) (((num 1 +o q) [ id ,o num 2 ]) [ id ,o false ])
  --     ≡⟨ cong {A = Tm _ _} (λ a → ite (a [ id ,o false ]) (((num zero [ id ,o num 2 ]) [ id ,o false ])) ((((num 1 +o q) [ id ,o num 2 ]) [ id ,o false ]))) {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   num 3
  --     ∎

  -- var-eq-6 : def (num 0) (def (isZero v0) (ite v0 false true)) ≡ false {∙}
  -- var-eq-6 =
  --   def (num 0) (def (isZero v0) (ite v0 false true))
  --     ≡⟨ {!   !} ⟩
  --   false
  --     ∎

  -- var-eq-7 : def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0))) ≡ num {∙} 2
  -- var-eq-7 =
  --   def (num 1) ((v0 +o num 1) +o def v0 (ite (isZero v0) (num 1) (num 0)))
  --     ≡⟨ {!   !} ⟩
  --   num 2
  --     ∎

  -- var-eq-8 : def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0))) ≡ false {∙}
  -- var-eq-8 =
  --   def false (def (num 0) (def (ite v1 v0 (v0 +o (num 1))) (isZero v0)))
  --     ≡⟨ {!   !} ⟩
  --   false
  --     ∎

  -- var-eq-9 : def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0))) ≡ num {∙} 2
  -- var-eq-9 =
  --   def (num 0) (def (num 1 +o v0) (ite (isZero v0) v1 (v1 +o (num 1) +o v0)))
  --     ≡⟨ {!   !} ⟩
  --   num 2
  --     ∎


  -- -- Task

  -- task-sub : Sub ∙ (∙ ▹ Bool ▹ Nat)
  -- task-sub = ε ,o false ,o num 4

  -- task-eq : (ite v1 (v0 +o v0) v0) [ task-sub ] ≡ num 4
  -- task-eq =
  --   (ite (q [ p ]) (q +o q) q [ (ε ,o false) ,o num 4 ])
  --     ≡⟨ {!   !} ⟩
  --   num 4
  --     ∎


  -- -- Equalities with substitutions

  -- sub-swap : {Γ : Con} → {A B : Ty} → Sub (Γ ▹ A ▹ B) (Γ ▹ B ▹ A)
  -- sub-swap = {!   !}

  -- sub-eq-1 : {Γ : Con} → {σ δ : Sub Γ ∙} → σ ≡ δ
  -- sub-eq-1 = {!  !}

  -- sub-eq-2 : {Γ Δ : Con} → {σ : Sub Γ Δ} → ε ⊚ σ ≡ ε
  -- sub-eq-2 = {!  !}

  -- sub-eq-3 :
  --   {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty} → {t : Tm Δ A} →
  --   (σ ,o t) ⊚ δ ≡ (σ ⊚ δ ,o t [ δ ])
  -- sub-eq-3 {Γ} {Δ} {Θ} {σ} {δ} {A} {t} =
  --   (σ ,o t) ⊚ δ
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   (σ ⊚ δ ,o t [ δ ])
  --     ∎

  -- sub-eq-4 :
  --   {Γ Δ : Con} → {σ : Sub Γ Δ} → {A B : Ty} → {t : Tm Δ A} → {u : Tm (Δ ▹ A) B} →
  --   (def t u) [ σ ] ≡ def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  -- sub-eq-4 {σ = σ} {t = t} {u = u} =
  --   (def t u) [ σ ]
  --     ≡⟨ {!   !} ⟩
  --   {!   !}
  --     ≡⟨ {!   !} ⟩
  --   def (t [ σ ]) (u [ σ ⊚ p ,o q ])
  --     ∎
