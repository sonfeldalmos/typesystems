{-# OPTIONS --prop --rewriting #-}
module gy04 where

open import Lib
open import Nat using ()

module NatBoolAST where

  module I where
    data Tm   : Set where
      true    : Tm
      false   : Tm
      ite     : Tm → Tm → Tm → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : Tm → Tm → Tm

  record Model {i} : Set (lsuc i) where
    field
      Tm      : Set i
      true    : Tm
      false   : Tm
      ite     : (co : Tm) → (tr : Tm) → (fa : Tm) → Tm
      num     : ℕ → Tm
      isZero  : Tm → Tm
      _+o_    : (l : Tm) → (r : Tm) → Tm

    ⟦_⟧ : I.Tm → Tm
    ⟦ I.true         ⟧ = true
    ⟦ I.false        ⟧ = false
    ⟦ I.ite t t' t'' ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n        ⟧ = num n
    ⟦ I.isZero t     ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'      ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  record DepModel {i} : Set (lsuc i) where
    field
      Tm      : (t : I.Tm) → Set i
      true    : Tm I.true
      false   : Tm I.false
      ite     : ∀{co' tr' fa'} → (co : Tm co') → (tr : Tm tr') → (fa : Tm fa') → Tm (I.ite co' tr' fa')
      num     : (n : ℕ) → Tm (I.num n)
      isZero  : ∀{t'} → (t : Tm t') → Tm (I.isZero t')
      _+o_    : ∀{l' r'} → (l : Tm l') → (r : Tm r') → Tm (l' I.+o r')

    ⟦_⟧ : (t : I.Tm) → Tm t
    ⟦ I.true          ⟧ = true
    ⟦ I.false         ⟧ = false
    ⟦ I.ite t t' t''  ⟧ = ite ⟦ t ⟧ ⟦ t' ⟧ ⟦ t'' ⟧
    ⟦ I.num n         ⟧ = num n
    ⟦ I.isZero t      ⟧ = isZero ⟦ t ⟧
    ⟦ t I.+o t'       ⟧ = ⟦ t ⟧ +o ⟦ t' ⟧

  open I -- All unqualified terms are from the initial model!

  tm1 : I.Tm
  tm1 = ite true (num 1 +o num 3) (isZero (isZero false))

  {-
          ite
        /  |  \
       /   |   \
     true  +o   isZero
          /\        |
         /  \       |
    num 1  num 3  isZero
                    |
                    |
                  false
  -}


  -- Count the leaves of a tree

  Leaves : Model {i = lzero}
  Leaves = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }
  module L = Model Leaves

  leaves-test-1 : L.⟦ false ⟧ ≡ 1
  leaves-test-1 = refl {x = 1}

  leaves-test-2 : L.⟦ num 3 +o (isZero true) ⟧ ≡ 2
  leaves-test-2 = refl {x = 2}

  leaves-test-3 : L.⟦ ite false true (num 5) ⟧ ≡ 3
  leaves-test-3 = refl {x = 3}

  leaves-test-4 : L.⟦ ite true (num 1 +o num 3) (isZero (isZero false)) ⟧ ≡ 4
  leaves-test-4 = refl {x = 4}


  -- Horizontally mirror a tree (can this be done in NatBoolWT?)

  Flip : Model {i = lzero}
  Flip = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }

  module F = Model Flip

  flip-test : I.Tm
  flip-test = {! F.⟦  ⟧  !}

  flip-test-1 : F.⟦ false ⟧ ≡ false
  flip-test-1 = refl {A = Tm}

  flip-test-2 : F.⟦ num 3 +o (isZero true) ⟧ ≡ (isZero true) +o num 3
  flip-test-2 = refl {A = Tm}

  flip-test-3 : F.⟦ ite false true (num 5) ⟧ ≡ ite (num 5) true false
  flip-test-3 = refl {A = Tm}

  flip-test-4 : F.⟦ ite true (num 1 +o num 3) (isZero (isZero false)) ⟧ ≡ ite (isZero (isZero false)) (num 3 +o num 1) true
  flip-test-4 = refl {A = Tm}


  -- Flipping a tree doesn't change the number of leaves it has

  postulate
    h1 : {a b c d e f : ℕ} → (a ≡ f) → (b ≡ e) → (c ≡ d) → a + b + c ≡ d + e + f
    h2 : {a b c d : ℕ} →     (a ≡ d) → (b ≡ c) →           a + b ≡ c + d

  FlipLeaves : DepModel {i = lzero}
  FlipLeaves = record
    { Tm     = {!   !}
    ; true   = {!   !}
    ; false  = {!   !}
    ; ite    = {!   !}
    ; num    = {!   !}
    ; isZero = {!   !}
    ; _+o_   = {!   !}
    }


-- Well Typed

module NatBoolWT where

  module I where
    data Ty   : Set where
      Nat     : Ty
      Bool    : Ty
    data Tm   : Ty → Set where
      true    : Tm Bool
      false   : Tm Bool
      ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
      num     : ℕ → Tm Nat
      isZero  : Tm Nat → Tm Bool
      _+o_    : Tm Nat → Tm Nat → Tm Nat

  record Model {i j} : Set (lsuc (i ⊔ j)) where
    field
      Ty      : Set i
      Tm      : Ty → Set j
      Nat     : Ty
      Bool    : Ty
      true    : Tm Bool
      false   : Tm Bool
      ite     : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
      num     : ℕ → Tm Nat
      isZero  : Tm Nat → Tm Bool
      _+o_    : Tm Nat → Tm Nat → Tm Nat

    ⟦_⟧T  : I.Ty → Ty
    ⟦_⟧t  : ∀{A} → I.Tm A → Tm ⟦ A ⟧T
    ⟦ I.Nat           ⟧T = Nat
    ⟦ I.Bool          ⟧T = Bool
    ⟦ I.true          ⟧t = true
    ⟦ I.false         ⟧t = false
    ⟦ I.ite t u v     ⟧t = ite ⟦ t ⟧t ⟦ u ⟧t ⟦ v ⟧t
    ⟦ I.num n         ⟧t = num n
    ⟦ I.isZero t      ⟧t = isZero ⟦ t ⟧t
    ⟦ u I.+o v        ⟧t = ⟦ u ⟧t +o ⟦ v ⟧t

  record DepModel {i j} : Set (lsuc (i ⊔ j)) where
    field
      Ty      :  I.Ty → Set i
      Tm      :  ∀{Aᴵ} → Ty Aᴵ → I.Tm Aᴵ → Set j
      Nat     :  Ty I.Nat
      Bool    :  Ty I.Bool
      true    :  Tm Bool I.true
      false   :  Tm Bool I.false
      ite     :  ∀{Aᴵ tᴵ uᴵ vᴵ}{A : Ty Aᴵ} → Tm Bool tᴵ → Tm A uᴵ → Tm A vᴵ →
                Tm A (I.ite tᴵ uᴵ vᴵ)
      num     :  (n : ℕ) → Tm Nat (I.num n)
      isZero  :  ∀{tᴵ} → Tm Nat tᴵ → Tm Bool (I.isZero tᴵ)
      _+o_    :  ∀{uᴵ vᴵ} → Tm Nat uᴵ → Tm Nat vᴵ → Tm Nat (uᴵ I.+o vᴵ)
    ⟦_⟧T  : (Aᴵ : I.Ty) → Ty Aᴵ
    ⟦_⟧t  : ∀{Aᴵ}(tᴵ : I.Tm Aᴵ) → Tm ⟦ Aᴵ ⟧T tᴵ
    ⟦ I.Nat           ⟧T = Nat
    ⟦ I.Bool          ⟧T = Bool
    ⟦ I.true          ⟧t = true
    ⟦ I.false         ⟧t = false
    ⟦ I.ite tᴵ uᴵ vᴵ  ⟧t = ite ⟦ tᴵ ⟧t ⟦ uᴵ ⟧t ⟦ vᴵ ⟧t
    ⟦ I.num n         ⟧t = num n
    ⟦ I.isZero tᴵ     ⟧t = isZero ⟦ tᴵ ⟧t
    ⟦ uᴵ I.+o vᴵ      ⟧t = ⟦ uᴵ ⟧t +o ⟦ vᴵ ⟧t

  open I -- All unqualified terms are from the initial model!

  -- Type inference

  data Result (A : Set) : Set where
    Ok : (r : A) → Result A
    Err : (e : ℕ) → Result A

  {-
    Error codes:
    · 1: Non boolean condition
    · 2: Differing branch types
    · 3: Non numeric isZero parameter
    · 4: Non numeric addition parameter
  -}

  Infer : NatBoolAST.Model {i = lzero}
  Infer = record
    { Tm      = {!   !}
    ; true    = {!   !}
    ; false   = {!   !}
    ; ite     = {!   !}
    ; num     = {!   !}
    ; isZero  = {!   !}
    ; _+o_    = {!   !}
    }

  module INF = NatBoolAST.Model Infer

  inf-test : Result (Σ Ty (λ T → Tm T))
  inf-test = {! INF.⟦ ite true (num 1) ((num 2) +o (num 3)) ⟧  !}
    where open NatBoolAST.I

  -- Standard model

  Standard : Model {i = lsuc lzero} {j = lzero}
  Standard = record
    { Ty      = {!   !}
    ; Tm      = {!   !}
    ; Nat     = {!   !}
    ; Bool    = {!   !}
    ; true    = {!   !}
    ; false   = {!   !}
    ; ite     = {!   !}
    ; num     = {!   !}
    ; isZero  = {!   !}
    ; _+o_    = {!   !}
    }
  module STD = Model Standard

  eval : {A : Ty} → Tm A → {!   !}
  eval = {!   !}

  run : (t : NatBoolAST.I.Tm) → {!   !}
  run t = {!   !}