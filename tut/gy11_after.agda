{-# OPTIONS --prop --rewriting #-}
module gy11_after where

open import Lib

module I where
  infixl 6 _⊚_
  infixl 6 _[_]
  infixl 5 _▹_
  infixl 5 _,o_
  infixr 5 _⇒_
  infixl 5 _$_

  data Ty     : Set where
    _⇒_       : Ty → Ty → Ty
    Prod      : Ty → Ty → Ty
    Unit      : Ty
    Sum       : Ty → Ty → Ty
    Empty     : Ty

  data Con    : Set where
    ∙         : Con
    _▹_       : Con → Ty → Con

  postulate
    Sub       : Con → Con → Set
    _⊚_       : ∀{Γ Δ Θ} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
    ass       : ∀{Γ Δ Θ Ξ}{γ : Sub Δ Γ}{δ : Sub Θ Δ}{θ : Sub Ξ Θ} → (γ ⊚ δ) ⊚ θ ≡ γ ⊚ (δ ⊚ θ)
    id        : ∀{Γ} → Sub Γ Γ
    idl       : ∀{Γ Δ}{γ : Sub Δ Γ} → id ⊚ γ ≡ γ
    idr       : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ⊚ id ≡ γ

    ε         : ∀{Γ} → Sub Γ ∙
    ∙η        : ∀{Γ}{σ : Sub Γ ∙} → σ ≡ ε

    Tm        : Con → Ty → Set
    _[_]      : ∀{Γ Δ A} → Tm Γ A → Sub Δ Γ → Tm Δ A
    [∘]       : ∀{Γ Δ Θ A}{t : Tm Γ A}{γ : Sub Δ Γ}{δ : Sub Θ Δ} →  t [ γ ⊚ δ ] ≡ t [ γ ] [ δ ]
    [id]      : ∀{Γ A}{t : Tm Γ A} → t [ id ] ≡ t
    _,o_      : ∀{Γ Δ A} → Sub Δ Γ → Tm Δ A → Sub Δ (Γ ▹ A)
    p         : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q         : ∀{Γ A} → Tm (Γ ▹ A) A
    ▹β₁       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → p ⊚ (γ ,o t) ≡ γ
    ▹β₂       : ∀{Γ Δ A}{γ : Sub Δ Γ}{t : Tm Δ A} → q [ γ ,o t ] ≡ t
    ▹η        : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → p ⊚ γa ,o q [ γa ] ≡ γa

    lam       : ∀{Γ A B} → Tm (Γ ▹ A) B → Tm Γ (A ⇒ B)
    _$_       : ∀{Γ A B} → Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
    ⇒β        : ∀{Γ A B}{t : Tm (Γ ▹ A) B}{u : Tm Γ A} → lam t $ u ≡ t [ id ,o u ]
    ⇒η        : ∀{Γ A B}{t : Tm Γ (A ⇒ B)} → lam (t [ p ] $ q) ≡ t
    lam[]     : ∀{Γ Δ A B}{t : Tm (Γ ▹ A) B}{γ : Sub Δ Γ} →
                (lam t) [ γ ] ≡ lam (t [ γ ⊚ p ,o q ])
    $[]       : ∀{Γ Δ A B}{t : Tm Γ (A ⇒ B)}{u : Tm Γ A}{γ : Sub Δ Γ} →
                (t $ u) [ γ ] ≡ t [ γ ] $ u [ γ ]

    pair      : ∀{Γ A B} → Tm Γ A → Tm Γ B → Tm Γ (Prod A B)
    fst       : ∀{Γ A B} → Tm Γ (Prod A B) → Tm Γ A
    snd       : ∀{Γ A B} → Tm Γ (Prod A B) → Tm Γ B
    Prodβ₁    : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → fst (pair u v) ≡ u
    Prodβ₂    : ∀{Γ A B}{u : Tm Γ A}{v : Tm Γ B} → snd (pair u v) ≡ v
    Prodη     : ∀{Γ A B}{t : Tm Γ (Prod A B)} → pair (fst t) (snd t) ≡ t
    pair[]    : ∀{Γ Δ A B}{u : Tm Γ A}{v : Tm Γ B}{γ : Sub Δ Γ} →
                pair u v [ γ ] ≡ pair (u [ γ ]) (v [ γ ])

    trivial   : ∀{Γ} → Tm Γ Unit
    Unitη     : ∀{Γ}{u : Tm Γ Unit} → u ≡ trivial

    -- data Either a b = Left a | Right b

    inl       : ∀{Γ A B} → Tm Γ A → Tm Γ (Sum A B)
    inr       : ∀{Γ A B} → Tm Γ B → Tm Γ (Sum A B)
    switch    : ∀{Γ A B C} → Tm Γ (Sum A B) → Tm (Γ ▹ A) C → Tm (Γ ▹ B) C → Tm Γ C
    Sumβ₁     : ∀{Γ A B C}{t : Tm Γ A}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
                switch (inl t) u v ≡ u [ id ,o t ]
    Sumβ₂     : ∀{Γ A B C}{t : Tm Γ B}{u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C} →
                switch (inr t) u v ≡ v [ id ,o t ]
    inl[]     : ∀{Γ Δ A B}{t : Tm Γ A}{γ : Sub Δ Γ} →
                (inl {B = B} t) [ γ ] ≡ inl (t [ γ ])
    inr[]     : ∀{Γ Δ A B}{t : Tm Γ B}{γ : Sub Δ Γ} →
                (inr {A = A} t) [ γ ] ≡ inr (t [ γ ])
    switch[]  : ∀{Γ Δ A B C}{t : Tm Γ (Sum A B)}
                {u : Tm (Γ ▹ A) C}{v : Tm (Γ ▹ B) C}{γ : Sub Δ Γ} →
                (switch t u v) [ γ ] ≡
                switch (t [ γ ]) (u [ γ ⊚ p ,o q ]) (v [ γ ⊚ p ,o q ])

    absurd    : ∀{Γ A} → Tm Γ Empty → Tm Γ A
    absurd[]  : ∀{Γ Δ A}{t : Tm Γ Empty}{γ : Sub Δ Γ} →
                (absurd {A = A} t) [ γ ] ≡ absurd (t [ γ ])

  def : ∀{Γ A B} → Tm Γ A → Tm (Γ ▹ A) B → Tm Γ B
  def t u = u [ id ,o t ]

  v0 : {Γ : Con} → {A : Ty} → Tm (Γ ▹ A) A
  v0 = q
  v1 : {Γ : Con} → {A B : Ty} → Tm (Γ ▹ A ▹ B) A
  v1 = q [ p ]
  v2 : {Γ : Con} → {A B C : Ty} → Tm (Γ ▹ A ▹ B ▹ C) A
  v2 = q [ p ⊚ p ]
  v3 : {Γ : Con} → {A B C D : Ty} → Tm (Γ ▹ A ▹ B ▹ C ▹ D) A
  v3 = q [ p ⊚ p ⊚ p ]

open I

-- Implement the Boolean type!
-- Hint: Use Sum and Unit!

-- Kell: 2
-- Van: 0, 1, +, *
-- 1 + 1 = 2

Bool : Ty
Bool = Sum Unit Unit

true : {Γ : Con} → Tm Γ Bool
true = inl trivial

false : {Γ : Con} → Tm Γ Bool
false = inr trivial

ite : {Γ : Con}{A : Ty} → Tm Γ Bool → Tm Γ A → Tm Γ A → Tm Γ A
ite co tr fa = switch co (tr [ p ]) (fa [ p ])

iteβ₁ : {Γ : Con}{A : Ty} → {u v : Tm Γ A} → ite true u v ≡ u
iteβ₁ {u = u} {v = v} =
  switch (inl trivial) (u [ p ]) (v [ p ])
    ≡⟨ Sumβ₁ ⟩
  u [ p ] [ id ,o trivial ]
    ≡⟨ sym {A = Tm _ _} [∘] ⟩
  u [ p ⊚ (id ,o trivial) ]
    ≡⟨ cong {A = Sub _ _} (λ a → u [ a ]) ▹β₁ ⟩
  u [ id ]
    ≡⟨ [id] ⟩
  u
    ∎

iteβ₂ : {Γ : Con}{A : Ty} → {u v : Tm Γ A} → ite false u v ≡ v
iteβ₂ {u = u} {v = v} =
  switch (inr trivial) (u [ p ]) (v [ p ])
    ≡⟨ Sumβ₂ ⟩
  v [ p ] [ id ,o trivial ]
    ≡⟨ sym {A = Tm _ _} [∘] ⟩
  v [ p ⊚ (id ,o trivial) ]
    ≡⟨ cong {A = Sub _ _} (λ a → v [ a ]) ▹β₁ ⟩
  v [ id ]
    ≡⟨ [id] ⟩
  v
    ∎

-- Implement the Maybe type!
-- Hint: Use Sum and Unit!
--
-- Since the name "Maybe" and it's usual constructors "Just" and "Nothing"
-- are already used in `Lib`, use the following names:
-- * Maybe   = Optional
-- * Just    = Some
-- * Nothing = None

-- isEven : R → Optional Bool
-- isEven 2 = Some True
-- isEven 3 = Some False
-- isEven 3.14 = None

-- output : R → String
-- output 2 = "Even"
-- output 3 = "Not even"
-- output 3.14 = "No idea"

-- output r = switch (isEven r) {
--   None -> "No idea"
--   Some b -> if b then "Even" else "Not even"
-- }

Optional : Ty → Ty
Optional ty = Sum Unit ty

None : {Γ : Con}{A : Ty} → Tm Γ (Optional A)
None = inl trivial

Some : {Γ : Con}{A : Ty} → Tm Γ A → Tm Γ (Optional A)
-- Some t = inr t
Some = inr

fromOptional : {Γ : Con}{A B : Ty} →
               Tm Γ B → Tm Γ (A ⇒ B) → Tm Γ (Optional A) → Tm Γ B
fromOptional n s o = switch o (n [ p ]) (s [ p ] $ v0)

fromOptionalβ₁ : {Γ : Con}{A B : Ty} → {n : Tm Γ B} → {s : Tm Γ (A ⇒ B)} →
                 fromOptional n s (None {A = A}) ≡ n
fromOptionalβ₁ {n = n} {s = s} =
  switch (inl trivial) (n [ p ]) (s [ p ] $ q)
    ≡⟨ Sumβ₁ ⟩
  n [ p ] [ id ,o trivial ]
    ≡⟨ sym {A = Tm _ _} [∘] ⟩
  n [ p ⊚ (id ,o trivial) ]
    ≡⟨ cong {A = Sub _ _} (n [_]) ▹β₁ ⟩
  n [ id ]
    ≡⟨ [id] ⟩
  n
    ∎

fromOptionalβ₂ : {Γ : Con}{A B : Ty} → {n : Tm Γ B} → {s : Tm Γ (A ⇒ B)} →
                 {t : Tm Γ A} → fromOptional n s (Some t) ≡ s $ t
fromOptionalβ₂ {n = n} {s = s} {t = t} =
  switch (inr t) (n [ p ]) (s [ p ] $ q)
    ≡⟨ Sumβ₂ ⟩
  (s [ p ] $ q) [ id ,o t ]
    ≡⟨ $[] ⟩
  s [ p ] [ id ,o t ] $ q [ id ,o t ]
    ≡⟨ cong {A = Tm _ _} (s [ p ] [ id ,o t ] $_) ▹β₂ ⟩
  s [ p ] [ id ,o t ] $ t
    ≡⟨ cong {A = Tm _ _} (_$ t) (sym {A = Tm _ _} [∘]) ⟩
  s [ p ⊚ (id ,o t) ] $ t
    ≡⟨ cong {A = Sub _ _} (λ a → s [ a ] $ t) ▹β₁ ⟩
  s [ id ] $ t
    ≡⟨ cong {A = Tm _ _} (_$ t) [id] ⟩
  s $ t
    ∎


-- Extra task: define n-tuples

Tuple : ℕ → Ty → Ty
Tuple n ty = {!   !}