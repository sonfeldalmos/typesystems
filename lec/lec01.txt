tegeződhetünk?

Kaposi Ambrus, docens, PRogramozási Nyelvek és Fordítóprogramok
akaposi@inf.elte.hu

Nyelvek típusrendszere

2 óra EA + 2 óra Gyak
gyakorlatok: Donkó István

https://bitbucket.org/akaposi/typesystems

jegyzet: előadások anyaga, folyamatosan fejlődik, angolul
további irodalom: Csörnyei Zoltán könyvei (alacsonyabb szintű tárgyalás),
                  Robert Harper: Practical foundations of programming languages (PFPL)
                  Wen Kokke, Philip Wadler: PLFA (Agda)
                  Pierce: SF (Coq)

gyakorlatokon: Agda programozási nyelv
előfeltétel: funkcionális programozás
előismeretek: típuselmélet; logika, számításelmélet, diszkrét matek (algebra, csoportok), formális nyelvek

számonkérés:
előadás:
  KVÍZ minden előadásra lesz egy canvas kvíz, trükkös kérdések
       az első kvíz nagyon egyszerű
       félév végén 50% fölött kell állni
  GYAKORLATI JEGY
  VIZSGA: Agda + elméleti (online)
          papíron: csak elméleti

INTERAKCIÓ

Formális szemantika: Coq
Itt: Agda

Martin-Löf típuselmélet: Coq, Agda, Idris, Lean, Epigram, F*
System F: Haskell, ML, OCaml, F#
System Fω: Java

kezdés: 16.00

félév felépítése:
1. egyszerű kifejezésnyelv (Nat, Bool): mi az, hogy (programozási) nyelv?
2. függvény típus
3. induktív típus (természetes számok, listák, bináris fák)
4. koinduktív típus (webszerver, végtelen lista, végtelen mélységű fák)
5. polimorf típusok (template típus <>, generikus)
6. altípus

TANANYAG

egyszerű kifejezésnyelv:

csak kifejezések (*term*, expression):

if isZero (num 2 + num 1) then false else isZero (num 0)
if isZero (num 3)         then false else isZero (num 0)
if false                  then false else isZero (num 0)
isZero (num 0)
true

alacsony absztrakciós szint -> magas
nem pontos -> pontos

1. karaktersorozat
  "if isZero (num 2 + num 1) then false else isZero (num 0)"

  NEM KÉNE TERMNEK TEKINTENI:
  "num 1 - num 2"
  "nun"
  "éáőü"
  ""

  TÚL SOK MINDENT MEGKÜLÖNBÖZTETÜNK:
  "num 1" ≠ "num    1"

  | lexing, lexikális elemzés
  v

2. lexikális elemek sorozatai:
   lexikális elemek: if, then, else, true, false, num, +, isZero, (, ), 0,1,2,3,...
   [num, 1] <- "num 1"

   PROGRAM:
   [if, then]
   [isZero]
   [0, num, +]
   [(, true]

   TÚL SOK MINDENT MEGKÜLÖNBÖZTETÜNK:
   [true] ≠ [(, true, )] ≠ [(, (, true, ), )]

   | szintaktikus elemzés, parsing
   v

3. absztrakt szintaxisfa

  BNF:
  N ::= 0 | 1 | 2 | 3 | ...
  T ::= true | false | if T then T else T | num N | isZero T | T + T


      +
     / \
    /   \
 num 0   num 2

      +
     / \
 true   num 0

      ite              ite=if then else
     / |   \
    /  |         \
 true   +          ite
      / \       /   |   \
     /   \     /    |    \
  true  num 1 num 1 isZero  true
                     |
                     |
                   false

   "if true then (true + num 1) else (if num 1 then isZero false else true)"

   (num 0 + num 0) + num 0 ≠ num 0 + (num 0 + num 0)

       /\        /\
      /\ 0      0 /\
     0  0        0  0


  N ::= 0 | 1 | 2 | 3 | ...
  T ::= true | false | if T then T else T | num N | isZero T | T + T

  data T = True | False | Ite T T T | Num Int | IsZero T | T :+: T      (Haskell)

  data Tm : Set where           (Agda)
    true   : Tm
    false  : Tm
    ite    : Tm → Tm → Tm → Tm               Tm × Tm × Tm → Tm
    num    : ℕ → Tm
    isZero : Tm → Tm
    _+_    : Tm → Tm → Tm
    
    --   ite t t' t''                       ite(t,t',t'')

  Tm egy induktív konstrukció


NYELV: metanyelv, objektumnyelv
nálunk: metanyelv = Agda (típuselmélet, halmazelmélet, magyar nyelv), objektumnyelv = NatBool
  tt, ff : 𝟚 (metanyelv)
  (true, false : Tm) objektumnyelv
  0,1,2,3 : ℕ (metanyelv)
  num 1 + num 3 : Tm objektumnyelvi természetes szám

  REKURZÍV DEFINÍCIÓ:
  height : Tm → ℕ
  height true := 0
  height false := 0
  height (ite t t' t'') := 1 + max (height t) (height t') (height t'')
  height (num _) := 0
  height (isZero t) := 1 + height t
  height (t + t') := 1 + max (height t) (height t')




    true   : Tm
    false  : Tm
    ite    : Tm → Tm → Tm → Tm               Tm × Tm × Tm → Tm
    num    : ℕ → Tm
    isZero : Tm → Tm
    _+_    : Tm → Tm → Tm

  TÚL SOK PROGRAM VAN:    
  isZero true
  true + false

  (1+2)*3 = 1+(2*3)

  NEM EGYENLŐ:
  (num 0 + num 1) ≠ num 1

4. jóltípusozott szintaxisfa

data
   Ty : Set
   Bool : Ty
   Nat  : Ty
   Tm : Ty → Set                  Tm Bool : Set,   Tm Nat : Set
   true false : Tm Bool
   ite : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
   num : ℕ → Tm Nat
   isZero : Tm Nat → Tm Bool
   _+_ : Tm Nat → Tm Nat → Tm Nat
   

      isZero        ite
        |         /  |    \
      num 3  false  true   isZero
                             |
                           num 2

5. algebrai szintaxis

   Ty : Set
   Bool : Ty
   Nat  : Ty
   Tm : Ty → Set                  Tm Bool : Set,   Tm Nat : Set
   true false : Tm Bool
   ite : {A : Ty} → Tm Bool → Tm A → Tm A → Tm A
   num : ℕ → Tm Nat
   isZero : Tm Nat → Tm Bool
   _+_ : Tm Nat → Tm Nat → Tm Nat
      ite true a b = a
      ite false a b = b
      isZero (num 0) = true
      isZero (num (1+n)) = false
      num n + num m = num (n + m)
