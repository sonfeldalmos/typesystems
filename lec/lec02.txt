NatBool
1 sztring
2 lex.el.sor
3 ast
4 WT
5 WT=

nyelv modellje, szintaxis, rekurzio, függő modell (dependens modell), indukcio

3. NatBoolAST
Tm     : Set                     <- algebrai struktura   M    : Set
true   : Tm                                              u    : M
false  : Tm                                              _⊗_  : M → M → M
ite    : Tm → Tm → Tm → Tm                               ass  : (a,b,c:M) → (a⊗b)⊗c = a⊗(b⊗c)
num    : ℕ → Tm                                          idl  : (a:M) → u⊗a = a
isZero : Tm → Tm                                         idr  : (a:M) → a⊗u = a
_+_    : Tm → Tm → Tm

modell = szemantika

true, false, num 0, num 1, num 2, ....
isZero
_+_
ite

NatBoolAST-model H:

H.Tm        := ℕ
H.true      := 0
H.false     := 0
H.ite a b c := 1 + max(a,b,c)
H.num n     := 0
H.isZero a  := 1 + a
a H.+o b    := 1 + max(a, b)

(true +o true) +o false = (0 +o 0) +o 0 = 1 + max(0 +o 0, 0) =
  1 + max(1 + max(0,0), 0) = 1 + 1 = 2

  +
 / \
+   true
/\
true treu

szintaxis = I = egy NatBoolAST modell, amibol minden NatBoolAST modellbe megy egy
  homomorfizmus (muvelettarto lekepezes)

M : NatBoolASTModell
---------------------
M.⟦_⟧ : I.Tm → M.Tm
M.⟦ I.true      ⟧ = M.true
M.⟦ I.false     ⟧ = M.false
M.⟦ I.ite t u v ⟧ = M.ite M.⟦ t ⟧ M.⟦ u ⟧ M.⟦ v ⟧
M.⟦ I.num n     ⟧ = M.num n
M.⟦ I.isZero t  ⟧ = M.isZero M.⟦ t ⟧
M.⟦ u I.+ v     ⟧ = M.⟦ u ⟧ M.+ M.⟦ v ⟧

⟦_⟧ = rekurzor, primitiv rekurzio, kiertekeles, eval, interpreter, fold, catamorfizmus, (nemdependens)eliminator

Osszefoglalas: I = inicialis modell, minden mas modellbe megy homomorfizmus

f : A -> B
f : A.Tm → B.Tm
megorzi az osszes muvelelet, pl. f A.true = B.true, stb.


height : I.Tm → ℕ
height := H.⟦_⟧

height ((I.true I.+o I.true) I.+o I.false) =
H.⟦ (I.true I.+o I.true) I.+o I.false ⟧ =
(H.true H.+o H.true) H.+o H.false =
1 + max(1 + max(0,0), 0) =
1 + 1 =
2

height : I.Tm → ℕ
height I.true = 0
height I.false = 0
height (I.isZero t) = 1 + height t
....

A, B: NatBoolAST-modellek

A×B : NatBoolAST-modell
(A×B).Tm := A.Tm × B.Tm
(A×B).true := (A.true , B.true)
(A×B).isZero (a,b) := (A.isZero a , B.isZero b)
...


F : Set
0,1 :F
+,* :F→F→F
-_  : F→F
_⁻¹ : (a:F) → a ≠ 0 →F
axioma

-----------------------------

C-stilusu szemantika: C

Tm := ℕ
true := 1
false := 0
ite t u v := ha t=0, akkor v, kulonben u
num n := n
isZero t := ha t=0, akkor 1, kulonben 0
u +o v := u + v

C.⟦_⟧ : I.Tm → ℕ
interpreter, kiertekeles, metacirkularis interpeter

--------------------------------------------------------

termeszetes szamok:

Nat:Set
zero:Nat
suc : Nat → Nat

_+_ : I.Nat → I.Nat → I.Nat
I.zero + n := n
I.suc a + n := I.suc (a + n)

_+_ := A.⟦_⟧ : I.Nat → I.Nat → I.Nat

A : Nat-modell
A.Nat := I.Nat → I.Nat
A.zero : λ n → n   I.Nat → I.Nat       A.zero = zero+_   fuggveny
A.suc f := I.suc ∘ f                           A.suc f x
